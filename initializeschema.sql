--filldb.info was used for data population
--sqlines application was used for syntax conversion

DROP TABLE IF EXISTS device CASCADE;

DROP TABLE IF EXISTS server_model CASCADE;

DROP TABLE IF EXISTS camera_model CASCADE;

DROP TABLE IF EXISTS projector_model CASCADE;

DROP TABLE IF EXISTS printer_model CASCADE;

DROP TABLE IF EXISTS computer_model CASCADE;

DROP TABLE IF EXISTS monitor_model CASCADE;

DROP TABLE IF EXISTS nwdevice_model CASCADE;

DROP TABLE IF EXISTS device_owner CASCADE;

DROP TABLE IF EXISTS works_in CASCADE;

DROP TABLE IF EXISTS member_of CASCADE;

DROP TABLE IF EXISTS rack CASCADE;

DROP TABLE IF EXISTS room CASCADE;

DROP TABLE IF EXISTS manufacturer CASCADE;

DROP TABLE IF EXISTS building CASCADE;

DROP TABLE IF EXISTS person CASCADE;

DROP TABLE IF EXISTS department CASCADE;

CREATE TABLE IF EXISTS manufacturer
(
  manuf_id INT NOT NULL,
  name VARCHAR(50) NOT NULL,
  support_site VARCHAR(50) NOT NULL,
  is_active BOOLEAN NOT NULL,
  PRIMARY KEY (manuf_id)
);

CREATE TABLE building
(
  bldg_id INT NOT NULL,
  campus VARCHAR(50) NOT NULL,
  name VARCHAR(50) NOT NULL,
  capacity INT NOT NULL CHECK(capacity > 0),
  is_active BOOLEAN NOT NULL,
  PRIMARY KEY (bldg_id)
);

CREATE TABLE person
(
  person_id INT NOT NULL,
  fname VARCHAR(50) NOT NULL,
  lname VARCHAR(50) NOT NULL,
  phone BIGINT NOT NULL,
  email VARCHAR(50) NOT NULL,
  role VARCHAR(50) NOT NULL,
  is_active BOOLEAN NOT NULL,
  PRIMARY KEY (person_id)
);

CREATE TABLE department
(
  dept_id INT NOT NULL,
  name VARCHAR(50) NOT NULL,
  head VARCHAR(50) NOT NULL,
  division VARCHAR(50) NOT NULL,
  is_active BOOLEAN NOT NULL,
  PRIMARY KEY (dept_id)
);

CREATE TABLE server_model
(
  model_id INT NOT NULL,
  ram INT NOT NULL,
  os VARCHAR(50) NOT NULL,
  hd_size INT NOT NULL,
  virtual BOOLEAN NOT NULL,
  manuf_id INT NOT NULL REFERENCES manufacturer(manuf_id),
  is_active BOOLEAN NOT NULL,
  PRIMARY KEY (model_id)
);

CREATE TABLE works_in
(
  person_id INT NOT NULL REFERENCES person(person_id),
  bldg_id INT NOT NULL REFERENCES building(bldg_id),
  is_active BOOLEAN NOT NULL
);

CREATE TABLE member_of
(
  person_id INT NOT NULL REFERENCES person(person_id),
  dept_id INT NOT NULL REFERENCES department(dept_id),
  is_active BOOLEAN NOT NULL
);

CREATE TABLE camera_model
(
  recording BOOLEAN NOT NULL,
  `range` INT NOT NULL,
  resolution INT CHECK(resolution > 0),
  field_of_view INT NOT NULL,
  model_id INT NOT NULL,
  manuf_id INT NOT NULL REFERENCES manufacturer(manuf_id),
  is_active BOOLEAN NOT NULL,
  PRIMARY KEY (model_id)
);

CREATE TABLE projector_model
(
  model_id INT NOT NULL,
  hdmi INT NOT NULL,
  wifi BOOLEAN NOT NULL,
  lumens INT NOT NULL,
  speakers BOOLEAN NOT NULL,
  resolution INT NOT NULL,
  manuf_id INT NOT NULL REFERENCES manufacturer(manuf_id),
  is_active BOOLEAN NOT NULL,
  PRIMARY KEY (model_id)
);

CREATE TABLE printer_model
(
  model_id INT NOT NULL,
  double_sided BOOLEAN NOT NULL,
  paper_size VARCHAR(10) NOT NULL,
  memory INT NOT NULL,
  speed INT NOT NULL,
  laser BOOLEAN NOT NULL,
  bw_color VARCHAR(50) NOT NULL,
  is_active BOOLEAN NOT NULL,
  manuf_id INT NOT NULL REFERENCES manufacturer(manuf_id),
  PRIMARY KEY (model_id)
);

CREATE TABLE computer_model
(
  model_id INT NOT NULL,
  cpu VARCHAR(50) NOT NULL,
  screen_size INT,
  resolution INT,
  ram INT NOT NULL,
  os VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  hd_size INT NOT NULL,
  is_active BOOLEAN NOT NULL,
  manuf_id INT NOT NULL REFERENCES manufacturer(manuf_id),
  PRIMARY KEY (model_id)
);

CREATE TABLE monitor_model
(
  model_id INT NOT NULL,
  hdmi INT NOT NULL,
  panel_type VARCHAR(50) NOT NULL,
  resolution INT NOT NULL,
  size INT NOT NULL,
  refresh_rate INT NOT NULL,
  is_active BOOLEAN NOT NULL,
  manuf_id INT NOT NULL REFERENCES manufacturer(manuf_id),
  PRIMARY KEY (model_id)
);

CREATE TABLE nwdevice_models
(
  model_id INT NOT NULL,
  mac_address VARCHAR(50) NOT NULL,
  protocol VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  speed INT NOT NULL,
  number_ports INT NOT NULL,
  coverage INT NOT NULL,
  number_clients INT NOT NULL,
  is_active BOOLEAN NOT NULL,
  manuf_id INT NOT NULL REFERENCES manufacturer(manuf_id),
  PRIMARY KEY (model_id)
);

CREATE TABLE device
(
  serial_number INT NOT NULL,
  custom_cpu VARCHAR(50),
  custom_ram INT,
  custom_hdsize INT,
  year_manufac INT NOT NULL,
  date_purchased VARCHAR(50) NOT NULL,
  os VARCHAR(50) NOT NULL,
  is_active BOOLEAN NOT NULL,
  nwmodel_id INT REFERENCES nwdevice_models(model_id),
  monitormodel_id INT REFERENCES monitor_model(model_id),
  cameramodel_id INT REFERENCES camera_model(model_id),
  projectormodel_id INT REFERENCES projector_model(model_id),
  printermodel_id INT REFERENCES printer_model(model_id),
  computermodel_id INT REFERENCES computer_model(model_id)
);

CREATE TABLE room
(
  room_id INT NOT NULL,
  description VARCHAR(50) NOT NULL,
  is_active BOOLEAN NOT NULL,
  bldg_id INT NOT NULL REFERENCES building(bldg_id),
  PRIMARY KEY (room_id)
);

CREATE TABLE rack
(
  slot INT NOT NULL,
  rack_id INT NOT NULL,
  is_active BOOLEAN NOT NULL,
  room_id INT NOT NULL REFERENCES room(room_id),
  PRIMARY KEY (rack_id)
);

CREATE TABLE owner
(
  serial_number INT NOT NULL,
  is_active BOOLEAN NOT NULL,
  person_id INT NOT NULL REFERENCES person(person_id),
  dept_id INT NOT NULL REFERENCES department(dept_id),
  room_id INT NOT NULL REFERENCES room(room_id),
  rack_id INT NOT NULL REFERENCES rack(rack_id)
);

INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (1, 'Westmont', 'Porter Center', 1173, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (2, 'Westmont', 'Chemistry Lab', 1510, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (3, 'Westmont', 'Porter Theatre', 138, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (4, 'Westmont', 'Whittier Hall', 1835, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (5, 'Westmont', 'Art Museum', 490, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (6, 'Westmont', 'Clark A', 841, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (7, 'Westmont', 'Ritchies', 1393, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (8, 'Westmont', 'Emerson Hall', 426, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (9, 'Westmont', 'Armington C', 669, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (10, 'Westmont', 'Deane Hall', 129, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (11, 'Westmont', 'Music Building', 778, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (12, 'Westmont', 'GLC East', 881, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (13, 'Westmont', 'Mail Center', 1526, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (14, 'Westmont', 'Murchison Gym', 1973, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (15, 'Westmont', 'Health', 1129, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (16, 'Westmont', 'Reynolds Hall', 435, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (17, 'Westmont', 'Bookstore', 946, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (18, 'Westmont', 'Winter Hall', 502, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (19, 'Westmont', 'Kerrwood Hall', 332, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (20, 'Westmont', 'Deane Chapel', 1028, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (21, 'Westmont', 'Voskuyl Library', 912, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (22, 'Westmont', ' GLC West', 1046, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (23, 'Westmont', 'Clark Hall', 790, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (24, 'Westmont', 'Center', 600, TRUE);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (25, 'Westmont', 'DC', 1417, TRUE);

INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 41, 3677, 75, 1, 24, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 99, 1729, 156, 2, 2, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 50, 1090, 259, 3, 22, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 17, 3069, 355, 4, 13, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 44, 2065, 98, 5, 25, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 85, 3826, 98, 6, 22, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 71, 741, 123, 7, 13, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 43, 540, 214, 8, 4, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 37, 1654, 109, 9, 11, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 76, 3088, 291, 10, 18, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 85, 2911, 193, 11, 6, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 91, 1069, 149, 12, 13, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 61, 4039, 129, 13, 25, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 11, 674, 272, 14, 12, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 100, 3091, 177, 15, 20, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 40, 3256, 174, 16, 4, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 99, 2323, 177, 17, 3, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 64, 3592, 125, 18, 14, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 32, 1018, 339, 19, 6, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 51, 2255, 141, 20, 18, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 60, 3197, 63, 21, 1, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 61, 959, 302, 22, 17, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 39, 3833, 358, 23, 11, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 28, 366, 273, 24, 17, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 52, 753, 83, 25, 5, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 45, 2103, 75, 26, 15, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 21, 1548, 154, 27, 16, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 37, 3901, 314, 28, 14, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 48, 4097, 287, 29, 2, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 17, 2121, 204, 30, 19, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 38, 2746, 218, 31, 5, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 54, 1019, 299, 32, 11, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 59, 3825, 360, 33, 20, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 68, 4078, 247, 34, 17, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 17, 2482, 118, 35, 3, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 83, 714, 134, 36, 20, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 35, 1783, 290, 37, 16, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 34, 4142, 273, 38, 6, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 40, 2416, 145, 39, 23, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 92, 4198, 236, 40, 12, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 83, 3055, 129, 41, 16, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 97, 914, 214, 42, 24, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 35, 1609, 71, 43, 5, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 53, 1992, 145, 44, 1, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 69, 1540, 100, 45, 18, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 92, 1375, 184, 46, 7, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 95, 3273, 131, 47, 15, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 41, 1323, 126, 48, 4, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 25, 2110, 128, 49, 19, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 68, 3452, 316, 50, 10, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 29, 3581, 307, 51, 9, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 70, 3923, 98, 52, 8, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 66, 3470, 166, 53, 7, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 80, 2981, 240, 54, 23, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 12, 610, 175, 55, 19, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 54, 3855, 191, 56, 9, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 44, 2342, 355, 57, 8, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 25, 335, 352, 58, 9, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 96, 1277, 84, 59, 12, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 31, 1653, 155, 60, 15, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 50, 1810, 154, 61, 21, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 57, 1715, 215, 62, 8, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 16, 1824, 318, 63, 10, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 28, 3924, 332, 64, 1, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 34, 1244, 345, 65, 3, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 50, 3882, 259, 66, 7, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (FALSE, 83, 2968, 163, 67, 21, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 23, 2700, 191, 68, 14, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 76, 1394, 339, 69, 2, TRUE);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (TRUE, 15, 2083, 203, 70, 10, TRUE);

INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (1, 'ARM', 0, 0, 55, 'Mac', 'Laptop', 1704, TRUE, 21);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (2, 'Apple M', 0, 1532, 19, 'Linux', 'Laptop', 752, TRUE, 20);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (3, 'AMD', 0, 2406, 142, 'Mac', 'Laptop', 105, TRUE, 9);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (4, 'Intel', 0, 0, 83, 'Mac', 'Laptop', 347, TRUE, 25);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (5, 'ARM', 0, 0, 86, 'Linux', 'Desktop', 1518, TRUE, 13);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (6, 'Intel', 11, 0, 61, 'Mac', 'Laptop', 1611, TRUE, 10);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (7, 'ARM', 0, 0, 122, 'Windows', 'Laptop', 579, TRUE, 7);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (8, 'ARM', 12, 1101, 55, 'Linux', 'Desktop', 227, TRUE, 19);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (9, 'AMD', 0, 4189, 183, 'Linux', 'Laptop', 1571, TRUE, 5);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (10, 'ARM', 0, 4319, 72, 'Linux', 'Laptop', 806, TRUE, 14);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (11, 'ARM', 15, 1189, 186, 'Windows', 'Desktop', 1953, TRUE, 20);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (12, 'AMD', 17, 3717, 132, 'Linux', 'Desktop', 827, TRUE, 22);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (13, 'Intel', 15, 0, 97, 'Windows', 'Laptop', 1194, TRUE, 11);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (14, 'Apple M', 0, 3747, 64, 'Linux', 'Desktop', 1083, TRUE, 14);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (15, 'Intel', 0, 0, 164, 'Windows', 'Laptop', 1870, TRUE, 25);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (16, 'AMD', 11, 2540, 171, 'Linux', 'Laptop', 1317, TRUE, 10);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (17, 'Intel', 0, 0, 240, 'Windows', 'Laptop', 899, TRUE, 15);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (18, 'AMD', 17, 0, 220, 'Windows', 'Desktop', 1724, TRUE, 4);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (19, 'ARM', 13, 4278, 157, 'Windows', 'Desktop', 1695, TRUE, 11);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (20, 'ARM', 13, 0, 215, 'Windows', 'Desktop', 1513, TRUE, 18);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (21, 'AMD', 0, 0, 245, 'Windows', 'Laptop', 566, TRUE, 7);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (22, 'ARM', 16, 3232, 76, 'Linux', 'Desktop', 1364, TRUE, 21);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (23, 'Intel', 11, 3429, 94, 'Mac', 'Desktop', 2019, TRUE, 3);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (24, 'AMD', 15, 3287, 174, 'Windows', 'Desktop', 1848, TRUE, 15);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (25, 'ARM', 0, 0, 67, 'Mac', 'Desktop', 1243, TRUE, 22);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (26, 'AMD', 0, 3828, 85, 'Windows', 'Desktop', 781, TRUE, 5);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (27, 'ARM', 16, 0, 209, 'Mac', 'Desktop', 1681, TRUE, 2);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (28, 'Apple M', 0, 0, 213, 'Windows', 'Laptop', 1922, TRUE, 1);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (29, 'Intel', 15, 2774, 154, 'Windows', 'Laptop', 1665, TRUE, 8);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (30, 'ARM', 17, 0, 191, 'Windows', 'Laptop', 1915, TRUE, 12);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (31, 'Intel', 13, 3564, 82, 'Mac', 'Desktop', 1067, TRUE, 1);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (32, 'Intel', 17, 0, 108, 'Windows', 'Desktop', 1079, TRUE, 2);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (33, 'ARM', 0, 0, 145, 'Mac', 'Desktop', 1760, TRUE, 23);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (34, 'ARM', 0, 0, 241, 'Linux', 'Laptop', 565, TRUE, 6);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (35, 'ARM', 12, 2417, 95, 'Windows', 'Desktop', 1639, TRUE, 23);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (36, 'Intel', 12, 2908, 225, 'Mac', 'Laptop', 1624, TRUE, 24);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (37, 'AMD', 0, 0, 241, 'Linux', 'Desktop', 394, TRUE, 13);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (38, 'ARM', 16, 1109, 237, 'Windows', 'Desktop', 1614, TRUE, 4);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (39, 'Apple M', 17, 0, 35, 'Mac', 'Laptop', 1355, TRUE, 16);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (40, 'ARM', 0, 4303, 227, 'Linux', 'Desktop', 1200, TRUE, 2);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (41, 'ARM', 15, 0, 98, 'Linux', 'Laptop', 1887, TRUE, 14);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (42, 'Intel', 13, 1896, 60, 'Windows', 'Laptop', 269, TRUE, 15);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (43, 'ARM', 17, 0, 70, 'Mac', 'Laptop', 363, TRUE, 18);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (44, 'Intel', 13, 2617, 115, 'Windows', 'Desktop', 1695, TRUE, 5);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (45, 'AMD', 0, 0, 186, 'Windows', 'Laptop', 662, TRUE, 17);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (46, 'AMD', 15, 0, 236, 'Linux', 'Laptop', 818, TRUE, 7);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (47, 'Intel', 13, 0, 159, 'Linux', 'Laptop', 1831, TRUE, 21);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (48, 'ARM', 16, 0, 117, 'Windows', 'Desktop', 2030, TRUE, 17);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (49, 'Intel', 0, 0, 83, 'Windows', 'Desktop', 572, TRUE, 16);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (50, 'ARM', 0, 2580, 150, 'Linux', 'Desktop', 1387, TRUE, 17);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (51, 'AMD', 11, 3555, 209, 'Linux', 'Desktop', 1580, TRUE, 3);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (52, 'Intel', 14, 2339, 141, 'Linux', 'Desktop', 474, TRUE, 6);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (53, 'Intel', 15, 0, 238, 'Mac', 'Desktop', 1509, TRUE, 25);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (54, 'AMD', 17, 2991, 93, 'Windows', 'Laptop', 1001, TRUE, 19);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (55, 'Intel', 0, 1890, 208, 'Windows', 'Laptop', 385, TRUE, 3);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (56, 'AMD', 0, 0, 157, 'Windows', 'Desktop', 1890, TRUE, 23);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (57, 'Apple M', 11, 1816, 108, 'Windows', 'Laptop', 566, TRUE, 12);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (58, 'ARM', 17, 0, 216, 'Linux', 'Laptop', 364, TRUE, 13);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (59, 'Apple M', 14, 3860, 40, 'Linux', 'Laptop', 274, TRUE, 8);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (60, 'Apple M', 16, 0, 134, 'Mac', 'Desktop', 184, TRUE, 10);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (61, 'Intel', 0, 0, 225, 'Linux', 'Laptop', 320, TRUE, 8);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (62, 'ARM', 0, 1297, 28, 'Windows', 'Laptop', 1743, TRUE, 22);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (63, 'Intel', 0, 3690, 162, 'Windows', 'Desktop', 955, TRUE, 6);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (64, 'Apple M', 0, 1708, 63, 'Mac', 'Desktop', 1486, TRUE, 9);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (65, 'ARM', 12, 3313, 102, 'Mac', 'Laptop', 93, TRUE, 9);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (66, 'AMD', 13, 0, 118, 'Linux', 'Desktop', 1820, TRUE, 4);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (67, 'AMD', 0, 0, 28, 'Linux', 'Laptop', 747, TRUE, 20);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (68, 'AMD', 13, 3758, 19, 'Windows', 'Laptop', 515, TRUE, 18);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (69, 'Apple M', 0, 0, 159, 'Windows', 'Laptop', 470, TRUE, 19);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (70, 'ARM', 12, 0, 220, 'Mac', 'Laptop', 1020, TRUE, 16);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (71, 'ARM', 16, 4089, 132, 'Windows', 'Desktop', 1544, TRUE, 12);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (72, 'Apple M', 15, 0, 95, 'Windows', 'Desktop', 1073, TRUE, 24);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (73, 'AMD', 0, 0, 209, 'Linux', 'Laptop', 361, TRUE, 4);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (74, 'ARM', 0, 3625, 65, 'Mac', 'Laptop', 164, TRUE, 24);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (75, 'AMD', 0, 0, 42, 'Windows', 'Laptop', 1672, TRUE, 3);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (76, 'Intel', 17, 0, 226, 'Linux', 'Laptop', 1104, TRUE, 11);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (77, 'Intel', 0, 1974, 80, 'Mac', 'Desktop', 1600, TRUE, 1);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (78, 'ARM', 0, 2724, 28, 'Mac', 'Laptop', 1449, TRUE, 1);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (79, 'AMD', 0, 0, 100, 'Windows', 'Laptop', 1996, TRUE, 5);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (80, 'AMD', 0, 2169, 122, 'Linux', 'Laptop', 414, TRUE, 2);

INSERT INTO department (dept_id, name, head, division, is_active) VALUES (1, 'Admissions', 'Mr. Johan Botsford DVM', 'admin', TRUE);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (2, 'English', 'Davon Leffler', 'academic', TRUE);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (3, 'Data Analytics', 'Ara Koelpin', 'academic', TRUE);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (4, 'Dining Services', 'Kendall Fadel', 'admin', TRUE);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (5, 'Theatre', 'Jewel Wolff', 'academic', TRUE);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (6, 'Computer Science', 'Prof. Otha McKenzie', 'academic', TRUE);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (7, 'Philosophy', 'Chris Gislason', 'academic', TRUE);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (8, 'Political Science', 'Rosa Littel III', 'academic', TRUE);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (9, 'Mathematics', 'Imogene Toy', 'academic', TRUE);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (10, 'Physics', 'Aaron Stanton MD', 'academic', TRUE);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (11, 'Communications', 'Dr. Willow Barton', 'acadmic', TRUE);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (12, 'Kinesiology', 'Myles Johnson', 'academic', TRUE);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (13, 'Dining Services', 'Eula Tromp', 'admin', TRUE);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (14, 'Campus Safety', 'Kelley Hayes', 'admin', TRUE);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (15, 'History', 'Monty Conroy', 'admin', TRUE);

INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14403, NULL, NULL, 1247, 2013, '2018-10-06 17:52:05', 'Windows', TRUE, 1, 1, 1, 1, 1, 1);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21119, NULL, 38, NULL, 2013, '2020-07-07 08:26:34', 'Windows', TRUE, 2, 2, 2, 2, 2, 2);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21359, 'AMD', 17, 827, 2014, '2021-06-06 22:27:56', 'Mac', TRUE, 3, 3, 3, 3, 3, 3);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28640, 'AMD', 54, NULL, 2014, '2018-12-04 08:37:17', 'Mac', TRUE, 4, 4, 4, 4, 4, 4);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31727, 'Intel', NULL, 0, 2015, '2020-12-26 02:34:32', 'Windows', TRUE, 5, 5, 5, 5, 5, 5);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19694, NULL, 113, NULL, 2015, '2018-02-26 08:21:51', 'Windows', TRUE, 6, 6, 6, 6, 6, 6);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25543, 'Intel', 43, 2020, 2014, '2019-11-16 20:48:40', 'Linux', TRUE, 7, 7, 7, 7, 7, 7);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20912, NULL, NULL, 1486, 2015, '2018-01-16 17:41:18', 'Linux', TRUE, 8, 8, 8, 8, 8, 8);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10296, 'Intel', NULL, 1192, 2016, '2020-06-09 19:48:40', 'Mac', TRUE, 9, 9, 9, 9, 9, 9);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12312, NULL, NULL, 676, 2013, '2020-08-29 03:15:21', 'Windows', TRUE, 10, 10, 10, 10, 10, 10);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15894, 'Intel', 30, 111, 2015, '2019-10-10 00:44:29', 'Mac', TRUE, 11, 11, 11, 11, 11, 11);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25991, 'ARM', NULL, 1536, 2013, '2018-08-21 17:21:45', 'Mac', TRUE, 12, 12, 12, 12, 12, 12);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22493, 'AMD', NULL, 1346, 2012, '2019-11-05 07:28:24', 'Windows', TRUE, 13, 13, 13, 13, 13, 13);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26894, NULL, 175, 501, 2016, '2021-09-03 14:19:05', 'Windows', TRUE, 14, 14, 14, 14, 14, 14);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27236, 'AMD', 139, 1927, 2011, '2020-08-13 05:40:56', 'Mac', TRUE, 15, 15, 15, 15, 15, 15);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22205, 'Intel', NULL, 522, 2016, '2019-12-16 05:15:28', 'Windows', TRUE, 16, 16, 16, 16, 16, 16);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18263, 'AMD', NULL, 0, 2015, '2021-08-18 12:33:23', 'Linux', TRUE, 17, 17, 17, 17, 17, 17);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10045, 'Intel', NULL, 0, 2014, '2018-03-08 01:35:04', 'Linux', TRUE, 18, 18, 18, 18, 18, 18);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18936, NULL, NULL, 0, 2014, '2018-02-24 08:47:08', 'Windows', TRUE, 19, 19, 19, 19, 19, 19);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23947, 'ARM', 174, NULL, 2012, '2018-06-18 11:04:50', 'Windows', TRUE, 20, 20, 20, 20, 20, 20);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32432, NULL, 135, 1389, 2012, '2019-08-14 00:20:43', 'Mac', TRUE, 21, 21, 21, 21, 21, 21);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13249, 'ARM', 114, 1653, 2016, '2019-11-05 22:09:41', 'Windows', TRUE, 22, 22, 22, 22, 22, 22);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38543, 'Intel', NULL, 0, 2016, '2021-04-12 12:36:36', 'Linux', TRUE, 23, 23, 23, 23, 23, 23);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12935, NULL, 64, NULL, 2015, '2020-06-09 14:06:46', 'Windows', TRUE, 24, 24, 24, 24, 24, 24);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12035, 'AMD', NULL, 0, 2015, '2018-01-25 13:20:46', 'Mac', TRUE, 25, 25, 25, 25, 25, 25);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36669, NULL, 66, 1544, 2013, '2019-03-14 14:59:44', 'Mac', TRUE, 26, 26, 26, 26, 26, 26);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38894, NULL, NULL, 189, 2015, '2018-04-12 07:26:06', 'Windows', TRUE, 27, 27, 27, 27, 27, 27);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14556, NULL, 52, NULL, 2014, '2018-07-14 15:05:18', 'Windows', TRUE, 28, 28, 28, 28, 28, 28);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33244, NULL, 195, 2015, 2014, '2021-07-16 09:19:53', 'Linux', TRUE, 29, 29, 29, 29, 29, 29);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28718, NULL, 127, NULL, 2015, '2021-11-26 20:22:18', 'Windows', TRUE, 30, 30, 30, 30, 30, 30);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19183, NULL, 86, NULL, 2016, '2021-02-13 07:19:16', 'Windows', TRUE, 31, 31, 31, 31, 31, 31);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22261, NULL, 81, NULL, 2012, '2019-05-25 13:12:10', 'Linux', TRUE, 32, 32, 32, 32, 32, 32);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11721, NULL, NULL, 1779, 2015, '2018-07-30 05:16:30', 'Mac', TRUE, 33, 33, 33, 33, 33, 33);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18479, NULL, NULL, 0, 2012, '2021-05-02 05:18:17', 'Linux', TRUE, 34, 34, 34, 34, 34, 34);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21772, NULL, 256, NULL, 2012, '2020-09-29 10:06:46', 'Linux', TRUE, 35, 35, 35, 35, 35, 35);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28329, 'AMD', NULL, 1308, 2015, '2019-08-14 22:36:01', 'Windows', TRUE, 36, 36, 36, 36, 36, 36);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28500, 'AMD', 148, 605, 2013, '2017-12-26 15:09:38', 'Mac', TRUE, 37, 37, 37, 37, 37, 37);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35027, 'Intel', 211, NULL, 2011, '2021-04-23 07:46:23', 'Mac', TRUE, 38, 38, 38, 38, 38, 38);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26183, NULL, NULL, 0, 2014, '2021-05-05 23:41:47', 'Mac', TRUE, 39, 39, 39, 39, 39, 39);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33296, 'Intel', NULL, 1867, 2013, '2019-01-18 01:36:15', 'Windows', TRUE, 40, 40, 40, 40, 40, 40);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21125, 'Intel', NULL, 1204, 2014, '2019-11-30 13:17:36', 'Windows', TRUE, 41, 41, 41, 41, 41, 41);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22815, 'Intel', 249, NULL, 2013, '2021-07-28 02:02:22', 'Mac', TRUE, 42, 42, 42, 42, 42, 42);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21008, 'AMD', NULL, 314, 2015, '2020-08-18 08:13:35', 'Mac', TRUE, 43, 43, 43, 43, 43, 43);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38424, NULL, NULL, 0, 2012, '2019-01-01 03:45:44', 'Mac', TRUE, 44, 44, 44, 44, 44, 44);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20450, NULL, NULL, 1695, 2012, '2021-11-03 18:51:51', 'Windows', TRUE, 45, 45, 45, 45, 45, 45);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28279, NULL, NULL, 0, 2014, '2020-10-10 19:50:57', 'Mac', TRUE, 46, 46, 46, 46, 46, 46);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31130, 'ARM', NULL, 0, 2012, '2021-01-05 20:00:09', 'Mac', TRUE, 47, 47, 47, 47, 47, 47);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11702, 'AMD', NULL, 0, 2013, '2020-03-25 05:42:15', 'Windows', TRUE, 48, 48, 48, 48, 48, 48);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13339, NULL, NULL, 0, 2013, '2020-04-27 23:04:41', 'Linux', TRUE, 49, 49, 49, 49, 49, 49);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16284, NULL, 68, NULL, 2016, '2020-04-16 21:32:48', 'Mac', TRUE, 50, 50, 50, 50, 50, 50);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36239, 'ARM', 190, 1429, 2012, '2021-05-30 16:32:06', 'Mac', TRUE, 51, 51, 51, 51, 51, 51);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36049, NULL, NULL, 0, 2015, '2020-07-12 00:18:30', 'Mac', TRUE, 52, 52, 52, 52, 52, 52);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27017, NULL, NULL, 795, 2012, '2020-05-15 01:43:14', 'Windows', TRUE, 53, 53, 53, 53, 53, 53);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17333, NULL, 11, NULL, 2015, '2021-08-06 16:23:26', 'Mac', TRUE, 54, 54, 54, 54, 54, 54);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25640, 'Intel', 141, NULL, 2013, '2021-03-09 16:17:36', 'Linux', TRUE, 55, 55, 55, 55, 55, 55);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24221, 'Intel', 92, 1324, 2013, '2019-12-23 21:19:48', 'Windows', TRUE, 56, 56, 56, 56, 56, 56);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16737, 'ARM', NULL, 1475, 2016, '2018-06-20 12:07:39', 'Linux', TRUE, 57, 57, 57, 57, 57, 57);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16702, NULL, 161, NULL, 2015, '2018-11-16 09:25:49', 'Windows', TRUE, 58, 58, 58, 58, 58, 58);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10167, NULL, 175, NULL, 2015, '2020-12-04 20:45:32', 'Mac', TRUE, 59, 59, 59, 59, 59, 59);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23585, NULL, 72, 115, 2014, '2020-05-23 10:23:00', 'Mac', TRUE, 60, 60, 60, 60, 60, 60);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19932, NULL, 185, 1665, 2016, '2020-11-02 07:18:01', 'Linux', TRUE, 61, 61, 61, 61, 61, 61);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12873, 'Intel', 227, NULL, 2014, '2019-03-30 03:31:02', 'Mac', TRUE, 62, 62, 62, 62, 62, 62);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27295, NULL, NULL, 0, 2012, '2020-06-19 12:50:09', 'Linux', TRUE, 63, 63, 63, 63, 63, 63);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11561, 'ARM', NULL, 448, 2013, '2020-07-29 14:15:08', 'Linux', TRUE, 64, 64, 64, 64, 64, 64);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12707, NULL, 133, NULL, 2015, '2020-07-24 14:10:24', 'Windows', TRUE, 65, 65, 65, 65, 65, 65);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28907, 'Intel', NULL, 0, 2013, '2018-09-15 07:31:07', 'Linux', TRUE, 66, 66, 66, 66, 66, 66);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38960, NULL, 137, NULL, 2014, '2020-11-01 20:06:08', 'Linux', TRUE, 67, 67, 67, 67, 67, 67);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34551, 'ARM', NULL, 0, 2015, '2018-03-19 17:03:50', 'Mac', TRUE, 68, 68, 68, 68, 68, 68);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11614, NULL, 93, 1606, 2015, '2020-05-21 08:59:33', 'Mac', TRUE, 69, 69, 69, 69, 69, 69);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16439, 'ARM', NULL, 665, 2011, '2020-02-03 01:16:27', 'Linux', TRUE, 70, 70, 70, 70, 70, 70);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37599, 'ARM', 22, NULL, 2012, '2021-11-03 01:14:15', 'Linux', TRUE, 1, 1, 1, 1, 1, 71);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16882, 'AMD', 255, NULL, 2012, '2021-08-09 02:10:26', 'Linux', TRUE, 2, 2, 2, 2, 2, 72);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16500, NULL, 70, NULL, 2016, '2020-09-29 02:58:33', 'Windows', TRUE, 3, 3, 3, 3, 3, 73);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24654, NULL, 154, 503, 2015, '2021-08-16 20:50:05', 'Windows', TRUE, 4, 4, 4, 4, 4, 74);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26673, NULL, NULL, 0, 2012, '2019-10-08 02:50:37', 'Mac', TRUE, 5, 5, 5, 5, 5, 75);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25068, NULL, 212, 1463, 2014, '2021-08-06 19:37:28', 'Windows', TRUE, 6, 6, 6, 6, 6, 76);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10133, 'Intel', NULL, 0, 2015, '2020-05-29 11:56:36', 'Mac', TRUE, 7, 7, 7, 7, 7, 77);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23417, NULL, NULL, 849, 2015, '2019-01-20 19:05:38', 'Windows', TRUE, 8, 8, 8, 8, 8, 78);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14507, NULL, 218, NULL, 2013, '2020-02-14 02:36:46', 'Mac', TRUE, 9, 9, 9, 9, 9, 79);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35678, 'ARM', NULL, 0, 2016, '2019-03-07 14:01:52', 'Mac', TRUE, 10, 10, 10, 10, 10, 80);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33111, 'AMD', 86, 1885, 2016, '2019-05-17 13:05:53', 'Linux', TRUE, 11, 11, 11, 11, 11, 1);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35916, NULL, NULL, 1751, 2016, '2018-06-11 11:49:24', 'Windows', TRUE, 12, 12, 12, 12, 12, 2);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15113, NULL, 58, 399, 2015, '2018-12-27 13:11:03', 'Mac', TRUE, 13, 13, 13, 13, 13, 3);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13424, 'AMD', NULL, 0, 2016, '2020-08-23 13:11:15', 'Mac', TRUE, 14, 14, 14, 14, 14, 4);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21499, NULL, 143, NULL, 2016, '2020-06-17 15:07:34', 'Windows', TRUE, 15, 15, 15, 15, 15, 5);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34421, 'Intel', 142, NULL, 2012, '2021-09-16 03:31:54', 'Mac', TRUE, 16, 16, 16, 16, 16, 6);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34266, 'AMD', NULL, 1086, 2013, '2018-05-27 02:45:23', 'Windows', TRUE, 17, 17, 17, 17, 17, 7);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29564, NULL, NULL, 1391, 2012, '2019-09-26 14:28:37', 'Mac', TRUE, 18, 18, 18, 18, 18, 8);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21370, 'AMD', 152, 1727, 2013, '2021-07-25 09:15:13', 'Windows', TRUE, 19, 19, 19, 19, 19, 9);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10741, NULL, NULL, 0, 2016, '2021-09-29 05:34:02', 'Windows', TRUE, 20, 20, 20, 20, 20, 10);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19186, NULL, NULL, 0, 2014, '2019-10-16 05:47:33', 'Windows', TRUE, 21, 21, 21, 21, 21, 11);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12953, 'ARM', 59, NULL, 2012, '2021-11-25 02:09:25', 'Windows', TRUE, 22, 22, 22, 22, 22, 12);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24517, 'ARM', 219, NULL, 2014, '2020-08-18 01:10:13', 'Linux', TRUE, 23, 23, 23, 23, 23, 13);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30960, 'ARM', NULL, 1856, 2015, '2018-10-22 15:50:34', 'Mac', TRUE, 24, 24, 24, 24, 24, 14);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14092, NULL, 118, NULL, 2013, '2021-04-15 21:19:31', 'Windows', TRUE, 25, 25, 25, 25, 25, 15);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35903, NULL, NULL, 943, 2014, '2020-02-21 21:42:13', 'Linux', TRUE, 26, 26, 26, 26, 26, 16);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12225, NULL, 21, 356, 2016, '2018-11-15 01:54:02', 'Windows', TRUE, 27, 27, 27, 27, 27, 17);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19038, 'Intel', NULL, 0, 2012, '2021-02-19 00:59:41', 'Mac', TRUE, 28, 28, 28, 28, 28, 18);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32974, 'AMD', 64, NULL, 2013, '2019-09-29 18:25:53', 'Mac', TRUE, 29, 29, 29, 29, 29, 19);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34143, 'Intel', NULL, 1707, 2015, '2021-10-17 19:43:03', 'Windows', TRUE, 30, 30, 30, 30, 30, 20);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10449, 'ARM', 133, NULL, 2015, '2021-05-23 07:33:27', 'Linux', TRUE, 31, 31, 31, 31, 31, 21);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12763, 'ARM', 162, 1699, 2016, '2020-01-15 21:38:25', 'Mac', TRUE, 32, 32, 32, 32, 32, 22);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25959, NULL, 128, NULL, 2013, '2021-03-20 10:05:32', 'Mac', TRUE, 33, 33, 33, 33, 33, 23);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38120, NULL, 196, 1889, 2016, '2019-03-25 23:00:26', 'Windows', TRUE, 34, 34, 34, 34, 34, 24);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10194, NULL, NULL, 1289, 2013, '2020-12-10 09:57:21', 'Linux', TRUE, 35, 35, 35, 35, 35, 25);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26127, 'AMD', 104, NULL, 2016, '2018-10-25 02:44:45', 'Linux', TRUE, 36, 36, 36, 36, 36, 26);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16448, NULL, NULL, 0, 2012, '2020-12-10 11:22:24', 'Linux', TRUE, 37, 37, 37, 37, 37, 27);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19993, NULL, NULL, 504, 2015, '2021-09-04 23:53:27', 'Linux', TRUE, 38, 38, 38, 38, 38, 28);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29865, 'Intel', NULL, 0, 2013, '2019-11-18 18:48:49', 'Windows', TRUE, 39, 39, 39, 39, 39, 29);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17378, NULL, NULL, 301, 2015, '2019-11-29 21:20:06', 'Mac', TRUE, 40, 40, 40, 40, 40, 30);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32617, 'ARM', NULL, 0, 2015, '2020-08-23 18:35:38', 'Linux', TRUE, 41, 41, 41, 41, 41, 31);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34919, 'AMD', 76, NULL, 2016, '2019-05-23 08:48:42', 'Windows', TRUE, 42, 42, 42, 42, 42, 32);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29833, NULL, 128, 1040, 2015, '2020-05-20 19:01:42', 'Linux', TRUE, 43, 43, 43, 43, 43, 33);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18844, 'Intel', NULL, 731, 2015, '2018-03-24 01:29:03', 'Linux', TRUE, 44, 44, 44, 44, 44, 34);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30455, NULL, 71, NULL, 2016, '2020-03-04 06:51:17', 'Linux', TRUE, 45, 45, 45, 45, 45, 35);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12265, NULL, NULL, 1587, 2015, '2018-09-24 03:10:25', 'Linux', TRUE, 46, 46, 46, 46, 46, 36);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34758, 'AMD', 129, NULL, 2014, '2021-10-18 17:58:06', 'Linux', TRUE, 47, 47, 47, 47, 47, 37);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23928, NULL, NULL, 1524, 2016, '2021-06-20 20:50:40', 'Mac', TRUE, 48, 48, 48, 48, 48, 38);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26392, NULL, 210, 906, 2015, '2021-07-25 11:14:35', 'Linux', TRUE, 49, 49, 49, 49, 49, 39);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38945, 'AMD', 251, 907, 2014, '2017-12-28 08:24:32', 'Windows', TRUE, 50, 50, 50, 50, 50, 40);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25435, NULL, 149, 676, 2013, '2020-09-20 06:47:51', 'Linux', TRUE, 51, 51, 51, 51, 51, 41);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26762, NULL, NULL, 1145, 2015, '2021-05-08 22:14:45', 'Mac', TRUE, 52, 52, 52, 52, 52, 42);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34661, NULL, NULL, 0, 2012, '2021-08-30 20:35:24', 'Mac', TRUE, 53, 53, 53, 53, 53, 43);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34104, NULL, NULL, 0, 2015, '2018-10-08 01:01:22', 'Windows', TRUE, 54, 54, 54, 54, 54, 44);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17630, 'AMD', NULL, 280, 2012, '2020-03-22 11:10:39', 'Mac', TRUE, 55, 55, 55, 55, 55, 45);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31651, NULL, 144, 355, 2013, '2019-03-12 04:08:17', 'Linux', TRUE, 56, 56, 56, 56, 56, 46);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38770, 'ARM', NULL, 0, 2015, '2020-04-18 15:24:45', 'Linux', TRUE, 57, 57, 57, 57, 57, 47);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19252, 'AMD', NULL, 766, 2015, '2020-11-20 14:12:16', 'Mac', TRUE, 58, 58, 58, 58, 58, 48);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19860, 'AMD', 235, NULL, 2013, '2018-09-22 02:55:14', 'Linux', TRUE, 59, 59, 59, 59, 59, 49);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28221, NULL, 222, 1927, 2013, '2020-12-29 20:20:07', 'Linux', TRUE, 60, 60, 60, 60, 60, 50);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28996, NULL, 39, NULL, 2015, '2020-10-30 23:20:14', 'Mac', TRUE, 61, 61, 61, 61, 61, 51);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34525, 'Intel', NULL, 0, 2013, '2020-02-16 15:14:41', 'Mac', TRUE, 62, 62, 62, 62, 62, 52);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16610, 'ARM', 131, 1947, 2013, '2021-05-14 15:01:06', 'Mac', TRUE, 63, 63, 63, 63, 63, 53);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20246, NULL, NULL, 1865, 2016, '2019-02-04 10:29:36', 'Mac', TRUE, 64, 64, 64, 64, 64, 54);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31240, 'ARM', NULL, 2023, 2012, '2021-04-29 02:43:53', 'Windows', TRUE, 65, 65, 65, 65, 65, 55);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13377, 'AMD', NULL, 1437, 2015, '2020-01-04 23:47:28', 'Linux', TRUE, 66, 66, 66, 66, 66, 56);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38419, 'Intel', NULL, 0, 2013, '2020-06-05 06:42:17', 'Linux', TRUE, 67, 67, 67, 67, 67, 57);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19669, 'ARM', 161, 1466, 2015, '2019-07-01 19:19:28', 'Linux', TRUE, 68, 68, 68, 68, 68, 58);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23172, 'Intel', 14, NULL, 2011, '2019-06-25 09:54:58', 'Windows', TRUE, 69, 69, 69, 69, 69, 59);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14962, NULL, NULL, 465, 2012, '2019-01-31 00:10:37', 'Mac', TRUE, 70, 70, 70, 70, 70, 60);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35846, NULL, NULL, 0, 2015, '2019-06-18 08:10:03', 'Windows', TRUE, 1, 1, 1, 1, 1, 61);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21093, NULL, 92, 914, 2013, '2018-02-15 17:20:59', 'Mac', TRUE, 2, 2, 2, 2, 2, 62);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11494, 'AMD', 47, NULL, 2015, '2020-09-29 19:10:05', 'Windows', TRUE, 3, 3, 3, 3, 3, 63);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37202, NULL, NULL, 0, 2015, '2019-10-19 05:15:31', 'Windows', TRUE, 4, 4, 4, 4, 4, 64);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14391, NULL, 119, 265, 2015, '2019-04-28 19:31:37', 'Linux', TRUE, 5, 5, 5, 5, 5, 65);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27826, NULL, NULL, 2027, 2015, '2018-07-23 15:53:30', 'Windows', TRUE, 6, 6, 6, 6, 6, 66);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28217, 'Intel', NULL, 1484, 2013, '2019-06-18 20:22:19', 'Windows', TRUE, 7, 7, 7, 7, 7, 67);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11570, NULL, NULL, 1291, 2014, '2020-02-09 07:40:57', 'Mac', TRUE, 8, 8, 8, 8, 8, 68);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10569, 'Intel', 200, NULL, 2012, '2019-02-24 19:21:41', 'Mac', TRUE, 9, 9, 9, 9, 9, 69);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34511, 'Intel', NULL, 0, 2013, '2021-03-30 20:27:20', 'Linux', TRUE, 10, 10, 10, 10, 10, 70);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38557, NULL, 47, 1055, 2011, '2019-01-17 23:36:25', 'Mac', TRUE, 11, 11, 11, 11, 11, 71);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12637, NULL, NULL, 1922, 2013, '2020-09-18 23:43:27', 'Mac', TRUE, 12, 12, 12, 12, 12, 72);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23060, NULL, 68, NULL, 2016, '2019-03-08 18:23:52', 'Linux', TRUE, 13, 13, 13, 13, 13, 73);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21410, NULL, 243, NULL, 2015, '2020-10-08 13:00:54', 'Mac', TRUE, 14, 14, 14, 14, 14, 74);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38173, 'AMD', 109, NULL, 2014, '2020-12-04 09:09:49', 'Linux', TRUE, 15, 15, 15, 15, 15, 75);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18295, 'Intel', 49, NULL, 2013, '2021-11-17 11:02:42', 'Linux', TRUE, 16, 16, 16, 16, 16, 76);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28982, 'ARM', 36, NULL, 2016, '2018-12-23 00:32:41', 'Linux', TRUE, 17, 17, 17, 17, 17, 77);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23680, NULL, 154, NULL, 2014, '2021-11-04 19:41:04', 'Linux', TRUE, 18, 18, 18, 18, 18, 78);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13646, NULL, 210, NULL, 2014, '2019-08-03 14:55:28', 'Windows', TRUE, 19, 19, 19, 19, 19, 79);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34861, NULL, NULL, 0, 2015, '2018-10-31 09:39:24', 'Windows', TRUE, 20, 20, 20, 20, 20, 80);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35563, 'AMD', 165, 1668, 2013, '2020-05-17 19:01:10', 'Linux', TRUE, 21, 21, 21, 21, 21, 1);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30900, 'ARM', NULL, 0, 2015, '2018-09-23 16:50:51', 'Linux', TRUE, 22, 22, 22, 22, 22, 2);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32838, 'Intel', 160, NULL, 2012, '2020-01-16 02:50:36', 'Windows', TRUE, 23, 23, 23, 23, 23, 3);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33868, NULL, 149, NULL, 2012, '2018-07-13 05:28:14', 'Linux', TRUE, 24, 24, 24, 24, 24, 4);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31535, NULL, 181, 273, 2013, '2020-04-06 03:28:52', 'Mac', TRUE, 25, 25, 25, 25, 25, 5);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23449, 'Intel', NULL, 0, 2012, '2018-05-20 03:11:19', 'Mac', TRUE, 26, 26, 26, 26, 26, 6);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30134, NULL, NULL, 0, 2013, '2021-06-10 04:53:11', 'Linux', TRUE, 27, 27, 27, 27, 27, 7);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22844, 'Intel', 26, NULL, 2015, '2020-09-15 02:36:47', 'Windows', TRUE, 28, 28, 28, 28, 28, 8);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27052, 'ARM', NULL, 1303, 2013, '2019-04-21 19:07:48', 'Mac', TRUE, 29, 29, 29, 29, 29, 9);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37921, NULL, 220, NULL, 2014, '2021-11-06 00:07:30', 'Linux', TRUE, 30, 30, 30, 30, 30, 10);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35704, 'Intel', NULL, 185, 2012, '2020-10-07 20:02:02', 'Linux', TRUE, 31, 31, 31, 31, 31, 11);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23370, NULL, 179, NULL, 2013, '2020-06-28 14:51:30', 'Linux', TRUE, 32, 32, 32, 32, 32, 12);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26318, 'ARM', NULL, 850, 2013, '2018-10-26 23:45:01', 'Linux', TRUE, 33, 33, 33, 33, 33, 13);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36364, NULL, 249, 707, 2014, '2021-10-26 07:57:17', 'Windows', TRUE, 34, 34, 34, 34, 34, 14);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17083, NULL, 18, NULL, 2016, '2021-07-11 23:40:06', 'Windows', TRUE, 35, 35, 35, 35, 35, 15);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14347, 'Intel', NULL, 0, 2016, '2018-07-12 13:02:07', 'Mac', TRUE, 36, 36, 36, 36, 36, 16);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25943, 'ARM', NULL, 0, 2012, '2020-08-25 00:40:08', 'Mac', TRUE, 37, 37, 37, 37, 37, 17);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15400, NULL, NULL, 0, 2014, '2020-03-13 22:48:46', 'Mac', TRUE, 38, 38, 38, 38, 38, 18);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16721, NULL, 8, 727, 2013, '2018-01-15 14:16:01', 'Linux', TRUE, 39, 39, 39, 39, 39, 19);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37670, 'Intel', 233, NULL, 2012, '2020-02-22 06:27:16', 'Windows', TRUE, 40, 40, 40, 40, 40, 20);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35289, NULL, NULL, 0, 2015, '2018-04-30 14:06:11', 'Mac', TRUE, 41, 41, 41, 41, 41, 21);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26363, 'Intel', 78, 116, 2013, '2021-10-30 06:10:46', 'Mac', TRUE, 42, 42, 42, 42, 42, 22);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17714, 'ARM', 111, 1011, 2014, '2018-07-03 16:59:14', 'Linux', TRUE, 43, 43, 43, 43, 43, 23);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32977, NULL, 82, NULL, 2014, '2020-07-09 05:08:14', 'Linux', TRUE, 44, 44, 44, 44, 44, 24);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20601, 'ARM', NULL, 1178, 2015, '2021-06-27 15:46:38', 'Linux', TRUE, 45, 45, 45, 45, 45, 25);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18358, 'Intel', NULL, 1991, 2013, '2021-03-29 16:17:12', 'Mac', TRUE, 46, 46, 46, 46, 46, 26);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22903, 'Intel', NULL, 0, 2016, '2018-05-31 01:22:44', 'Windows', TRUE, 47, 47, 47, 47, 47, 27);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31785, NULL, 183, NULL, 2013, '2020-10-15 19:14:25', 'Windows', TRUE, 48, 48, 48, 48, 48, 28);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29192, 'Intel', NULL, 1342, 2016, '2020-06-01 13:11:32', 'Linux', TRUE, 49, 49, 49, 49, 49, 29);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37420, 'Intel', NULL, 1837, 2012, '2021-01-09 07:22:52', 'Windows', TRUE, 50, 50, 50, 50, 50, 30);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28403, NULL, NULL, 264, 2013, '2019-02-14 23:23:23', 'Mac', TRUE, 51, 51, 51, 51, 51, 31);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26254, 'AMD', NULL, 343, 2014, '2020-11-20 17:04:01', 'Linux', TRUE, 52, 52, 52, 52, 52, 32);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32704, 'ARM', NULL, 0, 2016, '2019-05-06 09:41:23', 'Windows', TRUE, 53, 53, 53, 53, 53, 33);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16137, NULL, 20, NULL, 2015, '2019-11-26 09:02:27', 'Mac', TRUE, 54, 54, 54, 54, 54, 34);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17852, NULL, 63, NULL, 2015, '2018-06-24 06:09:37', 'Mac', TRUE, 55, 55, 55, 55, 55, 35);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27203, 'Intel', 107, NULL, 2012, '2019-04-09 06:27:20', 'Linux', TRUE, 56, 56, 56, 56, 56, 36);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16102, 'Intel', 196, 782, 2016, '2021-04-18 23:46:45', 'Linux', TRUE, 57, 57, 57, 57, 57, 37);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17160, NULL, NULL, 0, 2016, '2018-04-07 01:52:38', 'Linux', TRUE, 58, 58, 58, 58, 58, 38);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26342, NULL, 65, NULL, 2016, '2020-09-09 21:42:36', 'Windows', TRUE, 59, 59, 59, 59, 59, 39);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15813, NULL, NULL, 1721, 2015, '2021-06-02 12:45:06', 'Linux', TRUE, 60, 60, 60, 60, 60, 40);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28435, 'ARM', 212, 792, 2012, '2018-09-15 03:27:53', 'Linux', TRUE, 61, 61, 61, 61, 61, 41);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25627, NULL, NULL, 1621, 2016, '2020-10-30 17:49:35', 'Mac', TRUE, 62, 62, 62, 62, 62, 42);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17162, 'Intel', NULL, 1468, 2012, '2021-03-18 14:26:54', 'Windows', TRUE, 63, 63, 63, 63, 63, 43);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24457, NULL, NULL, 0, 2012, '2019-01-23 21:50:28', 'Mac', TRUE, 64, 64, 64, 64, 64, 44);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36407, NULL, NULL, 1848, 2013, '2020-04-13 18:32:00', 'Windows', TRUE, 65, 65, 65, 65, 65, 45);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12381, 'ARM', 84, NULL, 2015, '2021-09-06 18:34:20', 'Windows', TRUE, 66, 66, 66, 66, 66, 46);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32260, NULL, NULL, 0, 2012, '2020-12-24 17:22:49', 'Linux', TRUE, 67, 67, 67, 67, 67, 47);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11747, NULL, 29, NULL, 2012, '2019-10-08 16:49:57', 'Linux', TRUE, 68, 68, 68, 68, 68, 48);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31183, NULL, NULL, 0, 2014, '2020-11-29 18:02:20', 'Linux', TRUE, 69, 69, 69, 69, 69, 49);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19367, 'Intel', NULL, 1719, 2012, '2020-11-21 06:26:59', 'Mac', TRUE, 70, 70, 70, 70, 70, 50);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31843, 'Intel', NULL, 0, 2016, '2020-09-01 18:33:23', 'Linux', TRUE, 1, 1, 1, 1, 1, 51);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32078, NULL, NULL, 0, 2014, '2018-07-15 02:22:26', 'Windows', TRUE, 2, 2, 2, 2, 2, 52);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31201, NULL, 169, 959, 2014, '2021-08-17 05:32:59', 'Linux', TRUE, 3, 3, 3, 3, 3, 53);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11317, NULL, NULL, 0, 2012, '2021-09-20 12:16:19', 'Mac', TRUE, 4, 4, 4, 4, 4, 54);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15939, 'ARM', NULL, 1652, 2014, '2019-12-03 03:20:52', 'Linux', TRUE, 5, 5, 5, 5, 5, 55);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11473, NULL, NULL, 393, 2015, '2019-07-12 14:04:10', 'Mac', TRUE, 6, 6, 6, 6, 6, 56);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30444, NULL, NULL, 0, 2016, '2021-01-04 07:37:16', 'Windows', TRUE, 7, 7, 7, 7, 7, 57);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32164, NULL, NULL, 1565, 2015, '2021-12-04 13:46:29', 'Windows', TRUE, 8, 8, 8, 8, 8, 58);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12114, NULL, NULL, 73, 2015, '2018-07-27 01:09:19', 'Windows', TRUE, 9, 9, 9, 9, 9, 59);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22682, 'AMD', 119, 191, 2015, '2018-07-27 09:07:39', 'Windows', TRUE, 10, 10, 10, 10, 10, 60);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11987, 'ARM', NULL, 0, 2013, '2018-07-22 08:00:29', 'Windows', TRUE, 11, 11, 11, 11, 11, 61);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28424, NULL, NULL, 0, 2013, '2021-02-20 03:04:11', 'Linux', TRUE, 12, 12, 12, 12, 12, 62);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29294, 'ARM', 154, NULL, 2016, '2020-11-17 20:24:04', 'Linux', TRUE, 13, 13, 13, 13, 13, 63);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38897, NULL, NULL, 781, 2014, '2018-07-19 23:16:58', 'Windows', TRUE, 14, 14, 14, 14, 14, 64);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16665, 'AMD', NULL, 1142, 2015, '2019-09-18 17:23:53', 'Mac', TRUE, 15, 15, 15, 15, 15, 65);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28680, 'ARM', NULL, 890, 2015, '2019-06-25 01:25:54', 'Mac', TRUE, 16, 16, 16, 16, 16, 66);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26808, NULL, 165, 1618, 2014, '2019-06-18 23:33:33', 'Linux', TRUE, 17, 17, 17, 17, 17, 67);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18440, 'AMD', 186, 1033, 2014, '2019-06-03 22:21:47', 'Linux', TRUE, 18, 18, 18, 18, 18, 68);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31978, NULL, NULL, 758, 2012, '2019-09-12 08:13:16', 'Linux', TRUE, 19, 19, 19, 19, 19, 69);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15096, NULL, 81, 1519, 2013, '2018-12-12 22:34:58', 'Linux', TRUE, 20, 20, 20, 20, 20, 70);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27617, 'ARM', NULL, 0, 2012, '2020-04-18 01:27:18', 'Windows', TRUE, 21, 21, 21, 21, 21, 71);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22386, 'AMD', NULL, 0, 2016, '2019-06-03 13:30:36', 'Linux', TRUE, 22, 22, 22, 22, 22, 72);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30051, 'AMD', NULL, 0, 2015, '2019-12-27 13:45:45', 'Linux', TRUE, 23, 23, 23, 23, 23, 73);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21605, NULL, 191, 1497, 2013, '2020-12-26 08:00:59', 'Mac', TRUE, 24, 24, 24, 24, 24, 74);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12900, NULL, 31, NULL, 2014, '2020-09-20 13:01:16', 'Linux', TRUE, 25, 25, 25, 25, 25, 75);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10264, NULL, 123, 242, 2012, '2018-12-16 11:24:20', 'Mac', TRUE, 26, 26, 26, 26, 26, 76);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11113, 'AMD', 85, NULL, 2014, '2021-07-28 18:12:12', 'Mac', TRUE, 27, 27, 27, 27, 27, 77);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19460, 'AMD', NULL, 1905, 2012, '2020-08-08 01:20:21', 'Windows', TRUE, 28, 28, 28, 28, 28, 78);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20968, NULL, NULL, 0, 2013, '2018-09-28 20:30:58', 'Linux', TRUE, 29, 29, 29, 29, 29, 79);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32767, NULL, 190, NULL, 2012, '2018-02-20 23:18:47', 'Linux', TRUE, 30, 30, 30, 30, 30, 80);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12444, 'ARM', 211, NULL, 2016, '2021-08-05 19:38:13', 'Mac', TRUE, 31, 31, 31, 31, 31, 1);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10149, 'Intel', 24, 189, 2014, '2021-04-02 22:48:54', 'Mac', TRUE, 32, 32, 32, 32, 32, 2);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16249, NULL, NULL, 0, 2012, '2018-01-04 05:46:07', 'Mac', TRUE, 33, 33, 33, 33, 33, 3);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24134, NULL, NULL, 887, 2014, '2018-02-27 02:35:38', 'Mac', TRUE, 34, 34, 34, 34, 34, 4);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30448, 'Intel', NULL, 0, 2014, '2021-07-11 02:46:37', 'Windows', TRUE, 35, 35, 35, 35, 35, 5);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30721, 'AMD', NULL, 0, 2012, '2018-01-02 23:20:09', 'Mac', TRUE, 36, 36, 36, 36, 36, 6);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28401, 'Intel', 37, NULL, 2014, '2019-01-05 15:51:42', 'Windows', TRUE, 37, 37, 37, 37, 37, 7);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24574, 'ARM', 244, NULL, 2014, '2021-09-05 17:22:56', 'Mac', TRUE, 38, 38, 38, 38, 38, 8);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33080, NULL, 51, NULL, 2012, '2018-08-11 05:04:43', 'Windows', TRUE, 39, 39, 39, 39, 39, 9);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25137, 'AMD', 172, 1067, 2014, '2018-03-31 09:33:05', 'Windows', TRUE, 40, 40, 40, 40, 40, 10);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14178, NULL, 176, 116, 2015, '2021-08-17 04:10:00', 'Linux', TRUE, 41, 41, 41, 41, 41, 11);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13695, NULL, 192, NULL, 2013, '2018-12-11 06:17:21', 'Windows', TRUE, 42, 42, 42, 42, 42, 12);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22510, 'AMD', NULL, 1445, 2015, '2019-11-18 08:22:45', 'Windows', TRUE, 43, 43, 43, 43, 43, 13);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18854, 'Intel', NULL, 878, 2013, '2021-11-02 05:40:52', 'Linux', TRUE, 44, 44, 44, 44, 44, 14);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35347, NULL, 201, 819, 2016, '2020-12-03 20:45:41', 'Mac', TRUE, 45, 45, 45, 45, 45, 15);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27622, 'ARM', NULL, 0, 2013, '2019-06-08 20:52:03', 'Windows', TRUE, 46, 46, 46, 46, 46, 16);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27262, 'Intel', NULL, 0, 2013, '2020-01-04 07:55:03', 'Mac', TRUE, 47, 47, 47, 47, 47, 17);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37215, NULL, 213, 220, 2012, '2018-06-16 16:17:55', 'Linux', TRUE, 48, 48, 48, 48, 48, 18);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18396, NULL, 124, NULL, 2015, '2020-02-10 08:02:56', 'Windows', TRUE, 49, 49, 49, 49, 49, 19);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36959, NULL, NULL, 1424, 2015, '2017-12-27 23:06:53', 'Mac', TRUE, 50, 50, 50, 50, 50, 20);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24584, NULL, NULL, 0, 2013, '2020-11-07 21:59:49', 'Mac', TRUE, 51, 51, 51, 51, 51, 21);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17461, 'ARM', NULL, 0, 2015, '2021-10-18 23:22:30', 'Mac', TRUE, 52, 52, 52, 52, 52, 22);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33057, 'ARM', NULL, 701, 2016, '2020-10-29 15:38:17', 'Windows', TRUE, 53, 53, 53, 53, 53, 23);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17633, 'AMD', 72, 845, 2013, '2021-01-18 18:12:52', 'Linux', TRUE, 54, 54, 54, 54, 54, 24);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28367, NULL, NULL, 1190, 2014, '2019-08-24 12:32:48', 'Linux', TRUE, 55, 55, 55, 55, 55, 25);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38882, NULL, 234, 164, 2016, '2021-09-29 19:29:10', 'Mac', TRUE, 56, 56, 56, 56, 56, 26);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29030, 'ARM', NULL, 1000, 2014, '2019-11-30 23:50:20', 'Linux', TRUE, 57, 57, 57, 57, 57, 27);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34296, NULL, 224, NULL, 2013, '2019-02-01 17:28:27', 'Windows', TRUE, 58, 58, 58, 58, 58, 28);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36711, NULL, NULL, 1140, 2015, '2018-06-09 13:18:07', 'Mac', TRUE, 59, 59, 59, 59, 59, 29);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12439, NULL, NULL, 163, 2016, '2018-05-09 23:55:56', 'Linux', TRUE, 60, 60, 60, 60, 60, 30);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35187, 'AMD', NULL, 1983, 2015, '2019-09-22 16:07:36', 'Windows', TRUE, 61, 61, 61, 61, 61, 31);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28720, 'Intel', 142, NULL, 2016, '2019-01-10 16:43:06', 'Linux', TRUE, 62, 62, 62, 62, 62, 32);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32958, 'ARM', NULL, 0, 2015, '2021-06-29 00:31:45', 'Windows', TRUE, 63, 63, 63, 63, 63, 33);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13874, 'AMD', 201, 76, 2016, '2021-03-06 01:28:03', 'Mac', TRUE, 64, 64, 64, 64, 64, 34);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32242, 'Intel', NULL, 0, 2015, '2021-08-21 11:16:46', 'Linux', TRUE, 65, 65, 65, 65, 65, 35);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34020, NULL, NULL, 1485, 2015, '2018-08-12 09:53:46', 'Mac', TRUE, 66, 66, 66, 66, 66, 36);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24619, 'AMD', 203, 1252, 2013, '2020-10-14 13:52:24', 'Mac', TRUE, 67, 67, 67, 67, 67, 37);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38996, NULL, NULL, 0, 2012, '2020-04-11 23:52:42', 'Linux', TRUE, 68, 68, 68, 68, 68, 38);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21584, NULL, 122, 766, 2015, '2021-02-03 09:49:48', 'Mac', TRUE, 69, 69, 69, 69, 69, 39);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31407, NULL, NULL, 542, 2015, '2018-01-08 10:21:13', 'Mac', TRUE, 70, 70, 70, 70, 70, 40);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17538, NULL, 157, NULL, 2015, '2021-03-06 13:54:12', 'Mac', TRUE, 1, 1, 1, 1, 1, 41);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36788, 'ARM', NULL, 0, 2013, '2020-09-26 05:31:57', 'Linux', TRUE, 2, 2, 2, 2, 2, 42);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20266, 'ARM', 73, 411, 2015, '2020-02-17 03:26:08', 'Windows', TRUE, 3, 3, 3, 3, 3, 43);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18877, 'Intel', 167, 702, 2013, '2019-10-29 13:26:18', 'Linux', TRUE, 4, 4, 4, 4, 4, 44);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28791, 'ARM', NULL, 783, 2012, '2019-08-19 14:06:21', 'Windows', TRUE, 5, 5, 5, 5, 5, 45);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11190, 'Intel', NULL, 0, 2016, '2018-07-29 20:08:55', 'Windows', TRUE, 6, 6, 6, 6, 6, 46);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33993, NULL, 191, NULL, 2016, '2021-05-29 14:42:23', 'Mac', TRUE, 7, 7, 7, 7, 7, 47);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30729, NULL, NULL, 0, 2014, '2020-09-22 07:03:30', 'Windows', TRUE, 8, 8, 8, 8, 8, 48);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17081, 'ARM', 130, 693, 2012, '2021-04-18 09:24:28', 'Windows', TRUE, 9, 9, 9, 9, 9, 49);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23223, NULL, 87, NULL, 2015, '2019-08-19 08:19:51', 'Mac', TRUE, 10, 10, 10, 10, 10, 50);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31285, 'Intel', 38, NULL, 2014, '2018-07-27 12:30:12', 'Windows', TRUE, 11, 11, 11, 11, 11, 51);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20869, NULL, NULL, 0, 2014, '2018-12-29 03:09:15', 'Windows', TRUE, 12, 12, 12, 12, 12, 52);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32375, NULL, 124, NULL, 2014, '2020-01-20 23:11:37', 'Mac', TRUE, 13, 13, 13, 13, 13, 53);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28411, NULL, 151, NULL, 2012, '2021-02-10 17:41:07', 'Mac', TRUE, 14, 14, 14, 14, 14, 54);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34174, 'Intel', 82, NULL, 2012, '2021-07-03 13:37:01', 'Linux', TRUE, 15, 15, 15, 15, 15, 55);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23044, NULL, NULL, 389, 2011, '2020-08-07 18:00:01', 'Linux', TRUE, 16, 16, 16, 16, 16, 56);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22150, NULL, NULL, 0, 2013, '2018-03-12 20:27:30', 'Mac', TRUE, 17, 17, 17, 17, 17, 57);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22695, 'AMD', 19, 1219, 2015, '2021-08-15 18:27:11', 'Windows', TRUE, 18, 18, 18, 18, 18, 58);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16558, 'ARM', 183, 799, 2015, '2021-02-24 07:44:56', 'Linux', TRUE, 19, 19, 19, 19, 19, 59);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24127, NULL, NULL, 0, 2015, '2020-11-19 22:46:08', 'Mac', TRUE, 20, 20, 20, 20, 20, 60);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10816, NULL, 119, NULL, 2015, '2020-02-28 13:56:56', 'Windows', TRUE, 21, 21, 21, 21, 21, 61);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26579, 'Intel', 174, NULL, 2012, '2020-11-30 11:26:07', 'Windows', TRUE, 22, 22, 22, 22, 22, 62);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31331, 'Intel', 131, NULL, 2014, '2021-12-09 07:50:33', 'Linux', TRUE, 23, 23, 23, 23, 23, 63);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10732, 'Intel', 163, 703, 2015, '2021-05-28 21:30:59', 'Linux', TRUE, 24, 24, 24, 24, 24, 64);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15929, 'Intel', NULL, 1761, 2012, '2018-08-31 17:44:37', 'Windows', TRUE, 25, 25, 25, 25, 25, 65);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26559, NULL, NULL, 272, 2015, '2019-04-03 17:47:07', 'Windows', TRUE, 26, 26, 26, 26, 26, 66);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26929, 'Intel', NULL, 0, 2013, '2018-09-26 00:38:54', 'Linux', TRUE, 27, 27, 27, 27, 27, 67);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14191, 'ARM', 237, NULL, 2012, '2021-11-22 04:27:21', 'Windows', TRUE, 28, 28, 28, 28, 28, 68);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24612, 'Intel', 62, NULL, 2013, '2020-01-07 15:48:49', 'Windows', TRUE, 29, 29, 29, 29, 29, 69);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19931, 'ARM', NULL, 0, 2015, '2020-07-01 13:49:14', 'Mac', TRUE, 30, 30, 30, 30, 30, 70);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29135, NULL, 186, NULL, 2016, '2021-07-20 02:14:44', 'Linux', TRUE, 31, 31, 31, 31, 31, 71);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12437, NULL, NULL, 467, 2015, '2018-11-17 07:32:22', 'Mac', TRUE, 32, 32, 32, 32, 32, 72);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18664, 'Intel', 142, 1215, 2012, '2020-02-10 22:00:04', 'Mac', TRUE, 33, 33, 33, 33, 33, 73);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24646, 'AMD', NULL, 1817, 2012, '2019-10-04 19:15:50', 'Windows', TRUE, 34, 34, 34, 34, 34, 74);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14889, NULL, NULL, 1491, 2012, '2021-07-17 19:25:42', 'Mac', TRUE, 35, 35, 35, 35, 35, 75);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11907, NULL, NULL, 0, 2013, '2020-11-03 17:46:25', 'Mac', TRUE, 36, 36, 36, 36, 36, 76);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29566, NULL, 38, 393, 2015, '2019-11-29 16:09:51', 'Mac', TRUE, 37, 37, 37, 37, 37, 77);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29420, 'ARM', NULL, 439, 2013, '2018-12-12 16:21:43', 'Linux', TRUE, 38, 38, 38, 38, 38, 78);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20613, NULL, 193, NULL, 2015, '2018-05-29 06:10:15', 'Windows', TRUE, 39, 39, 39, 39, 39, 79);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26030, NULL, 248, NULL, 2013, '2021-01-30 17:38:13', 'Windows', TRUE, 40, 40, 40, 40, 40, 80);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21052, 'ARM', NULL, 0, 2015, '2019-10-13 08:05:53', 'Linux', TRUE, 41, 41, 41, 41, 41, 1);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34865, 'Intel', NULL, 1793, 2014, '2018-06-13 15:40:32', 'Mac', TRUE, 42, 42, 42, 42, 42, 2);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27454, NULL, 24, 517, 2013, '2018-05-25 21:15:24', 'Mac', TRUE, 43, 43, 43, 43, 43, 3);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16331, 'ARM', NULL, 1112, 2013, '2020-08-26 04:09:59', 'Windows', TRUE, 44, 44, 44, 44, 44, 4);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32292, 'ARM', 108, 1440, 2012, '2021-01-19 21:35:01', 'Mac', TRUE, 45, 45, 45, 45, 45, 5);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29748, 'AMD', NULL, 256, 2013, '2021-02-12 07:58:52', 'Windows', TRUE, 46, 46, 46, 46, 46, 6);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14793, NULL, 126, 1627, 2013, '2021-06-11 07:15:22', 'Mac', TRUE, 47, 47, 47, 47, 47, 7);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37826, NULL, NULL, 0, 2012, '2021-04-20 08:08:09', 'Windows', TRUE, 48, 48, 48, 48, 48, 8);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19223, NULL, 162, 1211, 2015, '2018-05-07 19:18:24', 'Mac', TRUE, 49, 49, 49, 49, 49, 9);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18438, NULL, 84, 294, 2012, '2018-01-21 18:35:57', 'Linux', TRUE, 50, 50, 50, 50, 50, 10);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30342, 'AMD', NULL, 336, 2013, '2021-03-01 04:45:18', 'Linux', TRUE, 51, 51, 51, 51, 51, 11);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23846, 'AMD', NULL, 1786, 2014, '2019-07-08 08:58:10', 'Windows', TRUE, 52, 52, 52, 52, 52, 12);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18706, NULL, 196, NULL, 2015, '2020-09-15 15:58:49', 'Windows', TRUE, 53, 53, 53, 53, 53, 13);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25549, NULL, 104, NULL, 2013, '2021-03-29 03:06:40', 'Linux', TRUE, 54, 54, 54, 54, 54, 14);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26988, 'AMD', NULL, 679, 2012, '2019-12-31 08:37:06', 'Linux', TRUE, 55, 55, 55, 55, 55, 15);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33763, 'Intel', NULL, 0, 2012, '2018-08-23 07:35:28', 'Windows', TRUE, 56, 56, 56, 56, 56, 16);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35782, NULL, NULL, 1585, 2015, '2021-08-21 01:52:42', 'Windows', TRUE, 57, 57, 57, 57, 57, 17);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18062, NULL, 241, 171, 2014, '2021-12-10 00:20:52', 'Windows', TRUE, 58, 58, 58, 58, 58, 18);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11670, NULL, NULL, 0, 2013, '2019-08-24 00:56:51', 'Mac', TRUE, 59, 59, 59, 59, 59, 19);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23935, 'ARM', NULL, 0, 2016, '2018-01-16 13:09:40', 'Windows', TRUE, 60, 60, 60, 60, 60, 20);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20041, NULL, NULL, 287, 2013, '2020-06-17 06:40:08', 'Mac', TRUE, 61, 61, 61, 61, 61, 21);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37981, NULL, 59, NULL, 2015, '2021-09-30 20:51:48', 'Mac', TRUE, 62, 62, 62, 62, 62, 22);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32673, 'Intel', 156, 281, 2015, '2020-07-29 07:51:38', 'Windows', TRUE, 63, 63, 63, 63, 63, 23);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11602, 'ARM', 136, NULL, 2012, '2020-08-26 14:28:29', 'Mac', TRUE, 64, 64, 64, 64, 64, 24);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19379, 'Intel', 92, 483, 2013, '2021-11-11 19:45:20', 'Windows', TRUE, 65, 65, 65, 65, 65, 25);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30480, 'ARM', NULL, 1589, 2012, '2018-01-10 23:41:51', 'Mac', TRUE, 66, 66, 66, 66, 66, 26);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11372, NULL, 255, NULL, 2012, '2018-05-28 03:40:13', 'Windows', TRUE, 67, 67, 67, 67, 67, 27);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15797, NULL, NULL, 0, 2012, '2019-01-08 23:22:37', 'Mac', TRUE, 68, 68, 68, 68, 68, 28);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31687, NULL, 102, 585, 2012, '2021-11-14 01:50:32', 'Linux', TRUE, 69, 69, 69, 69, 69, 29);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14903, 'AMD', NULL, 1075, 2015, '2021-08-22 13:29:05', 'Windows', TRUE, 70, 70, 70, 70, 70, 30);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10093, 'AMD', NULL, 0, 2014, '2019-06-05 23:27:40', 'Linux', TRUE, 1, 1, 1, 1, 1, 31);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24507, 'ARM', NULL, 0, 2011, '2021-09-03 03:53:08', 'Linux', TRUE, 2, 2, 2, 2, 2, 32);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36747, NULL, NULL, 0, 2015, '2018-09-12 15:41:06', 'Windows', TRUE, 3, 3, 3, 3, 3, 33);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38506, 'Intel', NULL, 1106, 2015, '2018-04-28 13:57:53', 'Mac', TRUE, 4, 4, 4, 4, 4, 34);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22973, 'Intel', NULL, 364, 2015, '2021-08-03 10:41:47', 'Mac', TRUE, 5, 5, 5, 5, 5, 35);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18550, NULL, NULL, 0, 2015, '2020-03-11 17:09:26', 'Windows', TRUE, 6, 6, 6, 6, 6, 36);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16523, 'ARM', NULL, 1343, 2013, '2018-05-21 04:49:29', 'Mac', TRUE, 7, 7, 7, 7, 7, 37);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38609, 'ARM', NULL, 0, 2015, '2021-04-04 13:00:06', 'Windows', TRUE, 8, 8, 8, 8, 8, 38);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38393, 'AMD', 145, 1098, 2016, '2020-08-16 16:05:43', 'Linux', TRUE, 9, 9, 9, 9, 9, 39);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11655, NULL, 94, NULL, 2016, '2018-09-28 01:01:30', 'Linux', TRUE, 10, 10, 10, 10, 10, 40);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38066, NULL, 111, NULL, 2012, '2021-07-13 02:50:55', 'Mac', TRUE, 11, 11, 11, 11, 11, 41);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26688, 'AMD', 79, NULL, 2013, '2020-03-09 20:15:45', 'Linux', TRUE, 12, 12, 12, 12, 12, 42);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35715, NULL, NULL, 0, 2016, '2021-09-23 19:37:34', 'Mac', TRUE, 13, 13, 13, 13, 13, 43);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25794, 'ARM', 183, NULL, 2014, '2018-08-18 12:59:43', 'Mac', TRUE, 14, 14, 14, 14, 14, 44);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18557, NULL, 228, NULL, 2013, '2019-06-05 19:41:59', 'Mac', TRUE, 15, 15, 15, 15, 15, 45);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28430, NULL, NULL, 0, 2014, '2021-09-13 02:39:48', 'Windows', TRUE, 16, 16, 16, 16, 16, 46);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19571, 'AMD', 193, NULL, 2013, '2018-02-08 20:21:45', 'Linux', TRUE, 17, 17, 17, 17, 17, 47);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23289, 'ARM', 78, 211, 2014, '2018-05-13 19:59:16', 'Windows', TRUE, 18, 18, 18, 18, 18, 48);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37621, NULL, 221, NULL, 2014, '2020-04-17 19:57:44', 'Windows', TRUE, 19, 19, 19, 19, 19, 49);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16363, NULL, NULL, 1674, 2016, '2020-04-11 03:11:07', 'Linux', TRUE, 20, 20, 20, 20, 20, 50);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10321, NULL, 236, NULL, 2016, '2021-04-25 19:24:23', 'Linux', TRUE, 21, 21, 21, 21, 21, 51);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33687, NULL, 205, NULL, 2014, '2018-07-26 16:33:43', 'Linux', TRUE, 22, 22, 22, 22, 22, 52);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26785, NULL, NULL, 995, 2015, '2021-09-09 08:59:31', 'Mac', TRUE, 23, 23, 23, 23, 23, 53);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10602, NULL, NULL, 0, 2015, '2018-03-08 15:11:22', 'Mac', TRUE, 24, 24, 24, 24, 24, 54);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17675, 'ARM', 252, NULL, 2015, '2018-07-03 10:18:35', 'Windows', TRUE, 25, 25, 25, 25, 25, 55);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26999, 'ARM', 132, 1451, 2015, '2021-07-03 02:34:58', 'Linux', TRUE, 26, 26, 26, 26, 26, 56);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15485, NULL, NULL, 0, 2015, '2019-07-19 15:38:29', 'Linux', TRUE, 27, 27, 27, 27, 27, 57);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38511, NULL, NULL, 0, 2013, '2019-09-22 15:15:13', 'Windows', TRUE, 28, 28, 28, 28, 28, 58);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21610, 'AMD', NULL, 0, 2013, '2018-01-23 04:04:19', 'Mac', TRUE, 29, 29, 29, 29, 29, 59);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12473, NULL, NULL, 0, 2015, '2021-04-07 12:31:23', 'Linux', TRUE, 30, 30, 30, 30, 30, 60);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13532, 'AMD', NULL, 1195, 2016, '2018-12-17 04:51:29', 'Windows', TRUE, 31, 31, 31, 31, 31, 61);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14775, 'AMD', 120, 1625, 2012, '2020-08-02 12:17:07', 'Mac', TRUE, 32, 32, 32, 32, 32, 62);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31102, NULL, NULL, 0, 2012, '2019-02-08 22:50:26', 'Windows', TRUE, 33, 33, 33, 33, 33, 63);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38193, NULL, NULL, 0, 2016, '2019-09-18 22:39:42', 'Linux', TRUE, 34, 34, 34, 34, 34, 64);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34900, NULL, 225, 1502, 2012, '2021-09-01 09:27:56', 'Windows', TRUE, 35, 35, 35, 35, 35, 65);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32820, 'ARM', NULL, 0, 2016, '2021-01-03 13:15:46', 'Mac', TRUE, 36, 36, 36, 36, 36, 66);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23133, 'Intel', 120, 1284, 2016, '2020-09-05 14:13:54', 'Windows', TRUE, 37, 37, 37, 37, 37, 67);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14792, 'ARM', NULL, 0, 2013, '2018-05-23 01:44:37', 'Windows', TRUE, 38, 38, 38, 38, 38, 68);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13624, NULL, NULL, 209, 2013, '2018-01-22 06:25:20', 'Mac', TRUE, 39, 39, 39, 39, 39, 69);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15527, 'ARM', 147, 1315, 2016, '2018-10-24 01:01:54', 'Windows', TRUE, 40, 40, 40, 40, 40, 70);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16155, 'ARM', 120, NULL, 2012, '2020-02-20 05:10:44', 'Mac', TRUE, 41, 41, 41, 41, 41, 71);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25079, NULL, 32, 1893, 2016, '2018-02-16 16:04:32', 'Mac', TRUE, 42, 42, 42, 42, 42, 72);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27741, 'AMD', NULL, 836, 2015, '2019-05-14 10:49:54', 'Mac', TRUE, 43, 43, 43, 43, 43, 73);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27317, NULL, NULL, 930, 2013, '2020-06-12 06:20:11', 'Mac', TRUE, 44, 44, 44, 44, 44, 74);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30767, 'Intel', NULL, 0, 2015, '2019-12-05 21:13:46', 'Linux', TRUE, 45, 45, 45, 45, 45, 75);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23320, NULL, NULL, 1565, 2016, '2019-12-10 09:14:46', 'Windows', TRUE, 46, 46, 46, 46, 46, 76);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11141, 'ARM', 57, NULL, 2015, '2021-11-26 02:57:53', 'Windows', TRUE, 47, 47, 47, 47, 47, 77);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20767, NULL, NULL, 0, 2012, '2019-02-01 23:38:38', 'Windows', TRUE, 48, 48, 48, 48, 48, 78);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18277, 'AMD', 90, 346, 2013, '2020-12-11 20:33:53', 'Linux', TRUE, 49, 49, 49, 49, 49, 79);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17047, NULL, 148, NULL, 2011, '2018-10-08 13:08:10', 'Windows', TRUE, 50, 50, 50, 50, 50, 80);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19740, 'ARM', NULL, 0, 2015, '2018-06-06 11:37:51', 'Linux', TRUE, 51, 51, 51, 51, 51, 1);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37189, NULL, 136, NULL, 2014, '2018-11-08 00:33:51', 'Linux', TRUE, 52, 52, 52, 52, 52, 2);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32964, 'ARM', NULL, 0, 2013, '2018-12-26 10:05:30', 'Mac', TRUE, 53, 53, 53, 53, 53, 3);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11439, 'ARM', NULL, 1770, 2015, '2021-03-05 20:39:24', 'Windows', TRUE, 54, 54, 54, 54, 54, 4);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25673, 'AMD', 29, NULL, 2012, '2018-11-24 09:06:06', 'Windows', TRUE, 55, 55, 55, 55, 55, 5);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11188, NULL, 59, 1862, 2015, '2021-06-27 06:16:04', 'Windows', TRUE, 56, 56, 56, 56, 56, 6);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14280, 'Intel', 200, NULL, 2016, '2018-02-27 23:49:12', 'Windows', TRUE, 57, 57, 57, 57, 57, 7);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11325, NULL, 26, 1699, 2013, '2020-01-27 04:08:45', 'Linux', TRUE, 58, 58, 58, 58, 58, 8);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27800, 'AMD', NULL, 0, 2014, '2019-10-23 09:02:01', 'Linux', TRUE, 59, 59, 59, 59, 59, 9);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36380, 'AMD', 124, 1670, 2012, '2018-10-29 15:12:38', 'Mac', TRUE, 60, 60, 60, 60, 60, 10);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25134, 'Intel', 169, 1941, 2013, '2020-04-20 17:40:51', 'Mac', TRUE, 61, 61, 61, 61, 61, 11);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23030, NULL, 201, 1393, 2015, '2021-11-16 19:26:17', 'Linux', TRUE, 62, 62, 62, 62, 62, 12);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35155, NULL, NULL, 0, 2013, '2020-09-02 11:21:36', 'Linux', TRUE, 63, 63, 63, 63, 63, 13);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18329, 'Intel', NULL, 0, 2014, '2019-11-22 03:54:21', 'Windows', TRUE, 64, 64, 64, 64, 64, 14);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25585, 'ARM', 146, 1381, 2013, '2020-01-26 05:41:00', 'Windows', TRUE, 65, 65, 65, 65, 65, 15);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32781, 'AMD', 61, 568, 2014, '2018-01-26 00:37:43', 'Windows', TRUE, 66, 66, 66, 66, 66, 16);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38476, 'AMD', 62, 1629, 2012, '2019-09-24 00:43:53', 'Windows', TRUE, 67, 67, 67, 67, 67, 17);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24016, 'AMD', 186, 403, 2014, '2021-08-28 03:42:39', 'Mac', TRUE, 68, 68, 68, 68, 68, 18);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24611, 'AMD', NULL, 1915, 2012, '2018-09-27 08:58:27', 'Mac', TRUE, 69, 69, 69, 69, 69, 19);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37208, NULL, NULL, 0, 2013, '2020-05-10 08:52:35', 'Windows', TRUE, 70, 70, 70, 70, 70, 20);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32529, NULL, NULL, 1198, 2014, '2020-04-24 15:41:11', 'Windows', TRUE, 1, 1, 1, 1, 1, 21);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20078, 'AMD', 38, NULL, 2012, '2018-04-20 18:27:35', 'Windows', TRUE, 2, 2, 2, 2, 2, 22);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11660, NULL, NULL, 0, 2012, '2021-04-24 11:00:02', 'Windows', TRUE, 3, 3, 3, 3, 3, 23);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34094, 'AMD', NULL, 1340, 2014, '2019-01-26 19:22:19', 'Windows', TRUE, 4, 4, 4, 4, 4, 24);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36640, NULL, 126, 301, 2014, '2018-04-24 02:34:02', 'Linux', TRUE, 5, 5, 5, 5, 5, 25);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16604, 'ARM', 220, 1551, 2015, '2018-12-29 23:17:17', 'Windows', TRUE, 6, 6, 6, 6, 6, 26);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34963, 'AMD', 239, 1937, 2015, '2018-04-06 22:00:51', 'Linux', TRUE, 7, 7, 7, 7, 7, 27);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37387, NULL, 162, 2029, 2016, '2019-02-02 23:13:43', 'Mac', TRUE, 8, 8, 8, 8, 8, 28);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37663, 'ARM', NULL, 0, 2014, '2019-12-15 21:50:07', 'Mac', TRUE, 9, 9, 9, 9, 9, 29);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18955, NULL, 147, 1755, 2014, '2021-09-29 07:43:12', 'Windows', TRUE, 10, 10, 10, 10, 10, 30);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30155, NULL, 106, 1983, 2016, '2021-01-01 05:46:11', 'Windows', TRUE, 11, 11, 11, 11, 11, 31);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15584, NULL, NULL, 1889, 2014, '2019-09-03 15:39:52', 'Mac', TRUE, 12, 12, 12, 12, 12, 32);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23359, NULL, 243, NULL, 2016, '2019-11-12 23:14:22', 'Linux', TRUE, 13, 13, 13, 13, 13, 33);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16147, NULL, 50, NULL, 2016, '2020-12-17 17:55:37', 'Mac', TRUE, 14, 14, 14, 14, 14, 34);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36818, 'ARM', 212, NULL, 2013, '2019-02-03 11:58:56', 'Mac', TRUE, 15, 15, 15, 15, 15, 35);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30288, 'ARM', 256, 1805, 2014, '2020-07-19 11:12:48', 'Windows', TRUE, 16, 16, 16, 16, 16, 36);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21576, 'Intel', NULL, 513, 2015, '2019-05-16 10:39:17', 'Mac', TRUE, 17, 17, 17, 17, 17, 37);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31007, 'Intel', 100, 1799, 2016, '2020-12-18 19:06:01', 'Linux', TRUE, 18, 18, 18, 18, 18, 38);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29374, 'ARM', NULL, 0, 2013, '2021-09-29 10:52:10', 'Linux', TRUE, 19, 19, 19, 19, 19, 39);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13782, NULL, 144, 457, 2014, '2021-07-24 12:37:24', 'Windows', TRUE, 20, 20, 20, 20, 20, 40);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14559, 'AMD', NULL, 0, 2012, '2020-05-12 13:08:34', 'Windows', TRUE, 21, 21, 21, 21, 21, 41);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10983, 'AMD', NULL, 1124, 2012, '2019-12-29 02:32:35', 'Linux', TRUE, 22, 22, 22, 22, 22, 42);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16229, 'AMD', 61, 1174, 2013, '2018-02-17 07:57:51', 'Linux', TRUE, 23, 23, 23, 23, 23, 43);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27045, 'Intel', 76, NULL, 2014, '2018-05-15 06:46:25', 'Linux', TRUE, 24, 24, 24, 24, 24, 44);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22704, 'ARM', NULL, 0, 2012, '2019-01-24 22:25:31', 'Mac', TRUE, 25, 25, 25, 25, 25, 45);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21760, 'ARM', NULL, 0, 2015, '2019-11-03 17:16:36', 'Mac', TRUE, 26, 26, 26, 26, 26, 46);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18172, 'ARM', NULL, 0, 2016, '2019-06-14 17:00:46', 'Mac', TRUE, 27, 27, 27, 27, 27, 47);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12326, NULL, 198, NULL, 2012, '2021-03-28 05:31:43', 'Windows', TRUE, 28, 28, 28, 28, 28, 48);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11381, 'AMD', NULL, 0, 2014, '2019-04-01 13:26:55', 'Mac', TRUE, 29, 29, 29, 29, 29, 49);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29007, NULL, 55, 1728, 2016, '2021-05-01 14:23:24', 'Linux', TRUE, 30, 30, 30, 30, 30, 50);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29736, 'Intel', NULL, 0, 2015, '2018-10-08 13:32:58', 'Mac', TRUE, 31, 31, 31, 31, 31, 51);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26087, 'AMD', NULL, 1796, 2016, '2021-06-04 05:27:00', 'Windows', TRUE, 32, 32, 32, 32, 32, 52);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22806, NULL, 14, NULL, 2012, '2019-04-12 11:21:10', 'Windows', TRUE, 33, 33, 33, 33, 33, 53);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23637, 'ARM', NULL, 0, 2016, '2018-10-22 06:04:52', 'Mac', TRUE, 34, 34, 34, 34, 34, 54);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29300, NULL, NULL, 1067, 2012, '2020-05-21 12:00:15', 'Mac', TRUE, 35, 35, 35, 35, 35, 55);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29702, NULL, 34, NULL, 2013, '2018-10-17 08:31:32', 'Windows', TRUE, 36, 36, 36, 36, 36, 56);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25297, NULL, 35, NULL, 2015, '2020-01-09 02:21:52', 'Linux', TRUE, 37, 37, 37, 37, 37, 57);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32732, 'AMD', NULL, 0, 2013, '2021-04-24 15:52:28', 'Windows', TRUE, 38, 38, 38, 38, 38, 58);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13878, NULL, NULL, 1080, 2013, '2018-08-03 10:12:07', 'Linux', TRUE, 39, 39, 39, 39, 39, 59);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27222, NULL, NULL, 754, 2013, '2018-11-25 09:31:58', 'Linux', TRUE, 40, 40, 40, 40, 40, 60);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27347, 'Intel', NULL, 0, 2014, '2019-01-24 22:01:34', 'Linux', TRUE, 41, 41, 41, 41, 41, 61);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15604, NULL, NULL, 0, 2013, '2021-02-02 00:49:57', 'Mac', TRUE, 42, 42, 42, 42, 42, 62);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29426, NULL, 242, 667, 2015, '2019-01-03 12:24:00', 'Linux', TRUE, 43, 43, 43, 43, 43, 63);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11819, 'ARM', 74, NULL, 2014, '2021-10-16 05:01:54', 'Linux', TRUE, 44, 44, 44, 44, 44, 64);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13877, 'ARM', NULL, 0, 2016, '2021-11-28 08:28:44', 'Linux', TRUE, 45, 45, 45, 45, 45, 65);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15845, 'AMD', 95, NULL, 2016, '2021-01-29 23:31:07', 'Linux', TRUE, 46, 46, 46, 46, 46, 66);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32556, 'ARM', 5, 1626, 2014, '2019-09-19 20:02:41', 'Linux', TRUE, 47, 47, 47, 47, 47, 67);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13340, NULL, NULL, 0, 2015, '2020-01-05 12:19:51', 'Windows', TRUE, 48, 48, 48, 48, 48, 68);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11741, NULL, 16, 1189, 2014, '2021-11-02 14:01:38', 'Linux', TRUE, 49, 49, 49, 49, 49, 69);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16627, NULL, NULL, 945, 2015, '2019-11-05 13:05:24', 'Mac', TRUE, 50, 50, 50, 50, 50, 70);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28120, 'AMD', 171, 1700, 2015, '2021-03-19 02:11:05', 'Mac', TRUE, 51, 51, 51, 51, 51, 71);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15162, 'AMD', 194, 1937, 2016, '2018-09-28 01:52:36', 'Linux', TRUE, 52, 52, 52, 52, 52, 72);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12941, NULL, NULL, 0, 2012, '2019-03-01 05:27:51', 'Windows', TRUE, 53, 53, 53, 53, 53, 73);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26380, 'ARM', 46, 435, 2012, '2021-10-03 18:12:00', 'Linux', TRUE, 54, 54, 54, 54, 54, 74);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13404, NULL, NULL, 331, 2015, '2018-10-03 03:20:53', 'Windows', TRUE, 55, 55, 55, 55, 55, 75);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16441, NULL, NULL, 1210, 2013, '2021-10-08 09:42:28', 'Windows', TRUE, 56, 56, 56, 56, 56, 76);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15546, NULL, 19, 1591, 2014, '2018-01-07 00:32:37', 'Linux', TRUE, 57, 57, 57, 57, 57, 77);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20460, 'Intel', 210, 1275, 2013, '2020-03-06 19:39:22', 'Mac', TRUE, 58, 58, 58, 58, 58, 78);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30080, NULL, NULL, 0, 2014, '2019-03-21 11:18:43', 'Windows', TRUE, 59, 59, 59, 59, 59, 79);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32136, NULL, 178, 689, 2014, '2020-11-04 10:48:34', 'Linux', TRUE, 60, 60, 60, 60, 60, 80);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34733, NULL, NULL, 0, 2012, '2020-07-13 07:22:18', 'Windows', TRUE, 61, 61, 61, 61, 61, 1);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13566, NULL, NULL, 309, 2016, '2020-08-28 06:47:37', 'Windows', TRUE, 62, 62, 62, 62, 62, 2);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10749, NULL, 202, 1243, 2014, '2019-07-05 01:49:01', 'Linux', TRUE, 63, 63, 63, 63, 63, 3);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19507, 'ARM', NULL, 0, 2012, '2018-07-27 18:21:20', 'Linux', TRUE, 64, 64, 64, 64, 64, 4);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10951, 'ARM', NULL, 1025, 2016, '2019-05-05 22:55:39', 'Windows', TRUE, 65, 65, 65, 65, 65, 5);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33215, NULL, NULL, 0, 2013, '2019-08-10 06:07:10', 'Windows', TRUE, 66, 66, 66, 66, 66, 6);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14610, NULL, NULL, 0, 2015, '2020-01-18 20:28:12', 'Linux', TRUE, 67, 67, 67, 67, 67, 7);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27608, 'Intel', NULL, 1929, 2015, '2018-12-15 08:27:36', 'Windows', TRUE, 68, 68, 68, 68, 68, 8);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27763, 'AMD', 224, 1790, 2012, '2018-02-09 13:49:50', 'Linux', TRUE, 69, 69, 69, 69, 69, 9);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38757, NULL, NULL, 959, 2016, '2018-06-18 18:30:00', 'Linux', TRUE, 70, 70, 70, 70, 70, 10);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29258, 'AMD', NULL, 0, 2014, '2020-02-03 06:49:58', 'Windows', TRUE, 1, 1, 1, 1, 1, 11);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21611, 'Intel', 34, 633, 2012, '2021-03-12 23:58:37', 'Mac', TRUE, 2, 2, 2, 2, 2, 12);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12958, 'ARM', 52, 1012, 2015, '2020-01-19 21:43:54', 'Linux', TRUE, 3, 3, 3, 3, 3, 13);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21595, 'ARM', 221, 81, 2015, '2018-07-23 08:46:41', 'Mac', TRUE, 4, 4, 4, 4, 4, 14);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17985, NULL, NULL, 0, 2015, '2020-05-29 12:22:43', 'Mac', TRUE, 5, 5, 5, 5, 5, 15);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11663, NULL, NULL, 1951, 2014, '2020-08-12 14:38:52', 'Windows', TRUE, 6, 6, 6, 6, 6, 16);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38436, 'Intel', NULL, 177, 2014, '2020-07-14 12:04:24', 'Linux', TRUE, 7, 7, 7, 7, 7, 17);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16345, NULL, 168, 527, 2016, '2021-10-22 18:45:26', 'Linux', TRUE, 8, 8, 8, 8, 8, 18);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11097, NULL, 112, NULL, 2015, '2019-12-26 13:05:42', 'Mac', TRUE, 9, 9, 9, 9, 9, 19);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15278, 'AMD', NULL, 1782, 2013, '2020-10-14 06:18:01', 'Mac', TRUE, 10, 10, 10, 10, 10, 20);

INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (1, 'Crist-Kohler', 'http://ohara.biz/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (2, 'Waters, Blick and Barrows', 'http://graham.com/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (3, 'Waelchi-Schoen', 'http://oberbrunner.org/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (4, 'Hilll Ltd', 'http://www.ruecker.com/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (5, 'Keeling, Keeling and Wolff', 'http://flatleyromaguera.com/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (6, 'Goodwin-Fritsch', 'http://www.funkgrady.com/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (7, 'Lemke-Dooley', 'http://www.bergstrom.com/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (8, 'Yost PLC', 'http://marks.com/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (9, 'Christiansen, Reynolds and Macejkovic', 'http://www.kihn.com/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (10, 'Murphy, Goyette and Conroy', 'http://okuneva.info/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (11, 'Gislason-Bernier', 'http://www.kuvalisaltenwerth.com/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (12, 'Shields-Gaylord', 'http://mclaughlinbruen.biz/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (13, 'Franecki-Walsh', 'http://welch.org/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (14, 'Dicki-Treutel', 'http://satterfield.biz/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (15, 'Kihn-Weber', 'http://www.christiansen.com/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (16, 'Marks LLC', 'http://www.bechtelargislason.com/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (17, 'Welch LLC', 'http://www.blickondricka.com/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (18, 'Quitzon-Steuber', 'http://heidenreichhowe.biz/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (19, 'Mraz-Okuneva', 'http://kihn.info/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (20, 'Stamm-Mohr', 'http://fahey.com/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (21, 'Stroman-Schuster', 'http://www.pouros.com/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (22, 'Satterfield and Sons', 'http://www.huels.info/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (23, 'Robel-Auer', 'http://www.aufderhar.com/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (24, 'Waelchi and Sons', 'http://kilbackwehner.org/', TRUE);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (25, 'Lowe, Hayes and Runte', 'http://wiegand.com/', TRUE);

INSERT INTO member_of (person_id, dept_id, is_active) VALUES (1, 1, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (2, 2, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (3, 3, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (4, 4, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (5, 5, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (6, 6, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (7, 7, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (8, 8, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (9, 9, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (10, 10, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (11, 11, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (12, 12, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (13, 13, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (14, 14, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (15, 15, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (16, 1, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (17, 2, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (18, 3, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (19, 4, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (20, 5, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (21, 6, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (22, 7, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (23, 8, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (24, 9, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (25, 10, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (26, 11, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (27, 12, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (28, 13, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (29, 14, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (30, 15, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (31, 1, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (32, 2, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (33, 3, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (34, 4, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (35, 5, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (36, 6, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (37, 7, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (38, 8, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (39, 9, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (40, 10, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (41, 11, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (42, 12, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (43, 13, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (44, 14, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (45, 15, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (46, 1, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (47, 2, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (48, 3, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (49, 4, TRUE);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (50, 5, TRUE);

INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (1, 1, 'IPS', 1211, 28, 246, TRUE, 13);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (2, 1, 'VA', 3119, 38, 268, TRUE, 12);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (3, 3, 'VA', 2405, 32, 153, TRUE, 14);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (4, 0, 'TN', 3244, 20, 357, TRUE, 9);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (5, 3, 'Retina', 1158, 40, 71, TRUE, 6);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (6, 3, 'IPS', 1167, 42, 138, TRUE, 3);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (7, 0, 'IPS', 3169, 35, 303, TRUE, 24);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (8, 4, 'IPS', 1143, 32, 110, TRUE, 25);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (9, 0, 'IPS', 3601, 33, 343, TRUE, 17);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (10, 0, 'TN', 3336, 39, 322, TRUE, 21);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (11, 4, 'Retina', 1275, 25, 86, TRUE, 7);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (12, 2, 'IPS', 2279, 37, 210, TRUE, 5);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (13, 2, 'Retina', 2552, 18, 119, TRUE, 16);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (14, 2, 'TN', 1354, 42, 262, TRUE, 23);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (15, 0, 'Retina', 1353, 34, 336, TRUE, 8);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (16, 0, 'Retina', 1167, 40, 92, TRUE, 1);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (17, 4, 'IPS', 2556, 36, 275, TRUE, 9);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (18, 1, 'IPS', 4138, 41, 160, TRUE, 18);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (19, 4, 'Retina', 3482, 41, 162, TRUE, 16);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (20, 0, 'TN', 1886, 38, 126, TRUE, 20);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (21, 1, 'TN', 1898, 22, 142, TRUE, 11);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (22, 1, 'Retina', 3046, 24, 137, TRUE, 25);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (23, 2, 'Retina', 4132, 43, 321, TRUE, 8);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (24, 2, 'VA', 1236, 35, 250, TRUE, 7);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (25, 0, 'VA', 1585, 29, 317, TRUE, 8);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (26, 2, 'IPS', 2400, 37, 77, TRUE, 2);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (27, 3, 'VA', 2470, 37, 134, TRUE, 15);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (28, 3, 'Retina', 2683, 35, 216, TRUE, 22);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (29, 1, 'VA', 2482, 30, 102, TRUE, 20);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (30, 4, 'TN', 1408, 33, 175, TRUE, 10);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (31, 1, 'TN', 1981, 37, 178, TRUE, 11);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (32, 3, 'VA', 3162, 40, 95, TRUE, 6);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (33, 4, 'IPS', 3075, 21, 74, TRUE, 5);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (34, 4, 'TN', 1295, 34, 79, TRUE, 1);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (35, 2, 'TN', 1813, 23, 288, TRUE, 24);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (36, 4, 'Retina', 1589, 42, 161, TRUE, 15);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (37, 1, 'Retina', 2232, 24, 284, TRUE, 15);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (38, 3, 'TN', 1910, 21, 154, TRUE, 6);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (39, 0, 'IPS', 1389, 32, 334, TRUE, 9);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (40, 1, 'VA', 2956, 28, 81, TRUE, 12);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (41, 2, 'VA', 1091, 23, 287, TRUE, 18);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (42, 1, 'TN', 4183, 34, 306, TRUE, 18);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (43, 4, 'Retina', 3952, 43, 333, TRUE, 19);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (44, 4, 'Retina', 4055, 20, 274, TRUE, 5);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (45, 2, 'VA', 3048, 19, 331, TRUE, 4);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (46, 1, 'VA', 4208, 30, 133, TRUE, 2);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (47, 0, 'TN', 2584, 29, 78, TRUE, 22);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (48, 1, 'Retina', 3617, 30, 310, TRUE, 17);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (49, 0, 'TN', 3016, 19, 146, TRUE, 4);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (50, 4, 'Retina', 2414, 38, 245, TRUE, 17);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (51, 0, 'VA', 2082, 25, 313, TRUE, 11);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (52, 3, 'Retina', 1201, 34, 256, TRUE, 19);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (53, 0, 'VA', 3064, 35, 218, TRUE, 1);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (54, 0, 'IPS', 1827, 40, 84, TRUE, 10);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (55, 0, 'VA', 2784, 32, 184, TRUE, 14);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (56, 1, 'TN', 3160, 23, 309, TRUE, 10);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (57, 2, 'Retina', 1763, 38, 152, TRUE, 19);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (58, 2, 'VA', 2409, 37, 264, TRUE, 14);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (59, 1, 'VA', 4050, 18, 123, TRUE, 3);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (60, 3, 'Retina', 2614, 29, 344, TRUE, 23);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (61, 1, 'VA', 2338, 21, 316, TRUE, 13);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (62, 2, 'TN', 1320, 31, 227, TRUE, 13);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (63, 2, 'Retina', 3660, 38, 111, TRUE, 7);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (64, 2, 'Retina', 1255, 26, 190, TRUE, 4);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (65, 1, 'TN', 1376, 33, 231, TRUE, 2);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (66, 0, 'Retina', 3047, 28, 331, TRUE, 16);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (67, 0, 'VA', 1573, 34, 60, TRUE, 20);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (68, 0, 'IPS', 1343, 40, 141, TRUE, 12);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (69, 0, 'TN', 1821, 25, 100, TRUE, 21);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (70, 4, 'IPS', 1451, 18, 357, TRUE, 3);

INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (1, '76y40ip25zq9', 'LWAPP', 'Router', 1612, 41, 125, 536, TRUE, 17);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (2, '57l98is85sz3', 'RGIP', 'Router', 6070, 79, 41, 863, TRUE, 13);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (3, '25n58op46gj1', 'RGIP', 'Switch', 2450, 79, 102, 1019, TRUE, 10);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (4, '15c99xp36tx3', 'RIP', 'Switch', 8544, 29, 124, 241, TRUE, 2);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (5, '11u31lq61gd1', 'RGIP', 'Switch', 8719, 74, 111, 457, TRUE, 8);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (6, '41m79cf74rz9', 'IEEE 802', 'Gateway', 8073, 74, 191, 866, TRUE, 16);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (7, '13d14fm71wg2', 'RIP', 'Gateway', 717, 39, 147, 1150, TRUE, 12);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (8, '13z26uv12vk6', 'LWAPP', 'Switch', 4047, 64, 167, 1167, TRUE, 6);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (9, '41x73zn75of3', 'RGIP', 'Gateway', 7382, 55, 171, 397, TRUE, 18);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (10, '93b46lm84zp9', 'RGIP', 'Switch', 149, 49, 117, 281, TRUE, 12);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (11, '84j58ff10kp5', 'IEEE 802', 'Switch', 699, 65, 34, 369, TRUE, 15);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (12, '90z29oc29us8', 'IEEE 802', 'Gateway', 4255, 31, 30, 1022, TRUE, 9);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (13, '96w00qg67wb0', 'RIP', 'Modem', 4140, 34, 52, 283, TRUE, 7);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (14, '12y55xt84rg0', 'RGIP', 'Switch', 8021, 27, 188, 1139, TRUE, 16);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (15, '54n47er50do3', 'LWAPP', 'Switch', 2583, 79, 171, 261, TRUE, 15);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (16, '37q68hq36pk5', 'LWAPP', 'Router', 7962, 65, 110, 123, TRUE, 22);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (17, '61y79jg56hq9', 'LWAPP', 'Switch', 4228, 14, 177, 969, TRUE, 17);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (18, '41m65ls64eg2', 'LWAPP', 'Gateway', 4829, 83, 127, 920, TRUE, 3);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (19, '39y27dg64mw9', 'RGIP', 'Modem', 7919, 13, 86, 298, TRUE, 24);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (20, '40z17pw34zh8', 'IEEE 802', 'Modem', 8822, 61, 113, 854, TRUE, 18);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (21, '00u74ma51jk3', 'RIP', 'Router', 1813, 47, 17, 621, TRUE, 1);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (22, '82i36un85dj5', 'IEEE 802', 'Gateway', 5230, 58, 141, 825, TRUE, 14);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (23, '17c60ej56gd0', 'LWAPP', 'Router', 4632, 83, 150, 513, TRUE, 11);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (24, '38d97sa81ly5', 'RIP', 'Switch', 2332, 57, 190, 140, TRUE, 17);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (25, '29b79yl22sj7', 'IEEE 802', 'Gateway', 9899, 50, 119, 44, TRUE, 9);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (26, '13b05mz75is8', 'IEEE 802', 'Switch', 482, 98, 128, 504, TRUE, 13);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (27, '73r78wg61sz9', 'IEEE 802', 'Gateway', 2761, 31, 43, 752, TRUE, 19);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (28, '77c71ry02ow5', 'RIP', 'Router', 288, 65, 59, 974, TRUE, 18);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (29, '01x31xg36ej3', 'LWAPP', 'Router', 2196, 26, 11, 818, TRUE, 6);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (30, '93i67dt21ey1', 'RGIP', 'Modem', 6824, 89, 18, 1067, TRUE, 11);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (31, '86r72pc93us0', 'RIP', 'Gateway', 3782, 96, 64, 211, TRUE, 14);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (32, '42b00tg24yh1', 'IEEE 802', 'Switch', 6723, 5, 162, 331, TRUE, 20);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (33, '88r27lk88kq1', 'RGIP', 'Gateway', 6207, 82, 121, 877, TRUE, 19);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (34, '37d40me40po7', 'RIP', 'Router', 6949, 7, 23, 812, TRUE, 12);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (35, '80w78lf90en1', 'RIP', 'Modem', 5509, 15, 62, 1119, TRUE, 8);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (36, '75b31fd85zv9', 'RGIP', 'Gateway', 1607, 90, 133, 221, TRUE, 7);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (37, '62r76ek20yk5', 'RGIP', 'Switch', 5809, 10, 56, 395, TRUE, 23);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (38, '67v23qm79ka9', 'RIP', 'Switch', 2195, 37, 110, 1051, TRUE, 9);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (39, '51u71wl13gj9', 'RIP', 'Router', 7138, 22, 55, 1124, TRUE, 11);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (40, '06d43fs66ce1', 'LWAPP', 'Gateway', 2241, 48, 41, 697, TRUE, 4);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (41, '87z60nq34wk4', 'RIP', 'Modem', 8623, 13, 34, 778, TRUE, 3);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (42, '62t89em96tk8', 'RIP', 'Router', 1075, 56, 197, 735, TRUE, 5);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (43, '73m50sk42pk4', 'IEEE 802', 'Switch', 3843, 6, 118, 40, TRUE, 1);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (44, '50w71ww52tn1', 'RIP', 'Router', 6011, 73, 139, 742, TRUE, 14);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (45, '76o27ce53mz9', 'RGIP', 'Switch', 1804, 29, 118, 470, TRUE, 21);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (46, '47v94zl09xk7', 'RGIP', 'Modem', 2519, 3, 111, 495, TRUE, 10);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (47, '11u95rn18bu8', 'IEEE 802', 'Gateway', 4441, 92, 101, 278, TRUE, 23);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (48, '96r24fv64wa7', 'IEEE 802', 'Router', 3918, 49, 173, 841, TRUE, 2);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (49, '96s83me38cd6', 'RGIP', 'Router', 8504, 79, 42, 516, TRUE, 2);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (50, '97f74lh35an5', 'RGIP', 'Modem', 2565, 67, 166, 808, TRUE, 25);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (51, '98l81fj01al9', 'LWAPP', 'Switch', 7696, 63, 100, 763, TRUE, 8);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (52, '26a08yc97nu8', 'LWAPP', 'Router', 9441, 54, 104, 185, TRUE, 4);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (53, '77f59ba40zd9', 'LWAPP', 'Router', 236, 69, 10, 941, TRUE, 20);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (54, '93y06ys95dj3', 'IEEE 802', 'Switch', 9137, 4, 129, 656, TRUE, 16);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (55, '03k83ik35cy6', 'RGIP', 'Router', 6747, 53, 169, 312, TRUE, 5);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (56, '06x91oj37jj5', 'LWAPP', 'Modem', 3443, 59, 123, 473, TRUE, 3);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (57, '64d27bh03tv3', 'RGIP', 'Switch', 6073, 97, 73, 999, TRUE, 5);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (58, '56w36pt02mj6', 'LWAPP', 'Modem', 8960, 98, 111, 109, TRUE, 24);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (59, '28o25ga53ph3', 'IEEE 802', 'Modem', 7456, 85, 83, 650, TRUE, 21);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (60, '23r07xg89qb1', 'RGIP', 'Switch', 3891, 56, 95, 185, TRUE, 7);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (61, '10i45wb33st6', 'RGIP', 'Modem', 5262, 28, 45, 125, TRUE, 1);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (62, '72c67vp69wv5', 'LWAPP', 'Gateway', 8552, 94, 67, 373, TRUE, 25);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (63, '40n34gm24rs4', 'IEEE 802', 'Gateway', 9490, 63, 136, 574, TRUE, 15);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (64, '07n99uc29pr1', 'RGIP', 'Switch', 5837, 38, 59, 601, TRUE, 22);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (65, '38u39tm35so3', 'RGIP', 'Router', 1276, 99, 186, 577, TRUE, 19);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (66, '00u74xd86tz1', 'RGIP', 'Router', 4573, 20, 67, 747, TRUE, 6);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (67, '67n41yf35fi7', 'RIP', 'Router', 5473, 71, 30, 113, TRUE, 4);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (68, '59g80ry59xj8', 'RGIP', 'Modem', 1422, 89, 36, 960, TRUE, 20);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (69, '04f97qo73mt7', 'RGIP', 'Modem', 6883, 93, 37, 499, TRUE, 10);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (70, '94a47xn08ju8', 'RIP', 'Gateway', 7091, 2, 163, 66, TRUE, 13);

INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10045, TRUE, 1, 1, 1, 1);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10093, TRUE, 2, 2, 2, 2);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10133, TRUE, 3, 3, 3, 3);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10149, TRUE, 4, 4, 4, 4);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10167, TRUE, 5, 5, 5, 5);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10194, TRUE, 6, 6, 6, 6);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10264, TRUE, 7, 7, 7, 7);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10296, TRUE, 8, 8, 8, 8);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10321, TRUE, 9, 9, 9, 9);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10449, TRUE, 10, 10, 10, 10);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10569, TRUE, 11, 11, 11, 11);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10602, TRUE, 12, 12, 12, 12);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10732, TRUE, 13, 13, 13, 13);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10741, TRUE, 14, 14, 14, 14);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10749, TRUE, 15, 15, 15, 15);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10816, TRUE, 16, 1, 16, 16);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10951, TRUE, 17, 2, 17, 17);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10983, TRUE, 18, 3, 18, 18);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11097, TRUE, 19, 4, 19, 19);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11113, TRUE, 20, 5, 20, 20);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11141, TRUE, 21, 6, 21, 21);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11188, TRUE, 22, 7, 22, 22);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11190, TRUE, 23, 8, 23, 23);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11317, TRUE, 24, 9, 24, 24);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11325, TRUE, 25, 10, 25, 25);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11372, TRUE, 26, 11, 26, 26);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11381, TRUE, 27, 12, 27, 27);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11439, TRUE, 28, 13, 28, 28);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11473, TRUE, 29, 14, 29, 29);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11494, TRUE, 30, 15, 30, 30);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11561, TRUE, 31, 1, 31, 1);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11570, TRUE, 32, 2, 32, 2);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11602, TRUE, 33, 3, 33, 3);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11614, TRUE, 34, 4, 34, 4);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11655, TRUE, 35, 5, 35, 5);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11660, TRUE, 36, 6, 1, 6);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11663, TRUE, 37, 7, 2, 7);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11670, TRUE, 38, 8, 3, 8);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11702, TRUE, 39, 9, 4, 9);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11721, TRUE, 40, 10, 5, 10);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11741, TRUE, 41, 11, 6, 11);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11747, TRUE, 42, 12, 7, 12);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11819, TRUE, 43, 13, 8, 13);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11907, TRUE, 44, 14, 9, 14);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11987, TRUE, 45, 15, 10, 15);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12035, TRUE, 46, 1, 11, 16);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12114, TRUE, 47, 2, 12, 17);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12225, TRUE, 48, 3, 13, 18);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12265, TRUE, 49, 4, 14, 19);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12312, TRUE, 50, 5, 15, 20);

INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (1, 'Pedro', 'Smitham', '759', 'bsmitham@example.com', 'professor', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (2, 'Walker', 'Kuhic', '703600', 'eunice29@example.net', 'admin', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (3, 'Marjory', 'Kiehn', '335799', 'xschumm@example.org', 'admin', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (4, 'Gay', 'Lakin', '0', 'ljaskolski@example.org', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (5, 'Kenneth', 'Cummerata', '932', 'kyra.feeney@example.com', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (6, 'Cathy', 'OHara', '433414', 'shalvorson@example.net', 'professor', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (7, 'Kacie', 'Ernser', '198', 'harber.eladio@example.com', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (8, 'Velva', 'Ryan', '5', 'kraig22@example.net', 'professor', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (9, 'Sierra', 'Cronin', '78', 'elda.brakus@example.com', 'professor', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (10, 'Justus', 'Larson', '1', 'tyrique.hilpert@example.net', 'professor', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (11, 'Tobin', 'Monahan', '283537', 'raphaelle.denesik@example.net', 'TA', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (12, 'Jewel', 'Stanton', '0', 'pearlie84@example.net', 'professor', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (13, 'Grayson', 'Kassulke', '2', 'stehr.adonis@example.net', 'admin', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (14, 'Christop', 'Gutkowski', '0', 'jwintheiser@example.net', 'TA', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (15, 'Horacio', 'Kuvalis', '2100597433', 'lehner.korbin@example.net', 'admin', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (16, 'Jesse', 'Moore', '694', 'yreynolds@example.net', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (17, 'Brionna', 'Koss', '46', 'christine60@example.org', 'professor', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (18, 'Winifred', 'Leuschke', '246', 'declan.dooley@example.org', 'admin', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (19, 'Boyd', 'Cummerata', '1', 'areilly@example.org', 'professor', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (20, 'Lexi', 'Wunsch', '909787', 'bergstrom.euna@example.org', 'TA', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (21, 'Lonny', 'Hintz', '769656', 'lenore53@example.com', 'professor', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (22, 'Matilde', 'Abbott', '9564768870', 'chase48@example.com', 'admin', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (23, 'Mayra', 'Labadie', '81', 'fahey.ephraim@example.org', 'TA', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (24, 'Bernardo', 'Walsh', '1', 'mayert.baron@example.com', 'TA', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (25, 'Trever', 'Gottlieb', '490', 'madelyn88@example.net', 'TA', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (26, 'Lorine', 'Christiansen', '1', 'zboncak.vita@example.com', 'professor', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (27, 'Judy', 'Balistreri', '27637', 'jo77@example.net', 'professor', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (28, 'Braeden', 'Padberg', '0', 'brogahn@example.net', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (29, 'Minerva', 'Daugherty', '1', 'bruen.etha@example.org', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (30, 'Judy', 'Bayer', '0', 'hahn.aniyah@example.com', 'admin', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (31, 'Albertha', 'Hane', '468', 'slowe@example.net', 'professor', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (32, 'Hosea', 'Yundt', '0', 'dare.billie@example.org', 'TA', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (33, 'Corene', 'Brown', '28', 'ankunding.dwight@example.net', 'admin', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (34, 'Magnus', 'Price', '721', 'howell.zena@example.net', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (35, 'Augusta', 'Wintheiser', '784', 'mitchell.unique@example.net', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (36, 'Jewell', 'Streich', '175154', 'linda.breitenberg@example.net', 'admin', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (37, 'Celestino', 'Lindgren', '283645', 'paucek.beulah@example.net', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (38, 'Pink', 'Raynor', '0', 'tkihn@example.net', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (39, 'Mariane', 'Bailey', '5937453702', 'ankunding.leola@example.org', 'admin', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (40, 'Jada', 'Welch', '235', 'kathryne21@example.net', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (41, 'Lura', 'Swaniawski', '442', 'bettye.breitenberg@example.com', 'professor', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (42, 'Brendan', 'Kub', '293894', 'sbreitenberg@example.org', 'admin', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (43, 'Bart', 'Mitchell', '302', 'stroman.kay@example.net', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (44, 'Trevor', 'Dooley', '437', 'rogelio39@example.org', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (45, 'Sonny', 'Lockman', '347', 'esmeralda33@example.com', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (46, 'Yoshiko', 'Bogan', '892821', 'roel49@example.com', 'professor', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (47, 'Sarah', 'Grant', '1', 'ncrist@example.com', 'admin', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (48, 'Phoebe', 'Hudson', '74080', 'cruickshank.warren@example.org', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (49, 'Hayden', 'Morar', '212', 'brando94@example.com', 'student', TRUE);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (50, 'Esta', 'Collins', '0', 'ykihn@example.net', 'student' TRUE);

INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (1, TRUE, 'A6', 7, 23, TRUE, 'bw', TRUE, 18);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (2, FALSE, 'A6', 7, 6, TRUE, 'bw', TRUE, 13);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (3, FALSE, 'A4', 5, 22, FALSE, 'color', TRUE, 4);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (4, FALSE, 'A6', 7, 9, FALSE, 'bw', TRUE, 10);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (5, TRUE, 'A4', 2, 21, TRUE, 'bw', TRUE, 8);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (6, TRUE, 'A2', 2, 22, FALSE, 'bw', TRUE, 21);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (7, FALSE, 'A4', 5, 12, FALSE, 'bw', TRUE, 7);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (8, FALSE, 'A6', 8, 25, TRUE, 'bw', TRUE, 16);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (9, FALSE, 'A4', 3, 22, TRUE, 'color', TRUE, 12);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (10, TRUE, 'A5', 4, 15, TRUE, 'color', TRUE, 19);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (11, TRUE, 'A1', 2, 25, TRUE, 'color', TRUE, 23);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (12, FALSE, 'A5', 7, 22, TRUE, 'color', TRUE, 11);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (13, FALSE, 'A3', 3, 13, TRUE, 'bw', TRUE, 6);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (14, TRUE, 'A1', 3, 12, TRUE, 'color', TRUE, 2);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (15, FALSE, 'A4', 5, 16, TRUE, 'color', TRUE, 1);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (16, FALSE, 'A6', 6, 12, TRUE, 'bw', TRUE, 13);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (17, FALSE, 'A5', 4, 21, TRUE, 'color', TRUE, 5);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (18, FALSE, 'A4', 4, 15, FALSE, 'color', TRUE, 1);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (19, TRUE, 'A4', 8, 10, FALSE, 'bw', TRUE, 17);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (20, TRUE, 'A3', 6, 11, TRUE, 'bw', TRUE, 10);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (21, FALSE, 'A5', 6, 11, FALSE, 'color', TRUE, 24);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (22, FALSE, 'A6', 8, 25, TRUE, 'bw', TRUE, 14);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (23, TRUE, 'A3', 8, 4, FALSE, 'color', TRUE, 6);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (24, TRUE, 'A1', 6, 12, TRUE, 'bw', TRUE, 25);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (25, TRUE, 'A1', 6, 19, TRUE, 'bw', TRUE, 11);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (26, FALSE, 'A4', 2, 20, FALSE, 'color', TRUE, 4);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (27, FALSE, 'A4', 8, 14, FALSE, 'bw', TRUE, 14);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (28, TRUE, 'A1', 2, 5, FALSE, 'color', TRUE, 7);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (29, TRUE, 'A1', 6, 16, FALSE, 'bw', TRUE, 10);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (30, FALSE, 'A3', 6, 18, FALSE, 'bw', TRUE, 15);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (31, TRUE, 'A4', 4, 8, TRUE, 'bw', TRUE, 25);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (32, TRUE, 'A6', 3, 16, TRUE, 'color', TRUE, 6);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (33, FALSE, 'A6', 7, 14, TRUE, 'color', TRUE, 12);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (34, FALSE, 'A1', 7, 14, TRUE, 'bw', TRUE, 1);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (35, TRUE, 'A5', 4, 4, FALSE, 'color', TRUE, 2);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (36, TRUE, 'A3', 8, 8, FALSE, 'bw', TRUE, 3);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (37, TRUE, 'A4', 7, 24, TRUE, 'bw', TRUE, 20);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (38, TRUE, 'A1', 6, 9, FALSE, 'color', TRUE, 15);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (39, TRUE, 'A5', 8, 18, FALSE, 'bw', TRUE, 18);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (40, FALSE, 'A3', 2, 24, FALSE, 'color', TRUE, 19);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (41, FALSE, 'A4', 3, 17, TRUE, 'color', TRUE, 13);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (42, FALSE, 'A1', 8, 15, FALSE, 'color', TRUE, 18);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (43, FALSE, 'A5', 8, 16, TRUE, 'color', TRUE, 23);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (44, FALSE, 'A5', 2, 24, TRUE, 'color', TRUE, 19);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (45, FALSE, 'A1', 7, 17, TRUE, 'color', TRUE, 11);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (46, TRUE, 'A4', 2, 16, FALSE, 'color', TRUE, 2);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (47, TRUE, 'A4', 5, 12, TRUE, 'bw', TRUE, 17);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (48, TRUE, 'A4', 6, 14, FALSE, 'bw', TRUE, 3);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (49, TRUE, 'A3', 5, 18, TRUE, 'bw', TRUE, 16);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (50, FALSE, 'A4', 3, 24, FALSE, 'color', TRUE, 20);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (51, FALSE, 'A3', 6, 8, FALSE, 'bw', TRUE, 9);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (52, TRUE, 'A6', 4, 15, FALSE, 'bw', TRUE, 8);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (53, FALSE, 'A5', 2, 13, FALSE, 'color', TRUE, 21);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (54, TRUE, 'A5', 6, 17, FALSE, 'bw', TRUE, 22);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (55, FALSE, 'A6', 2, 24, TRUE, 'bw', TRUE, 22);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (56, TRUE, 'A1', 8, 24, TRUE, 'bw', TRUE, 5);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (57, TRUE, 'A2', 6, 20, FALSE, 'color', TRUE, 20);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (58, TRUE, 'A6', 8, 6, FALSE, 'color', TRUE, 17);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (59, FALSE, 'A3', 3, 13, TRUE, 'color', TRUE, 16);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (60, FALSE, 'A6', 3, 18, FALSE, 'bw', TRUE, 8);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (61, FALSE, 'A5', 6, 15, TRUE, 'bw', TRUE, 24);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (62, TRUE, 'A1', 7, 24, TRUE, 'color', TRUE, 5);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (63, FALSE, 'A4', 5, 6, TRUE, 'bw', TRUE, 4);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (64, TRUE, 'A5', 6, 23, TRUE, 'bw', TRUE, 9);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (65, TRUE, 'A1', 4, 11, TRUE, 'color', TRUE, 14);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (66, TRUE, 'A5', 8, 13, FALSE, 'color', TRUE, 9);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (67, FALSE, 'A4', 2, 19, TRUE, 'color', TRUE, 3);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (68, FALSE, 'A3', 4, 17, TRUE, 'color', TRUE, 7);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (69, TRUE, 'A5', 8, 12, FALSE, 'color', TRUE, 15);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (70, TRUE, 'A6', 4, 12, FALSE, 'color', TRUE, 12);

INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (1, 0, TRUE, 3118, FALSE, 1476, 14, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (2, 0, TRUE, 2541, FALSE, 1863, 18, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (3, 3, FALSE, 2277, TRUE, 1519, 5, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (4, 4, TRUE, 2321, FALSE, 1872, 4, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (5, 1, FALSE, 1272, FALSE, 1620, 15, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (6, 4, FALSE, 3534, FALSE, 2125, 3, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (7, 2, FALSE, 3420, FALSE, 2055, 19, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (8, 2, TRUE, 1431, FALSE, 1779, 17, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (9, 4, FALSE, 2284, TRUE, 1649, 1, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (10, 4, FALSE, 771, TRUE, 1244, 10, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (11, 0, TRUE, 2518, TRUE, 1542, 19, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (12, 1, TRUE, 2236, FALSE, 2116, 8, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (13, 2, FALSE, 2090, FALSE, 1862, 20, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (14, 1, FALSE, 1652, FALSE, 1175, 11, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (15, 1, TRUE, 2867, FALSE, 2090, 4, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (16, 0, FALSE, 2619, FALSE, 1603, 23, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (17, 1, FALSE, 1451, TRUE, 1363, 11, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (18, 2, FALSE, 656, FALSE, 1268, 21, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (19, 1, FALSE, 3369, FALSE, 1980, 9, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (20, 4, FALSE, 2640, TRUE, 1892, 25, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (21, 1, TRUE, 3304, FALSE, 1943, 13, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (22, 1, FALSE, 2008, FALSE, 1227, 7, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (23, 3, TRUE, 2978, TRUE, 1916, 14, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (24, 2, FALSE, 2443, FALSE, 1228, 17, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (25, 1, FALSE, 3436, FALSE, 1967, 12, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (26, 1, FALSE, 685, FALSE, 1693, 24, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (27, 0, FALSE, 2272, FALSE, 1117, 1, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (28, 1, FALSE, 2379, TRUE, 1080, 16, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (29, 0, TRUE, 1854, TRUE, 1529, 20, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (30, 2, FALSE, 1005, FALSE, 1416, 20, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (31, 1, TRUE, 1032, TRUE, 1083, 7, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (32, 2, TRUE, 1453, TRUE, 2133, 6, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (33, 3, TRUE, 1183, TRUE, 1135, 13, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (34, 3, FALSE, 799, TRUE, 1370, 17, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (35, 4, FALSE, 637, TRUE, 1412, 8, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (36, 2, FALSE, 3596, FALSE, 1489, 16, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (37, 1, FALSE, 1184, FALSE, 2008, 15, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (38, 0, FALSE, 1259, FALSE, 1597, 3, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (39, 3, TRUE, 3000, TRUE, 2131, 18, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (40, 1, TRUE, 3334, TRUE, 1385, 5, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (41, 0, TRUE, 1234, FALSE, 1848, 6, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (42, 3, FALSE, 2164, TRUE, 1747, 1, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (43, 2, FALSE, 2183, TRUE, 1138, 2, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (44, 3, TRUE, 2828, TRUE, 1668, 2, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (45, 4, FALSE, 2546, FALSE, 1918, 15, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (46, 0, FALSE, 2773, TRUE, 1977, 24, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (47, 4, TRUE, 827, FALSE, 1254, 23, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (48, 3, FALSE, 1697, TRUE, 1281, 18, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (49, 1, FALSE, 681, FALSE, 1371, 2, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (50, 3, FALSE, 3512, TRUE, 1651, 21, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (51, 1, FALSE, 849, TRUE, 1987, 13, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (52, 3, FALSE, 3164, FALSE, 1761, 10, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (53, 4, FALSE, 2483, FALSE, 1491, 19, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (54, 4, TRUE, 1446, TRUE, 1812, 11, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (55, 0, FALSE, 1896, FALSE, 2131, 9, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (56, 2, FALSE, 815, TRUE, 1655, 22, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (57, 4, FALSE, 1356, TRUE, 1469, 16, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (58, 1, FALSE, 3128, FALSE, 1571, 8, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (59, 3, FALSE, 1050, TRUE, 1489, 7, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (60, 3, TRUE, 1391, FALSE, 1502, 9, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (61, 0, TRUE, 3380, FALSE, 1193, 22, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (62, 4, TRUE, 1247, FALSE, 1480, 10, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (63, 3, FALSE, 2780, FALSE, 1133, 12, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (64, 4, FALSE, 1244, TRUE, 1972, 12, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (65, 4, FALSE, 1475, FALSE, 2040, 3, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (66, 2, FALSE, 2402, TRUE, 1447, 14, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (67, 1, FALSE, 1974, TRUE, 1117, 5, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (68, 1, FALSE, 1659, TRUE, 1632, 25, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (69, 3, TRUE, 763, TRUE, 1361, 6, TRUE);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (70, 0, TRUE, 2574, FALSE, 2112, 4, TRUE);

INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (21, 1, TRUE, 12);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (7, 2, TRUE, 28);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (34, 3, TRUE, 16);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (6, 4, TRUE, 2);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (30, 5, TRUE, 9);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (27, 6, TRUE, 8);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (25, 7, TRUE, 18);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (3, 8, TRUE, 6);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (9, 9, TRUE, 20);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (1, 10, TRUE, 10);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (30, 11, TRUE, 29);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (7, 12, TRUE, 21);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (14, 13, TRUE, 25);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (13, 14, TRUE, 13);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (14, 15, TRUE, 14);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (13, 16, TRUE, 7);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (11, 17, TRUE, 3);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (37, 18, TRUE, 4);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (34, 19, TRUE, 15);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (27, 20, TRUE, 5);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (22, 21, TRUE, 30);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (3, 22, TRUE, 11);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (1, 23, TRUE, 24);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (27, 24, TRUE, 17);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (16, 25, TRUE, 1);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (37, 26, TRUE, 22);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (20, 27, TRUE, 27);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (23, 28, TRUE, 26);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (33, 29, TRUE, 23);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (0, 30, TRUE, 19);

INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (1, 'Exercitationem accusantium voluptates quidem beata', TRUE, 4);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (2, 'Et iste expedita ad est tempora.', TRUE, 14);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (3, 'Cupiditate ut error temporibus voluptatem sint per', TRUE, 16);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (4, 'Quia voluptates ut sed quo.', TRUE, 20);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (5, 'Harum in rerum totam repudiandae.', TRUE, 8);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (6, 'Omnis incidunt numquam ut qui at occaecati in.', TRUE, 24);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (7, 'Quam quasi nostrum molestiae ea.', TRUE, 11);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (8, 'Dignissimos expedita nobis molestiae voluptates do', TRUE, 2);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (9, 'Et fugiat facilis voluptas provident suscipit.', TRUE, 8);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (10, 'Et quod qui quia non nulla unde.', TRUE, 1);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (11, 'Rem ipsum et veritatis ipsam.', TRUE, 9);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (12, 'Enim iste quos quaerat quia.', TRUE, 6);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (13, 'Nobis aut minus minima architecto qui.', TRUE, 21);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (14, 'Expedita molestias aut aut quis.', TRUE, 10);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (15, 'Asperiores quia rem sit veritatis laborum aspernat', TRUE, 25);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (16, 'Voluptate velit autem molestiae quis repudiandae e', TRUE, 15);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (17, 'Ut earum quia qui reprehenderit.', TRUE, 23);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (18, 'Et tempora quis qui rerum.', TRUE, 2);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (19, 'Quos omnis quia inventore.', TRUE, 5);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (20, 'Natus pariatur nisi voluptate fugiat nihil porro i', TRUE, 17);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (21, 'Consequuntur nostrum natus dolorem ab doloribus co', TRUE, 10);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (22, 'Est corporis rerum sed.', TRUE, 4);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (23, 'Animi temporibus tempore velit.', TRUE, 22);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (24, 'Dolores qui beatae saepe rem.', TRUE, 1);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (25, 'Rerum dolores ipsum quia aliquam.', TRUE, 13);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (26, 'Sapiente iste blanditiis optio adipisci suscipit.', TRUE, 19);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (27, 'Reiciendis itaque sed eius ut dolor alias.', TRUE, 12);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (28, 'Praesentium libero et sunt qui molestias.', TRUE, 6);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (29, 'Eos beatae ducimus aliquam totam at dignissimos qu', TRUE, 3);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (30, 'At officia saepe tenetur voluptatem eius dolorum v', TRUE, 5);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (31, 'Tenetur a qui magnam sit in et dolor.', TRUE, 3);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (32, 'Ut vero quo voluptatem aliquid voluptatum aut sunt', TRUE, 7);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (33, 'Sunt voluptatibus voluptatem incidunt id ad volupt', TRUE, 7);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (34, 'Amet rerum eos libero animi voluptatum consectetur', TRUE, 9);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (35, 'Aliquam officiis cupiditate impedit numquam aut od', TRUE, 18);

INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (1, 210, 'Mac', 1547, TRUE, 2, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (2, 120, 'Windows', 486, FALSE, 10, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (3, 191, 'Windows', 1778, FALSE, 11, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (4, 42, 'Windows', 1117, FALSE, 21, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (5, 216, 'Mac', 1677, FALSE, 14, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (6, 66, 'Windows', 1295, TRUE, 12, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (7, 33, 'Windows', 1746, FALSE, 20, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (8, 173, 'Linux', 1086, TRUE, 19, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (9, 101, 'Windows', 892, TRUE, 19, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (10, 192, 'Windows', 1906, FALSE, 13, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (11, 33, 'Mac', 2015, FALSE, 23, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (12, 49, 'Mac', 1537, FALSE, 9, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (13, 172, 'Mac', 1514, TRUE, 1, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (14, 68, 'Windows', 775, TRUE, 10, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (15, 180, 'Windows', 474, FALSE, 10, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (16, 179, 'Windows', 543, FALSE, 6, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (17, 164, 'Linux', 762, FALSE, 23, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (18, 91, 'Mac', 284, TRUE, 2, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (19, 130, 'Linux', 2003, TRUE, 21, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (20, 127, 'Mac', 1320, TRUE, 11, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (21, 18, 'Mac', 156, TRUE, 1, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (22, 255, 'Linux', 1355, FALSE, 4, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (23, 166, 'Windows', 1378, FALSE, 7, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (24, 255, 'Windows', 784, FALSE, 12, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (25, 231, 'Windows', 722, TRUE, 3, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (26, 70, 'Windows', 1009, FALSE, 17, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (27, 111, 'Linux', 431, TRUE, 3, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (28, 26, 'Mac', 1267, TRUE, 5, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (29, 157, 'Linux', 1341, TRUE, 6, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (30, 49, 'Windows', 1060, TRUE, 7, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (31, 87, 'Linux', 708, TRUE, 15, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (32, 47, 'Linux', 2013, FALSE, 20, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (33, 197, 'Mac', 129, FALSE, 8, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (34, 236, 'Linux', 1174, FALSE, 24, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (35, 130, 'Windows', 709, TRUE, 12, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (36, 235, 'Linux', 976, TRUE, 4, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (37, 125, 'Mac', 1976, TRUE, 18, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (38, 103, 'Windows', 1395, FALSE, 17, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (39, 218, 'Windows', 1433, TRUE, 15, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (40, 121, 'Windows', 750, FALSE, 13, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (41, 190, 'Mac', 1251, TRUE, 16, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (42, 21, 'Linux', 1778, FALSE, 4, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (43, 197, 'Linux', 207, TRUE, 7, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (44, 200, 'Windows', 1274, TRUE, 25, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (45, 156, 'Mac', 1483, FALSE, 6, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (46, 180, 'Linux', 122, TRUE, 15, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (47, 249, 'Windows', 1581, FALSE, 8, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (48, 236, 'Windows', 1308, FALSE, 13, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (49, 160, 'Windows', 1242, TRUE, 5, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (50, 146, 'Mac', 1149, TRUE, 18, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (51, 229, 'Windows', 2048, FALSE, 17, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (52, 182, 'Windows', 1572, FALSE, 24, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (53, 79, 'Windows', 1041, FALSE, 2, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (54, 226, 'Linux', 1092, FALSE, 18, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (55, 207, 'Mac', 457, FALSE, 25, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (56, 195, 'Windows', 1678, TRUE, 14, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (57, 207, 'Mac', 1138, FALSE, 11, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (58, 79, 'Linux', 177, FALSE, 19, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (59, 201, 'Linux', 1028, TRUE, 8, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (60, 184, 'Linux', 1251, TRUE, 16, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (61, 30, 'Windows', 1223, TRUE, 9, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (62, 109, 'Windows', 1920, FALSE, 22, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (63, 76, 'Mac', 1494, FALSE, 20, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (64, 212, 'Mac', 1115, TRUE, 1, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (65, 73, 'Mac', 1731, TRUE, 22, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (66, 162, 'Windows', 1461, FALSE, 16, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (67, 91, 'Mac', 670, FALSE, 5, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (68, 245, 'Windows', 510, TRUE, 14, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (69, 178, 'Mac', 1769, FALSE, 3, TRUE);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (70, 49, 'Windows', 548, TRUE, 9, TRUE);

INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (1, 1, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (2, 2, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (3, 3, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (4, 4, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (5, 5, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (6, 6, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (7, 7, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (8, 8, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (9, 9, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (10, 10, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (11, 11, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (12, 12, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (13, 13, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (14, 14, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (15, 15, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (16, 16, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (17, 17, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (18, 18, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (19, 19, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (20, 20, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (21, 21, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (22, 22, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (23, 23, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (24, 24, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (25, 25, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (26, 1, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (27, 2, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (28, 3, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (29, 4, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (30, 5, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (31, 6, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (32, 7, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (33, 8, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (34, 9, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (35, 10, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (36, 11, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (37, 12, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (38, 13, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (39, 14, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (40, 15, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (41, 16, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (42, 17, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (43, 18, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (44, 19, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (45, 20, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (46, 21, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (47, 22, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (48, 23, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (49, 24, TRUE);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (50, 25, TRUE);