# This is the Project Bidgood Phase 5 repository.

## CS-125 Database Design

### Project members:

- Ethan Bañez
- Sam Mokracek
- Adeline Ndacyayisenga
- Noah Stevens

### Sources used

- Django Documentation
- Postgres Documentation

### Tools used

- filldb.info
- sqlines studio
- django-boostrap
- django-fonts

### Repostitory URL:

https://bitbucket.org/andacyayisenga/quad1/src/master/
