from django.conf.urls import include, url
from django.db import router
from django.urls import path
from rest_framework import routers

from . import views


app_name = "public"

# router = routers.DefaultRouter()
# router.register(r'manufacturers$', views.ManufViewSet, 'manufacturers')

urlpatterns = [
    # path("", views.manufacturer, name='manufacturers'),
    # path("create/", views.createManuf, name='create_man'),
    # path("update/<int:manuf_id>", views.updateManuf),
    # path("delete/<int:manuf_id>", views.deleteManuf),
    # path("manufacturers/", include(router.urls)),
    url(
        r"^manufacturers/$",
        views.ManufViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    url(r"^manufacturers/all$", views.ManufViewSet.as_view({"get": "list",})),
    url(
        r"^buildings$",
        views.BuildingViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    url(r"^buildings/all$", views.BuildingViewSet.as_view({"get": "list",})),
    url(
        r"^persons$",
        views.PersonViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    url(r"^persons/all$", views.PersonViewSet.as_view({"get": "list",})),
    url(
        r"^departments$",
        views.DepartmentViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    url(r"^departments/all$", views.DepartmentViewSet.as_view({"get": "list",})),
    url(
        r"^projectors$",
        views.ProjectorViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    url(r"^projeectors/all$", views.ProjectorViewSet.as_view({"get": "list",})),
    url(
        r"^printers$",
        views.PrinterViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    url(r"^printers/all$", views.PrinterViewSet.as_view({"get": "list",})),
    url(
        r"^computers$",
        views.ComputerViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    url(r"^computers/all$", views.ComputerViewSet.as_view({"get": "list",})),
    url(
        r"^monitors$",
        views.MonitorViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    url(r"^monitors/all$", views.MonitorViewSet.as_view({"get": "list",})),
    url(
        r"^nwdevices$",
        views.NWDeviceViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    url(r"^nwdevices/all$", views.NWDeviceViewSet.as_view({"get": "list",})),
    url(
        r"^rooms$",
        views.RoomViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    url(r"^rooms/all$", views.RoomViewSet.as_view({"get": "list",})),
    url(
        r"^racks$",
        views.RackViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    url(r"^racks/all$", views.RackViewSet.as_view({"get": "list",})),
    url(
        r"^owners$",
        views.OwnerViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    url(r"^owners/all$", views.OwnerViewSet.as_view({"get": "list",})),
    url(
        r"^servers$",
        views.ServerViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    url(r"^servers/all$", views.ServerViewSet.as_view({"get": "list",})),
    url(
        r"^cameras$",
        views.CameraViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    url(r"^cameras/all$", views.CameraViewSet.as_view({"get": "list",})),
    url(
        r"^devices$",
        views.DeviceViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
    ),
    url(r"^devices/all$", views.DeviceViewSet.as_view({"get": "list",})),
    url(r"^racks/<id:rack_id>", views.RackViewSet.as_view({"get": "retrieve",})),
]
