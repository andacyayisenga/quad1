from django.shortcuts import get_object_or_404, redirect, render
from django.http import HttpResponse

from .forms import ManufacturerCreate
from .models import (
    Manufacturer,
    Building,
    Camera_Model,
    Computer_Model,
    Department,
    Device,
    Member_of,
    Monitor_Model,
    Nwdevice_Model,
    Printer_Model,
    Projector_Model,
    Owner,
    Server_Model,
    Room,
    Person,
    Works_in,
    Rack,
)
from rest_framework import serializers
from rest_framework.viewsets import ModelViewSet

# Create your views here.

# implement a serializer??

# make a view set for each app?

# # def man(request) -> HttpResponse:
# #     assert isinstance(request, HttpRequest)
# #     QuerySet = Manufacturer.objects.filter(is_active=True).order_by('manuf_id')
# #     serializer_class = ManufSerializer(QuerySet, many=True)
# #     return render(request, 'models/manufacturer.html', {'data': serializer_class.data})

# def manufacturer(request) -> HttpResponse:
#     directory = Manufacturer.objects.all()
#     return render(request, 'manufacturers.html', {'manufacturer': directory})

# def createManuf(request) -> HttpResponse:
#     new_manufacturers = ManufacturerCreate()
#     if request.method == 'POST':
#         new_manufacturers = ManufacturerCreate(request.POST, request.FILES)
#         if new_manufacturers.is_valid():
#             new_manufacturers.save()
#             return redirect('manufacturers')
#         else :
#             return HttpResponse("Your form is wrong, reload.")

#     else :
#         return render(request, 'manufacturers_create.html', {'manufacturer': new_manufacturers})


# def updateManuf(request, manuf_id) -> HttpResponse:
#     manuf_id = int(manuf_id)
#     try :
#         manufSel = Manufacturer.objects.get(manuf_id = manuf_id)
#     except Manufacturer.DoesNotExist:
#         return redirect('manufacturers')

#     manForm = ManufacturerCreate(request.POST or None, instance=manufSel)
#     if manForm.is_valid():
#         manForm.save()
#         return redirect('manufacturers')
#     return render(request, 'manufacturers_create.html', {'manufacturer': manForm})


# def deleteManuf(request, manuf_id) -> HttpResponse:
#     manuf_id = int(manuf_id)
#     try:
#         manufSel = Manufacturer.objects.get(id=manuf_id)
#     except Manufacturer.DoesNotExist:
#         return redirect('manufacturers')
#     manufSel.delete()
#     return redirect('manufacturers')


class ManufSerializer(serializers.ModelSerializer):
    # renderer_classes = [TemplateHTMLRenderer]
    # template_name = "manufacturer.html"

    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Manufacturer
        fields = "__all__"


class ManufViewSet(ModelViewSet):
    serializer_class = ManufSerializer

    def get_object(self):
        return get_object_or_404(
            Manufacturer, manuf_id=self.request.query_params.get("manuf_id")
        )

    def get_queryset(self):
        return Manufacturer.objects.filter(is_active=True).order_by("-manuf_id")

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class BuildingSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Building
        fields = "__all__"


class BuildingViewSet(ModelViewSet):
    serializer_class = BuildingSerializer

    def get_object(self):
        return get_object_or_404(
            Building, bldg_id=self.request.query_params.get("bldg_id")
        )

    def get_queryset(self):
        return Building.objects.filter(is_active=True).order_by("-bldg_id")

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class PersonSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Person
        fields = "__all__"


class PersonViewSet(ModelViewSet):
    serializer_class = PersonSerializer

    def get_object(self):
        return get_object_or_404(
            Person, person_id=self.request.query_params.get("person_id")
        )

    def get_queryset(self):
        return Person.objects.filter(is_active=True).order_by("-person_id")

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class DepartmentSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Department
        fields = "__all__"


class DepartmentViewSet(ModelViewSet):
    serializer_class = DepartmentSerializer

    def get_object(self):
        return get_object_or_404(
            Department, dept_id=self.request.query_params.get("dept_id")
        )

    def get_queryset(self):
        return Department.objects.filter(is_active=True).order_by("-dept_id")

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class ProjectorSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Manufacturer
        fields = "__all__"


class ProjectorViewSet(ModelViewSet):
    serializer_class = ProjectorSerializer

    def get_object(self):
        return get_object_or_404(
            Projector_Model, model_id=self.request.query_params.get("model_id")
        )

    def get_queryset(self):
        return Projector_Model.objects.filter(is_active=True).order_by("-model_id")

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class PrinterSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Printer_Model
        fields = "__all__"


class PrinterViewSet(ModelViewSet):
    serializer_class = PrinterSerializer

    def get_object(self):
        return get_object_or_404(
            Printer_Model, model_id=self.request.query_params.get("model_id")
        )

    def get_queryset(self):
        return Printer_Model.objects.filter(is_active=True).order_by("-model_id")

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class ComputerSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Computer_Model
        fields = "__all__"


class ComputerViewSet(ModelViewSet):
    serializer_class = ComputerSerializer

    def get_object(self):
        return get_object_or_404(
            Computer_Model, model_id=self.request.query_params.get("model_id")
        )

    def get_queryset(self):
        return Computer_Model.objects.filter(is_active=True).order_by("-model_id")

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class MonitorSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Monitor_Model
        fields = "__all__"


class MonitorViewSet(ModelViewSet):
    serializer_class = MonitorSerializer

    def get_object(self):
        return get_object_or_404(
            Monitor_Model, model_id=self.request.query_params.get("model_id")
        )

    def get_queryset(self):
        return Monitor_Model.objects.filter(is_active=True).order_by("-model_id")

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class NWDeviceSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Nwdevice_Model
        fields = "__all__"


class NWDeviceViewSet(ModelViewSet):
    serializer_class = NWDeviceSerializer

    def get_object(self):
        return get_object_or_404(
            Nwdevice_Model, model_id=self.request.query_params.get("model_id")
        )

    def get_queryset(self):
        return Nwdevice_Model.objects.filter(is_active=True).order_by("-model_id")

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class RoomSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Room
        fields = "__all__"


class RoomViewSet(ModelViewSet):
    serializer_class = RoomSerializer

    def get_object(self):
        return get_object_or_404(Room, room_id=self.request.query_params.get("room_id"))

    def get_queryset(self):
        return Room.objects.filter(is_active=True).order_by("-room_id")

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class RackSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Rack
        fields = "__all__"


class RackViewSet(ModelViewSet):
    serializer_class = RackSerializer

    def get_object(self):
        return get_object_or_404(Rack, rack_id=self.request.query_params.get("rack_id"))

    def get_queryset(self):
        return Rack.objects.filter(is_active=True).order_by("-rack_id")

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class OwnerSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Owner
        fields = "__all__"


class OwnerViewSet(ModelViewSet):
    serializer_class = OwnerSerializer

    def get_object(self):
        return get_object_or_404(
            Owner, person_id=self.request.query_params.get("person_id")
        )

    def get_queryset(self):
        return Owner.objects.filter(is_active=True).order_by("-person_id")

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class ServerSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Server_Model
        fields = "__all__"


class ServerViewSet(ModelViewSet):
    serializer_class = Server_Model

    def get_object(self):
        return get_object_or_404(
            Server_Model, model_id=self.request.query_params.get("model_id")
        )

    def get_queryset(self):
        return Server_Model.objects.filter(is_active=True).order_by("-model_id")

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class CameraSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Camera_Model
        fields = "__all__"


class CameraViewSet(ModelViewSet):
    serializer_class = CameraSerializer

    def get_object(self):
        return get_object_or_404(
            Camera_Model, model_id=self.request.query_params.get("model_id")
        )

    def get_queryset(self):
        return Camera_Model.objects.filter(is_active=True).order_by("-model_id")

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class DeviceSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Device
        fields = "__all__"


class DeviceViewSet(ModelViewSet):
    serializer_class = DeviceSerializer

    def get_object(self):
        return get_object_or_404(
            Device, serial_number=self.request.query_params.get("serial_number")
        )

    def get_queryset(self):
        return Device.objects.filter(is_active=True).order_by("-serial_number")

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()
