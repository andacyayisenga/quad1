from django import forms
from .models import Manufacturer


# class ManufacturerForm(forms.Form):
#     manuf_id = forms.IntegerField(required=True, widget=forms.TextInput())
#     name = forms.CharField(required=True, widget=forms.TextInput())
#     support_site = forms.CharField(required=True, widget=forms.TextInput())

# for receiving maufacturer details
class ManufacturerCreate(forms.ModelForm):
    class Meta:

        model = Manufacturer
        fields = "__all__"
