from os import RTLD_NODELETE, name
from django.core import checks
from django.db import models
from django.db.models import constraints
from django.db.models import deletion
from django.db.models.aggregates import Max
from django.db.models.base import Model
from django.db.models.deletion import CASCADE
from django.db.models.fields.related import OneToOneField
from django.forms import ModelForm

# Create your models here.
class Manufacturer(models.Model):
    manuf_id = models.IntegerField(primary_key=True, null=False)
    name = models.CharField(max_length=50, null=False)
    support_site = models.CharField(max_length=50, null=False)

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "manufacturer"

    def __str__(self):
        return (
            f"manuf_id: {self.manuf_id},\n"
            + f"manuf_name: {self.name},\n"
            + f"supportSite: {self.support_site}\n"
        )


class Building(models.Model):
    bldg_id = models.IntegerField(primary_key=True, null=False)
    campus = models.CharField(max_length=50, null=False)
    name = models.CharField(max_length=50, null=False)
    capacity = models.IntegerField(null=False)

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "building"
        constraints = [
            models.CheckConstraint(check=models.Q(capacity__gt=0), name="capacity_gt_0")
        ]

    def __str__(self) -> str:
        return (
            f"bldg_id: {self.bldg_id},\n"
            + f"campus: {self.campus},\n"
            + f"name: {self.name},\n"
            + f"capacity: {self.capacity}\n"
        )


class Person(models.Model):
    person_id = models.IntegerField(primary_key=True, null=False)
    fname = models.CharField(max_length=50, null=False)
    lname = models.CharField(max_length=50, null=False)
    phone = models.BigIntegerField(null=False)
    email = models.EmailField(null=False)
    role = models.CharField(max_length=50, null=False)

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "person"

    def __str__(self) -> str:
        return (
            f"person_id: {self.person_id},\n"
            + f"fname: {self.fname},\n"
            + f"lname: {self.lname},\n"
            + f"phone: {self.phone},\n"
            + f"email: {self.email},\n"
            + f"role: {self.role}\n"
        )


class Department(models.Model):
    dept_id = models.IntegerField(primary_key=True, null=False)
    name = models.CharField(max_length=50, null=False)
    head = models.CharField(max_length=50, null=False)
    division = models.CharField(max_length=50, null=False)

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "department"

    def __str__(self) -> str:
        return (
            f"dept_id: {self.dept_id},\n"
            + f"name: {self.name},\n"
            + f"head: {self.head},\n"
            + f"division: {self.division}\n"
        )


class Works_in(models.Model):
    person_id = models.OneToOneField(Person, on_delete=CASCADE, null=False)
    bldg_id = models.OneToOneField(Building, on_delete=CASCADE, null=False)

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "works_in"

    def __str__(self) -> str:
        return f"person_id: {self.person_id},\n" + f"bldg_id: {self.bldg_id}\n"


class Member_of(models.Model):
    person_id = models.OneToOneField(Person, on_delete=CASCADE, null=False)
    dept_id = models.ForeignKey(Department, on_delete=CASCADE, null=False)

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "member_of"

    def __str__(self) -> str:
        return f"person_id: {self.person_id},\n" + f"dept_id: {self.dept_id}\n"


class Projector_Model(models.Model):
    model_id = models.IntegerField(primary_key=True, unique=True)
    manuf_id = models.OneToOneField(Manufacturer, on_delete=CASCADE, null=False)
    hdmi = models.IntegerField(null=False)
    wi_fi = models.BooleanField(null=False)
    lumens = models.IntegerField(null=False)
    speakers = models.BooleanField(null=False)
    resolution = models.IntegerField(null=False)

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "projector_model"
        constraints = [
            models.CheckConstraint(
                check=models.Q(lumens__gt=1500, hdmi__gt=0), name="valid_projector"
            )
        ]

    def __str__(self):
        return (
            f"Projector Model: {self.model_id}, \n"
            + f"Manufacturer ID: {self.manuf_id}, \n"
            + f"HDMI: {self.hdmi}, \n"
            + f"Wi-Fi: {self.wi_fi}, \n"
            + f"Lumens: {self.lumens}, \n"
            + f"Speakers: {self.speakers}, \n"
            + f"Resolution: {self.resolution}\n"
        )


class Printer_Model(models.Model):
    model_id = models.IntegerField(primary_key=True, unique=True)
    manuf_id = models.OneToOneField(
        Manufacturer, on_delete=CASCADE, null=False
    )  # ForeignKey
    double_sided = models.BooleanField()
    paper_size = models.CharField(max_length=256)
    memory = models.IntegerField()
    speed = models.IntegerField()
    bw_color = models.CharField(max_length=256)

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "printer_model"

    def __str__(self):
        return (
            f"Printer Model: {self.model_id}, \n"
            + f"Manufacturer ID: {self.manuf_id}, \n"
            + f"Double-sided: {self.double_sided}, \n"
            + f"Paper Size: {self.paper_size}, \n"
            + f"Memory: {self.memory}, \n"
            + f"Speed: {self.speed}, \n"
            + f"BW/Color: {self.bw_color}\n"
        )


class Computer_Model(models.Model):
    model_id = models.IntegerField(primary_key=True, unique=True)
    manuf_id = models.OneToOneField(
        Manufacturer, on_delete=CASCADE, null=False
    )  # ForeignKey
    cpu = models.CharField(max_length=256)
    screen_size = models.IntegerField(blank=True, null=True)
    resolution = models.CharField(max_length=256, blank=True, null=True)
    ram = models.IntegerField()
    os = models.CharField(max_length=256)
    type = models.CharField(max_length=256)
    hd_size = models.IntegerField()

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "computer_model"

    def __str__(self):
        return (
            f"Computer Model: {self.model_id}, \n"
            + f"Manufacturer ID: {self.manuf_id}, \n"
            + f"CPU: {self.cpu}, \n"
            + f"Screen Size: {self.screen_size}, \n"
            + f"Resolution: {self.resolution}, \n"
            + f"RAM: {self.ram}, \n"
            + f"OS: {self.os}, \n"
            + f"Type: {self.type}, \n"
            + f"HD Size: {self.hd_size}\n"
        )


class Monitor_Model(models.Model):
    model_id = models.IntegerField(primary_key=True, unique=True)
    manuf_id = models.OneToOneField(
        Manufacturer, on_delete=CASCADE, null=False
    )  # ForeignKey
    panel_type = models.CharField(max_length=256)
    resolution = models.CharField(max_length=256)
    size = models.IntegerField()
    refresh_rate = models.IntegerField()

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "monitor_model"

    def __str__(self):
        return (
            f"Monitor Model: {self.model_id}, \n"
            + f"Manufacturer ID: {self.manuf_id}, \n"
            + f"Panel Type: {self.panel_type}, \n"
            + f"Resolution: {self.resolution}, \n"
            + f"Size: {self.size}, \n"
            + f"Refresh Rate: {self.refresh_rate}\n"
        )


class Nwdevice_Model(models.Model):
    model_id = models.IntegerField(primary_key=True, unique=True)
    manuf_id = models.OneToOneField(
        Manufacturer, on_delete=CASCADE, null=False
    )  # ForeignKey
    mac_address = models.CharField(max_length=256)
    protocol = models.CharField(max_length=256)
    type = models.CharField(max_length=256)
    speed = models.IntegerField()
    number_ports = models.IntegerField()
    coverage = models.IntegerField()
    number_clients = models.IntegerField()

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "nwdevice_model"

    def __str__(self):
        return (
            f"Network Device Model: {self.model_id}, \n"
            + f"Manufacturer ID: {self.manuf_id}, \n"
            + f"MAC Address: {self.mac_address}, \n"
            + f"Protocol: {self.protocol}, \n"
            + f"Type: {self.type}, \n"
            + f"Speed: {self.speed}, \n"
            + f"Number of Ports: {self.number_ports}, \n"
            + f"Coverage: {self.coverage}, \n"
            + f"Number of Clients: {self.number_clients}\n"
        )


class Room(models.Model):
    room_id = models.IntegerField(primary_key=True, null=False)
    description = models.CharField(max_length=500, null=False)
    bldg_id = models.OneToOneField(Building, on_delete=CASCADE, null=False)

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "room"

    def __str__(self) -> str:
        return (
            f"room_id: {self.room_id},\n"
            + f"description: {self.description},\n"
            + f"bldg_id: {self.bldg_id}\n"
        )


class Rack(models.Model):
    slot = models.IntegerField(null=False)
    rack_id = models.IntegerField(primary_key=True, null=False)
    room_id = models.OneToOneField(Room, on_delete=CASCADE, null=False)

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "rack"

    def __str__(self) -> str:
        return (
            f"slot: {self.slot},\n"
            + f"rack_id: {self.rack_id},\n"
            + f"room_id: {self.room_id}\n"
        )


class Owner(models.Model):
    serial_number = models.IntegerField(null=False)
    person_id = models.OneToOneField(Person, on_delete=CASCADE, null=False)
    dept_id = models.ForeignKey(Department, on_delete=CASCADE, null=False)
    room_id = models.OneToOneField(Room, on_delete=CASCADE, null=False)
    rack_id = models.OneToOneField(Rack, on_delete=CASCADE, null=False)

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "owner"

    def __str__(self) -> str:
        return (
            f"serial_number: {self.serial_number},\n"
            + f"person_id: {self.person_id},\n"
            + f"dept_id: {self.dept_id},\n"
            + f"room_id: {self.room_id},\n"
            + f"rack_id: {self.rack_id}\n"
        )


class Server_Model(models.Model):
    model_id = models.IntegerField(primary_key=True, unique=True)
    cpu = models.CharField(max_length=256)
    ram = models.CharField(max_length=256)
    os = models.CharField(max_length=256)
    hd_size = models.CharField(max_length=256)
    virtual = models.BooleanField()
    manuf_id = models.OneToOneField(
        Manufacturer, on_delete=CASCADE, null=False
    )  # ForeignKey

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "server_Model"

    def __str__(self):
        return (
            f"Server Model: {self.model_id}, \n"
            + f"CPU: {self.cpu}, \n"
            + f"RAM: {self.ram}, \n"
            + f"OS: {self.os}, \n"
            + f"HD Size: {self.hd_size}, \n"
            + f"Virtual: {self.virtual}, \n"
            + f"manuf_id: {self.manuf_id}"
        )


class Camera_Model(models.Model):
    recording = models.BooleanField()
    range_ft = models.IntegerField()
    resolution = models.CharField(max_length=256)
    field_of_view = models.IntegerField()
    model_id = models.IntegerField(primary_key=True, unique=True)
    manuf_id = models.OneToOneField(
        Manufacturer, on_delete=CASCADE, null=False
    )  # ForeignKey

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "camera_Model"

    def __str__(self):
        return (
            f"Camera Model: {self.model_id}, \n"
            + f"Recording: {self.recording}, \n"
            + f"Range (ft): {self.range_ft}, \n"
            + f"Resolution: {self.resolution}, \n"
            + f"Field of View: {self.field_of_view}, \n"
            + f"manuf_id: {self.manuf_id}"
        )


class Device(models.Model):
    serial_number = models.CharField(max_length=256, primary_key=True, unique=True)
    custom_cpu = models.CharField(max_length=256, blank=True, null=True)
    custom_ram = models.CharField(max_length=256, blank=True, null=True)
    custom_hdsize = models.IntegerField(blank=True, null=True)
    year_manufac = models.IntegerField()
    date_purchased = models.DateTimeField()
    os = models.CharField(max_length=256, blank=True, null=True)
    active = models.BooleanField()
    monitor_id = models.OneToOneField(
        Monitor_Model, on_delete=models.CASCADE, blank=True, null=True
    )
    printer_id = models.OneToOneField(
        Printer_Model, on_delete=models.CASCADE, blank=True, null=True
    )
    projector_id = models.OneToOneField(
        Projector_Model, on_delete=models.CASCADE, blank=True, null=True
    )
    camera_id = models.OneToOneField(
        Camera_Model, on_delete=models.CASCADE, blank=True, null=True
    )
    computer_id = models.OneToOneField(
        Computer_Model, on_delete=models.CASCADE, blank=True, null=True
    )
    nwdevice_id = models.OneToOneField(
        Nwdevice_Model, on_delete=models.CASCADE, blank=True, null=True
    )
    server_id = models.OneToOneField(
        Server_Model, on_delete=models.CASCADE, blank=True, null=True
    )

    # to delete model
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "device"

    def __str__(self):
        return (
            f"Device: {self.serial_number}, \n"
            + f"Custom CPU: {self.custom_cpu}, \n"
            + f"Custom RAM: {self.custom_ram}, \n"
            + f"Custom HD Size: {self.custom_hdsize}, \n"
            + f"Year Manufactured: {self.year_manufac}, \n"
            + f"Date Purchased: {self.date_purchased}, \n"
            + f"OS: {self.os}, \n"
            + f"Active: {self.active}, \n"
            + f"Monitor ID: {self.monitor_id}, \n"
            + f"Printer ID: {self.printer_id}, \n"
            + f"Projector ID: {self.projector_id}, \n"
            + f"Camera ID: {self.camera_id}, \n"
            + f"Computer ID: {self.computer_id}, \n"
            + f"Network Device ID: {self.nwdevice_id}, \n"
            + f"Server ID: {self.server_id}"
        )


# TODO : forms
class Device_Form(ModelForm):
    class Meta:
        model = Device
        fields = [
            "serial_number",
            "custom_cpu",
            "custom_ram",
            "custom_hdsize",
            "year_manufac",
            "date_purchased",
            "os",
            "active",
            "monitor_id",
            "printer_id",
            "projector_id",
            "camera_id",
            "computer_id",
            "nwdevice_id",
            "server_id",
        ]


class Server_Form(ModelForm):
    class Meta:
        model = Server_Model
        fields = ["model_id", "cpu", "ram", "os", "hd_size", "virtual", "manuf_id"]


class Camera_Form(ModelForm):
    class Meta:
        model = Camera_Model
        fields = [
            "model_id",
            "manuf_id",
            "recording",
            "resolution",
            "field_of_view",
            "range_ft",
        ]


class Projector_Form(ModelForm):
    class Meta:
        model = Projector_Model
        fields = [
            "model_id",
            "manuf_id",
            "lumens",
            "speakers",
            "resolution",
            "wi_fi",
            "hdmi",
        ]


class Printer_Form(ModelForm):
    class Meta:
        model = Printer_Model
        fields = [
            "model_id",
            "manuf_id",
            "double_sided",
            "paper_size",
            "memory",
            "speed",
            "bw_color",
        ]


class Computer_Form(ModelForm):
    class Meta:
        model = Computer_Model
        fields = [
            "model_id",
            "manuf_id",
            "cpu",
            "screen_size",
            "resolution",
            "ram",
            "os",
            "type",
            "hd_size",
        ]


class Monitor_Form(ModelForm):
    class Meta:
        model = Monitor_Model
        fields = [
            "model_id",
            "manuf_id",
            "panel_type",
            "resolution",
            "size",
            "refresh_rate",
        ]


class Nwdevice_Form(ModelForm):
    class Meta:
        model = Nwdevice_Model
        fields = [
            "model_id",
            "manuf_id",
            "mac_address",
            "protocol",
            "type",
            "speed",
            "number_ports",
            "coverage",
            "number_clients",
        ]


class Rack_Form(ModelForm):
    class Meta:
        model = Rack
        fields = ["rack_id", "slot", "room_id"]
