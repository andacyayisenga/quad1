from django.http import HttpResponse
from django.template import loader


def extended(request) -> HttpResponse:
    print(request.user)
    template = loader.get_template("extended.html")
    return HttpResponse(template.render({}, request))
