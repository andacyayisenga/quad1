--filldb.info was used for data population
--sqlines application was used for syntax conversion

DROP TABLE device CASCADE;

DROP TABLE server_model CASCADE;

DROP TABLE camera_model CASCADE;

DROP TABLE projector_model CASCADE;

DROP TABLE printer_model CASCADE;

DROP TABLE computer_model CASCADE;

DROP TABLE monitor_model CASCADE;

DROP TABLE nwdevice_model CASCADE;

DROP TABLE device_owner CASCADE;

DROP TABLE works_in CASCADE;

DROP TABLE member_of CASCADE;

DROP TABLE rack CASCADE;

DROP TABLE room CASCADE;

DROP TABLE manufacturer CASCADE;

DROP TABLE building CASCADE;

DROP TABLE person CASCADE;

DROP TABLE department CASCADE;

CREATE TABLE manufacturer
(
  manuf_id INT NOT NULL,
  name VARCHAR(50) NOT NULL,
  support_site VARCHAR(50) NOT NULL,
  is_active BOOLEAN NOT NULL,
  PRIMARY KEY (manuf_id)
);

CREATE TABLE building
(
  bldg_id INT NOT NULL,
  campus VARCHAR(50) NOT NULL,
  name VARCHAR(50) NOT NULL,
  capacity INT NOT NULL CHECK(capacity > 0),
  is_active BOOLEAN NOT NULL,
  PRIMARY KEY (bldg_id)
);

CREATE TABLE person
(
  person_id INT NOT NULL,
  fname VARCHAR(50) NOT NULL,
  lname VARCHAR(50) NOT NULL,
  phone BIGINT NOT NULL,
  email VARCHAR(50) NOT NULL,
  role VARCHAR(50) NOT NULL,
  is_active BOOLEAN NOT NULL,
  PRIMARY KEY (person_id)
);

CREATE TABLE department
(
  dept_id INT NOT NULL,
  name VARCHAR(50) NOT NULL,
  head VARCHAR(50) NOT NULL,
  division VARCHAR(50) NOT NULL,
  is_active BOOLEAN NOT NULL,
  PRIMARY KEY (dept_id)
);

CREATE TABLE server_model
(
  model_id INT NOT NULL,
  ram INT NOT NULL,
  os VARCHAR(50) NOT NULL,
  hd_size INT NOT NULL,
  virtual BOOLEAN NOT NULL,
  manuf_id INT NOT NULL REFERENCES manufacturer(manuf_id),
  is_active BOOLEAN NOT NULL,
  PRIMARY KEY (model_id)
);

CREATE TABLE works_in
(
  person_id INT NOT NULL REFERENCES person(person_id),
  bldg_id INT NOT NULL REFERENCES building(bldg_id),
  is_active BOOLEAN NOT NULL
);

CREATE TABLE member_of
(
  person_id INT NOT NULL REFERENCES person(person_id),
  dept_id INT NOT NULL REFERENCES department(dept_id),
  is_active BOOLEAN NOT NULL
);

CREATE TABLE camera_model
(
  recording BOOLEAN NOT NULL,
  range INT NOT NULL,
  resolution INT CHECK(resolution > 0),
  field_of_view INT NOT NULL,
  model_id INT NOT NULL,
  manuf_id INT NOT NULL REFERENCES manufacturer(manuf_id),
  is_active BOOLEAN NOT NULL,
  PRIMARY KEY (model_id)
);

CREATE TABLE projector_model
(
  model_id INT NOT NULL,
  hdmi INT NOT NULL,
  wifi BOOLEAN NOT NULL,
  lumens INT NOT NULL,
  speakers BOOLEAN NOT NULL,
  resolution INT NOT NULL,
  manuf_id INT NOT NULL REFERENCES manufacturer(manuf_id),
  is_active BOOLEAN NOT NULL,
  PRIMARY KEY (model_id),
);

CREATE TABLE printer_model
(
  model_id INT NOT NULL,
  double_sided BOOLEAN NOT NULL,
  paper_size VARCHAR(10) NOT NULL,
  memory INT NOT NULL,
  speed INT NOT NULL,
  laser BOOLEAN NOT NULL,
  bw_color VARCHAR(50) NOT NULL,
  is_active BOOLEAN NOT NULL,
  manuf_id INT NOT NULL REFERENCES manufacturer(manuf_id),
  PRIMARY KEY (model_id)
);

CREATE TABLE computer_model
(
  model_id INT NOT NULL,
  cpu VARCHAR(50) NOT NULL,
  screen_size INT,
  resolution INT,
  ram INT NOT NULL,
  os VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  hd_size INT NOT NULL,
  is_active BOOLEAN NOT NULL,
  manuf_id INT NOT NULL REFERENCES manufacturer(manuf_id),
  PRIMARY KEY (model_id)
);

CREATE TABLE monitor_model
(
  model_id INT NOT NULL,
  hdmi INT NOT NULL,
  panel_type VARCHAR(50) NOT NULL,
  resolution INT NOT NULL,
  size INT NOT NULL,
  refresh_rate INT NOT NULL,
  is_active BOOLEAN NOT NULL,
  manuf_id INT NOT NULL REFERENCES manufacturer(manuf_id),
  PRIMARY KEY (model_id)
);

CREATE TABLE nwdevice_models
(
  model_id INT NOT NULL,
  mac_address VARCHAR(50) NOT NULL,
  protocol VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  speed INT NOT NULL,
  number_ports INT NOT NULL,
  coverage INT NOT NULL,
  number_clients INT NOT NULL,
  is_active BOOLEAN NOT NULL,
  manuf_id INT NOT NULL REFERENCES manufacturer(manuf_id),
  PRIMARY KEY (model_id)
);

CREATE TABLE device
(
  serial_number INT NOT NULL,
  custom_cpu VARCHAR(50),
  custom_ram INT,
  custom_hdsize INT,
  year_manufac INT NOT NULL,
  date_purchased VARCHAR(50) NOT NULL,
  os VARCHAR(50) NOT NULL,
  is_active BOOLEAN NOT NULL,
  nwmodel_id INT REFERENCES nwdevice_models(model_id),
  monitormodel_id INT REFERENCES monitor_model(model_id),
  cameramodel_id INT REFERENCES camera_model(model_id),
  projectormodel_id INT REFERENCES projector_model(model_id),
  printermodel_id INT REFERENCES printer_model(model_id),
  computermodel_id INT REFERENCES computer_model(model_id)
);

CREATE TABLE room
(
  room_id INT NOT NULL,
  description VARCHAR(50) NOT NULL,
  is_active BOOLEAN NOT NULL,
  bldg_id INT NOT NULL REFERENCES building(bldg_id),
  PRIMARY KEY (room_id)
);

CREATE TABLE rack
(
  slot INT NOT NULL,
  rack_id INT NOT NULL,
  is_active BOOLEAN NOT NULL,
  room_id INT NOT NULL REFERENCES room(room_id),
  PRIMARY KEY (rack_id)
);

CREATE TABLE owner
(
  serial_number INT NOT NULL,
  is_active BOOLEAN NOT NULL,
  person_id INT NOT NULL REFERENCES person(person_id),
  dept_id INT NOT NULL REFERENCES department(dept_id),
  room_id INT NOT NULL REFERENCES room(room_id),
  rack_id INT NOT NULL REFERENCES rack(rack_id)
);

INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (1, 'UCSB', 'Page', 36, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (2, 'SBBC', 'Armington C', 604, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (3, 'UCSB', 'Porter Theatre', 270, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (4, 'Westmont', 'Winter', 248, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (5, 'SBBC', 'Mail Center', 48, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (6, 'SBBC', 'Bookstore', 884, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (7, 'UCSB', 'Deane Hall', 1270, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (8, 'Westmont', 'Music Building', 669, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (9, 'SBBC', 'Carroll Hall', 206, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (10, 'SBBC', 'Deane Chapel', 957, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (11, 'Westmont', 'Reynolds Hall', 1070, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (12, 'SBBC', 'Ritchies', 726, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (13, 'Westmont', 'Art Museum', 998, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (14, 'Westmont', 'GLC West', 494, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (15, 'UCSB', 'Whittier Hall', 560, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (16, 'Westmont', 'VK Hall', 542, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (17, 'SBBC', 'Bio Lab', 1461, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (18, 'UCSB', 'Clark A', 1348, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (19, 'Westmont', 'Murchison Gym', 733, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (20, 'UCSB', 'Voskuyl Library', 595, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (21, 'SBBC', 'Winter Hall', 1382, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (22, 'SBBC', 'Emerson', 1048, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (23, 'SBBC', 'Adams', 210, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (24, 'Westmont', 'GLC East', 1277, 1);
INSERT INTO building (bldg_id, campus, name, capacity, is_active) VALUES (25, 'UCSB', 'Observatory', 20, 1);

INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 53, 3814, 93, 1, 20, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 85, 484, 72, 2, 19, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 99, 1397, 66, 3, 25, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 85, 891, 82, 4, 1, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 32, 4029, 78, 5, 17, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 31, 3438, 70, 6, 14, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 21, 4313, 60, 7, 4, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 91, 1814, 89, 8, 7, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 22, 2753, 87, 9, 2, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 91, 1185, 72, 10, 13, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 27, 740, 75, 11, 20, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 76, 3678, 81, 12, 5, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 77, 3836, 78, 13, 9, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 86, 2672, 96, 14, 19, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 44, 1890, 73, 15, 18, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 58, 684, 65, 16, 11, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 48, 3802, 64, 17, 4, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 74, 1779, 66, 18, 15, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 11, 1838, 96, 19, 12, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 69, 4197, 90, 20, 24, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 89, 4090, 87, 21, 10, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 12, 1565, 82, 22, 3, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 57, 949, 79, 23, 9, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 97, 2368, 61, 24, 12, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 89, 890, 78, 25, 15, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 96, 2625, 91, 26, 10, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 63, 4143, 71, 27, 15, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 55, 1346, 96, 28, 8, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 49, 3405, 68, 29, 16, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 22, 3944, 77, 30, 6, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 12, 360, 76, 31, 14, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 81, 477, 78, 32, 21, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 23, 641, 96, 33, 13, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 91, 3936, 91, 34, 22, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 35, 394, 93, 35, 23, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 36, 3474, 71, 36, 18, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 71, 1727, 71, 37, 1, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 94, 2668, 70, 38, 9, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 34, 1310, 90, 39, 19, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 81, 3270, 73, 40, 5, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 70, 3428, 85, 41, 2, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 26, 3825, 89, 42, 14, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 21, 3020, 78, 43, 16, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 51, 821, 78, 44, 7, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 69, 1821, 81, 45, 8, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 85, 3518, 78, 46, 8, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 71, 3087, 70, 47, 23, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 46, 2758, 89, 48, 25, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 11, 2826, 65, 49, 1, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 62, 3815, 96, 50, 11, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 94, 3671, 70, 51, 6, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 30, 2811, 91, 52, 7, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 54, 1552, 84, 53, 20, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 20, 2625, 86, 54, 3, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 64, 3501, 62, 55, 21, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 20, 762, 62, 56, 18, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 22, 1546, 75, 57, 2, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 98, 3369, 61, 58, 13, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 62, 3062, 92, 59, 17, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 60, 3075, 63, 60, 16, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 95, 489, 97, 61, 11, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 79, 3368, 95, 62, 10, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 65, 1645, 91, 63, 12, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 84, 1911, 77, 64, 4, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 67, 615, 73, 65, 17, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 18, 3616, 83, 66, 5, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 68, 1576, 65, 67, 22, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (0, 73, 2988, 93, 68, 3, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 56, 3474, 79, 69, 24, 1);
INSERT INTO camera_model (recording, range, resolution, field_of_view, model_id, manuf_id, is_active) VALUES (1, 78, 3348, 62, 70, 6, 1);

INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (1, 'AMD', 0, 1873, 233, 'Mac', 'Laptop', 1250, 1, 14);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (2, 'Apple M', 0, 2134, 225, 'Linux', 'Desktop', 1474, 1, 5);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (3, 'AMD', 13, 2100, 109, 'Windows', 'Laptop', 383, 1, 15);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (4, 'Apple M', 12, 1369, 216, 'Windows', 'Laptop', 1791, 1, 16);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (5, 'Apple M', 0, 1930, 103, 'Mac', 'Desktop', 437, 1, 4);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (6, 'Apple M', 0, 1356, 94, 'Windows', 'Desktop', 1686, 1, 19);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (7, 'AMD', 0, 0, 92, 'Mac', 'Laptop', 1873, 1, 1);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (8, 'ARM', 11, 1670, 64, 'Windows', 'Laptop', 1004, 1, 20);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (9, 'AMD', 0, 2109, 192, 'Linux', 'Laptop', 563, 1, 10);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (10, 'ARM', 12, 0, 150, 'Windows', 'Laptop', 693, 1, 5);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (11, 'Intel', 13, 0, 66, 'Linux', 'Laptop', 355, 1, 14);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (12, 'ARM', 12, 0, 6, 'Mac', 'Laptop', 347, 1, 19);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (13, 'Intel', 0, 1817, 110, 'Mac', 'Desktop', 1147, 1, 23);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (14, 'AMD', 0, 0, 15, 'Mac', 'Laptop', 667, 1, 11);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (15, 'AMD', 0, 1879, 185, 'Windows', 'Laptop', 1219, 1, 9);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (16, 'ARM', 0, 0, 42, 'Mac', 'Laptop', 1246, 1, 18);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (17, 'Intel', 12, 1166, 90, 'Mac', 'Desktop', 1847, 1, 25);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (18, 'Intel', 0, 0, 83, 'Windows', 'Laptop', 1005, 1, 6);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (19, 'Intel', 0, 0, 67, 'Linux', 'Laptop', 710, 1, 22);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (20, 'AMD', 0, 0, 147, 'Mac', 'Desktop', 795, 1, 3);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (21, 'Intel', 12, 2126, 40, 'Mac', 'Desktop', 372, 1, 1);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (22, 'Intel', 13, 0, 28, 'Windows', 'Laptop', 936, 1, 4);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (23, 'Intel', 13, 1218, 101, 'Linux', 'Desktop', 724, 1, 5);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (24, 'Apple M', 0, 0, 84, 'Linux', 'Laptop', 337, 1, 1);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (25, 'Apple M', 11, 0, 222, 'Linux', 'Laptop', 925, 1, 5);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (26, 'ARM', 11, 1536, 123, 'Windows', 'Desktop', 1610, 1, 23);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (27, 'AMD', 0, 0, 63, 'Linux', 'Desktop', 1834, 1, 20);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (28, 'ARM', 0, 1172, 243, 'Windows', 'Laptop', 852, 1, 24);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (29, 'AMD', 13, 0, 169, 'Mac', 'Laptop', 1396, 1, 16);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (30, 'Apple M', 13, 1461, 17, 'Linux', 'Desktop', 1506, 1, 18);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (31, 'ARM', 13, 0, 78, 'Linux', 'Desktop', 741, 1, 2);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (32, 'ARM', 0, 0, 30, 'Windows', 'Desktop', 1523, 1, 1);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (33, 'Apple M', 0, 0, 161, 'Windows', 'Laptop', 1611, 1, 7);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (34, 'ARM', 13, 1963, 52, 'Mac', 'Desktop', 1250, 1, 22);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (35, 'AMD', 0, 1411, 119, 'Mac', 'Desktop', 917, 1, 11);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (36, 'Apple M', 0, 0, 119, 'Windows', 'Laptop', 1377, 1, 25);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (37, 'AMD', 0, 1904, 75, 'Windows', 'Desktop', 605, 1, 24);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (38, 'Apple M', 12, 1258, 115, 'Windows', 'Laptop', 132, 1, 10);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (39, 'Apple M', 0, 0, 68, 'Windows', 'Desktop', 556, 1, 3);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (40, 'AMD', 11, 1217, 195, 'Windows', 'Desktop', 465, 1, 21);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (41, 'Apple M', 12, 1389, 174, 'Mac', 'Laptop', 1044, 1, 12);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (42, 'Intel', 0, 0, 238, 'Windows', 'Desktop', 1348, 1, 9);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (43, 'ARM', 0, 0, 247, 'Mac', 'Laptop', 1493, 1, 18);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (44, 'ARM', 0, 0, 36, 'Linux', 'Laptop', 312, 1, 2);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (45, 'AMD', 0, 0, 63, 'Windows', 'Desktop', 973, 1, 19);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (46, 'Intel', 11, 1476, 69, 'Mac', 'Desktop', 521, 1, 6);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (47, 'ARM', 0, 0, 153, 'Windows', 'Laptop', 1411, 1, 3);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (48, 'Apple M', 0, 1940, 107, 'Linux', 'Desktop', 1830, 1, 15);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (49, 'Intel', 0, 1999, 17, 'Linux', 'Laptop', 903, 1, 10);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (50, 'Apple M', 0, 0, 12, 'Mac', 'Desktop', 884, 1, 13);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (51, 'AMD', 0, 1992, 154, 'Linux', 'Desktop', 428, 1, 14);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (52, 'ARM', 11, 2017, 72, 'Windows', 'Desktop', 1768, 1, 4);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (53, 'AMD', 0, 1094, 187, 'Linux', 'Desktop', 1330, 1, 2);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (54, 'Apple M', 0, 2142, 59, 'Windows', 'Laptop', 941, 1, 8);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (55, 'Intel', 0, 1792, 83, 'Linux', 'Laptop', 1162, 1, 17);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (56, 'ARM', 0, 0, 114, 'Mac', 'Laptop', 979, 1, 21);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (57, 'AMD', 13, 1511, 32, 'Linux', 'Desktop', 657, 1, 9);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (58, 'AMD', 0, 1309, 196, 'Linux', 'Desktop', 878, 1, 15);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (59, 'ARM', 12, 1904, 167, 'Linux', 'Laptop', 272, 1, 25);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (60, 'Apple M', 13, 0, 123, 'Windows', 'Desktop', 1478, 1, 17);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (61, 'AMD', 0, 0, 28, 'Mac', 'Laptop', 1466, 1, 7);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (62, 'Intel', 11, 1351, 186, 'Linux', 'Desktop', 531, 1, 20);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (63, 'ARM', 13, 0, 148, 'Windows', 'Desktop', 845, 1, 13);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (64, 'Apple M', 0, 0, 18, 'Windows', 'Laptop', 512, 1, 11);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (65, 'Apple M', 11, 0, 78, 'Linux', 'Laptop', 195, 1, 8);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (66, 'Apple M', 11, 1870, 119, 'Linux', 'Desktop', 928, 1, 13);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (67, 'AMD', 12, 0, 16, 'Windows', 'Laptop', 1276, 1, 24);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (68, 'ARM', 13, 0, 180, 'Mac', 'Desktop', 309, 1, 3);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (69, 'Apple M', 12, 1849, 246, 'Mac', 'Laptop', 1696, 1, 8);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (70, 'AMD', 11, 0, 125, 'Mac', 'Desktop', 1394, 1, 21);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (71, 'Intel', 0, 1975, 189, 'Linux', 'Desktop', 1345, 1, 12);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (72, 'Intel', 13, 1157, 33, 'Linux', 'Laptop', 1755, 1, 2);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (73, 'ARM', 13, 0, 247, 'Windows', 'Laptop', 1837, 1, 22);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (74, 'ARM', 11, 0, 140, 'Windows', 'Desktop', 1585, 1, 12);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (75, 'AMD', 13, 1704, 227, 'Mac', 'Desktop', 410, 1, 6);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (76, 'ARM', 12, 1218, 156, 'Linux', 'Desktop', 678, 1, 7);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (77, 'ARM', 0, 1872, 8, 'Mac', 'Desktop', 1036, 1, 17);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (78, 'Apple M', 0, 1749, 15, 'Linux', 'Laptop', 1865, 1, 4);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (79, 'ARM', 11, 1526, 125, 'Windows', 'Laptop', 1180, 1, 23);
INSERT INTO computer_model (model_id, cpu, screen_size, resolution, ram, os, type, hd_size, is_active, manuf_id) VALUES (80, 'ARM', 12, 0, 165, 'Windows', 'Laptop', 1837, 1, 16);

INSERT INTO department (dept_id, name, head, division, is_active) VALUES (1, 'Computer Science', 'Mrs. Bettye Renner III', 'academic', 1);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (2, 'Dining Services', 'Stacey Stark I', 'admin', 1);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (3, 'Registrar', 'Ansel Beer', 'academic', 1);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (4, 'Communications', 'Miss Rhea Kohler', 'admin', 1);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (5, 'Mathematics', 'Kaylee Runolfsson DVM', 'academic', 1);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (6, 'English', 'Prof. Malvina Haag MD', 'admin', 1);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (7, 'History', 'Deontae Okuneva', 'admin', 1);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (8, 'Political Science', 'Angus Mann', 'academic', 1);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (9, 'Physics', 'Maci Schneider', 'academic', 1);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (10, 'Philosophy', 'Rocky Hahn', 'academic', 1);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (11, 'Music', 'Mr. Isaac Metz', 'admin', 1);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (12, 'Theatre', 'Marilie Luettgen', 'academic', 1);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (13, 'Kinesiology', 'Emilio Ondricka', 'academic', 1);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (14, 'Data Analytics', 'Dr. Nikki McClure PhD', 'academic', 1);
INSERT INTO department (dept_id, name, head, division, is_active) VALUES (15, 'Admissions', 'Kelley Hudson III', 'admin', 1);

INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31790, '', 12, 1523, 2014, '2019-11-27 04:59:50', 'Mac OSX', 1, 1, 1, 1, 1, 1, 1);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35751, 'Intel', 23, 0, 2014, '2020-02-12 02:46:24', 'Mac OSX', 1, 2, 2, 2, 3, 2, 2);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16266, '', 0, 0, 2014, '2021-09-23 00:49:39', 'Linux', 1, 3, 3, 3, 4, 3, 3);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17589, 'Intel', 0, 1553, 2013, '2018-08-01 18:10:14', 'Mac OSX', 1, 4, 4, 4, 6, 4, 4);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21038, '', 0, 0, 2016, '2019-08-22 02:02:29', 'Linux', 1, 5, 5, 5, 8, 5, 5);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35523, '', 63, 341, 2014, '2018-05-30 23:01:46', 'Linux', 1, 6, 6, 6, 9, 6, 6);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11608, 'ARM', 0, 0, 2016, '2017-08-14 07:56:13', 'Mac OSX', 1, 7, 7, 7, 10, 7, 7);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35713, '', 0, 1992, 2013, '2021-06-13 17:42:26', 'Mac OSX', 1, 8, 8, 8, 11, 8, 8);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34543, '', 0, 0, 2012, '2018-02-07 13:14:49', 'Linux', 1, 9, 9, 9, 12, 9, 9);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38688, 'Intel', 100, 0, 2012, '2017-12-21 07:57:02', 'Mac OSX', 1, 10, 10, 10, 13, 10, 10);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34537, '', 0, 0, 2015, '2018-01-16 12:52:41', 'Windows', 1, 11, 11, 11, 15, 11, 11);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18517, 'Intel', 239, 0, 2015, '2020-09-08 02:15:21', 'Mac OSX', 1, 12, 12, 12, 17, 12, 12);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15995, 'ARM', 0, 1078, 2015, '2021-01-11 12:36:43', 'Mac OSX', 1, 13, 13, 13, 18, 13, 13);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25237, '', 154, 976, 2013, '2021-06-27 09:48:45', 'Mac OSX', 1, 14, 14, 14, 19, 14, 14);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11299, 'ARM', 0, 695, 2013, '2021-01-13 17:03:57', 'Linux', 1, 15, 15, 15, 20, 15, 15);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22387, '', 89, 0, 2013, '2018-09-29 22:45:24', 'Windows', 1, 16, 16, 16, 22, 16, 16);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24533, '', 44, 0, 2013, '2021-09-30 01:55:31', 'Windows', 1, 17, 17, 17, 25, 17, 17);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26677, 'AMD', 86, 1126, 2013, '2020-08-17 11:24:20', 'Linux', 1, 18, 18, 18, 27, 18, 18);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23489, 'ARM', 66, 0, 2016, '2020-03-27 02:09:23', 'Windows', 1, 19, 19, 19, 37, 19, 19);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36775, 'ARM', 0, 0, 2015, '2019-12-27 11:38:57', 'Windows', 1, 20, 20, 20, 38, 20, 20);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29993, '', 149, 2043, 2012, '2019-01-16 12:33:10', 'Linux', 1, 21, 21, 21, 45, 21, 21);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37919, 'AMD', 159, 0, 2014, '2021-07-17 02:47:47', 'Mac OSX', 1, 22, 22, 22, 47, 22, 22);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36189, 'Intel', 189, 399, 2012, '2019-12-29 20:38:58', 'Mac OSX', 1, 23, 23, 23, 48, 23, 23);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34186, 'AMD', 0, 0, 2014, '2017-02-25 16:38:40', 'Windows', 1, 24, 24, 24, 52, 24, 24);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31156, 'ARM', 90, 327, 2012, '2020-07-01 20:14:42', 'Windows', 1, 25, 25, 25, 53, 25, 25);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32060, 'Intel', 148, 0, 2012, '2018-12-12 10:49:02', 'Windows', 1, 26, 26, 26, 54, 26, 26);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14129, '', 177, 0, 2014, '2019-07-03 11:00:56', 'Linux', 1, 27, 27, 27, 56, 27, 27);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19875, '', 0, 572, 2015, '2017-10-26 21:21:26', 'Windows', 1, 28, 28, 28, 57, 28, 28);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35378, 'AMD', 0, 0, 2012, '2017-02-24 02:25:27', 'Linux', 1, 29, 29, 29, 58, 29, 29);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21751, 'ARM', 0, 357, 2014, '2017-09-05 22:56:59', 'Mac OSX', 1, 30, 30, 30, 60, 30, 30);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20108, 'Intel', 0, 0, 2016, '2020-03-30 16:59:00', 'Windows', 1, 31, 31, 31, 61, 31, 31);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37844, 'Intel', 241, 0, 2013, '2016-12-26 13:36:06', 'Mac OSX', 1, 32, 32, 32, 66, 32, 32);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21592, '', 165, 0, 2013, '2020-11-21 09:27:56', 'Linux', 1, 33, 33, 33, 67, 33, 33);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15098, 'AMD', 0, 0, 2015, '2019-06-18 03:11:09', 'Windows', 1, 34, 34, 34, 69, 34, 34);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29935, '', 0, 0, 2016, '2019-04-10 14:45:36', 'Linux', 1, 35, 35, 35, 1, 35, 35);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27782, 'AMD', 156, 367, 2013, '2020-05-03 04:22:09', 'Mac OSX', 1, 36, 36, 36, 3, 36, 36);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24747, 'ARM', 0, 0, 2015, '2020-12-28 04:53:10', 'Windows', 1, 37, 37, 37, 4, 37, 37);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24760, '', 101, 0, 2013, '2017-01-15 11:34:03', 'Mac OSX', 1, 38, 38, 38, 6, 38, 38);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15735, 'ARM', 0, 0, 2013, '2017-05-25 12:43:29', 'Windows', 1, 39, 39, 39, 8, 39, 39);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34129, 'ARM', 0, 0, 2013, '2018-02-06 12:33:59', 'Windows', 1, 40, 40, 40, 9, 40, 40);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24891, '', 0, 0, 2015, '2021-04-01 17:04:56', 'Mac OSX', 1, 41, 41, 41, 10, 41, 41);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14740, '', 30, 0, 2015, '2017-01-09 21:05:39', 'Linux', 1, 42, 42, 42, 11, 42, 42);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20705, 'Intel', 220, 521, 2014, '2021-07-07 20:51:23', 'Windows', 1, 43, 43, 43, 12, 43, 43);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21718, '', 123, 0, 2013, '2017-10-22 13:58:56', 'Mac OSX', 1, 44, 44, 44, 13, 44, 44);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35632, '', 161, 1522, 2014, '2021-09-15 07:35:09', 'Linux', 1, 45, 45, 45, 15, 45, 45);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18915, 'ARM', 192, 693, 2015, '2017-03-14 19:31:17', 'Mac OSX', 1, 46, 46, 46, 17, 46, 46);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26242, '', 119, 0, 2013, '2017-09-19 22:12:15', 'Windows', 1, 47, 47, 47, 18, 47, 47);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18120, '', 0, 0, 2014, '2020-09-26 10:03:50', 'Mac OSX', 1, 48, 48, 48, 19, 48, 48);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10057, 'AMD', 14, 399, 2013, '2021-07-11 12:18:58', 'Linux', 1, 49, 49, 49, 20, 49, 49);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11044, 'ARM', 246, 0, 2014, '2020-06-25 02:08:36', 'Windows', 1, 50, 50, 50, 22, 50, 50);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36564, 'Intel', 15, 334, 2014, '2018-09-08 08:04:32', 'Windows', 1, 51, 51, 51, 25, 51, 51);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36215, '', 121, 1228, 2012, '2019-04-04 12:57:05', 'Windows', 1, 52, 52, 52, 27, 52, 52);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25168, 'ARM', 142, 1829, 2013, '2021-01-16 11:22:43', 'Mac OSX', 1, 53, 53, 53, 37, 53, 53);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22756, 'AMD', 0, 0, 2016, '2018-06-27 00:02:57', 'Mac OSX', 1, 54, 54, 54, 38, 54, 54);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16109, '', 0, 1338, 2013, '2019-08-30 07:27:09', 'Linux', 1, 55, 55, 55, 45, 55, 55);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23941, '', 0, 0, 2016, '2019-01-25 01:20:36', 'Linux', 1, 56, 56, 56, 47, 56, 56);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28879, '', 0, 0, 2012, '2018-01-20 17:48:07', 'Mac OSX', 1, 57, 57, 57, 48, 57, 57);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33717, '', 64, 0, 2016, '2018-07-09 13:31:08', 'Windows', 1, 58, 58, 58, 52, 58, 58);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34720, '', 0, 0, 2014, '2021-07-28 18:04:35', 'Windows', 1, 59, 59, 59, 53, 59, 59);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35909, 'AMD', 110, 0, 2012, '2017-05-27 23:18:59', 'Linux', 1, 60, 60, 60, 54, 60, 60);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28068, 'AMD', 86, 112, 2013, '2017-11-12 20:30:42', 'Windows', 1, 61, 61, 61, 56, 61, 61);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23926, '', 0, 0, 2012, '2020-10-23 17:35:46', 'Mac OSX', 1, 62, 62, 62, 57, 62, 62);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14360, 'ARM', 99, 0, 2015, '2020-01-16 17:27:12', 'Windows', 1, 63, 63, 63, 58, 63, 63);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34605, '', 49, 0, 2016, '2021-12-03 11:04:56', 'Windows', 1, 64, 64, 64, 60, 64, 64);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21148, 'Intel', 207, 0, 2015, '2018-01-05 06:57:22', 'Mac OSX', 1, 65, 65, 65, 61, 65, 65);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34554, 'ARM', 167, 1442, 2015, '2018-10-02 00:20:40', 'Mac OSX', 1, 66, 66, 66, 66, 66, 66);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17878, 'ARM', 0, 0, 2012, '2017-01-22 07:24:31', 'Linux', 1, 67, 67, 67, 67, 67, 67);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35200, 'ARM', 0, 1154, 2015, '2017-02-07 06:35:55', 'Linux', 1, 68, 68, 68, 69, 68, 68);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38549, '', 53, 1469, 2015, '2018-01-02 14:58:46', 'Windows', 1, 69, 69, 69, 1, 69, 69);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28747, '', 227, 566, 2014, '2018-07-05 18:16:55', 'Linux', 1, 70, 70, 70, 3, 70, 70);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24746, '', 0, 0, 2014, '2018-03-21 02:55:37', 'Windows', 1, 1, 1, 1, 4, 1, 71);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24361, '', 236, 0, 2016, '2019-12-14 20:48:07', 'Mac OSX', 1, 2, 2, 2, 6, 2, 72);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12055, 'AMD', 0, 0, 2013, '2021-11-03 06:16:38', 'Windows', 1, 3, 3, 3, 8, 3, 73);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11850, 'Intel', 116, 180, 2016, '2021-08-22 08:13:37', 'Windows', 1, 4, 4, 4, 9, 4, 74);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26632, '', 29, 0, 2013, '2021-06-18 18:07:33', 'Linux', 1, 5, 5, 5, 10, 5, 75);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10034, '', 146, 1092, 2016, '2019-11-21 16:05:03', 'Mac OSX', 1, 6, 6, 6, 11, 6, 76);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33606, 'Intel', 0, 0, 2012, '2019-11-15 04:24:58', 'Mac OSX', 1, 7, 7, 7, 12, 7, 77);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11389, '', 0, 0, 2014, '2018-02-23 08:28:08', 'Linux', 1, 8, 8, 8, 13, 8, 78);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20433, '', 98, 0, 2012, '2020-06-05 22:21:18', 'Linux', 1, 9, 9, 9, 15, 9, 79);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36037, '', 0, 1512, 2016, '2021-05-18 16:04:07', 'Mac OSX', 1, 10, 10, 10, 17, 10, 80);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36327, 'Intel', 0, 872, 2016, '2017-09-15 02:55:59', 'Linux', 1, 11, 11, 11, 18, 11, 1);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30780, '', 83, 0, 2012, '2021-02-12 16:25:21', 'Mac OSX', 1, 12, 12, 12, 19, 12, 2);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16273, 'ARM', 0, 1952, 2013, '2017-12-27 22:17:46', 'Windows', 1, 13, 13, 13, 20, 13, 3);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37747, 'ARM', 136, 0, 2016, '2021-03-11 21:27:20', 'Linux', 1, 14, 14, 14, 22, 14, 4);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18126, 'ARM', 0, 0, 2012, '2019-11-29 10:57:10', 'Mac OSX', 1, 15, 15, 15, 25, 15, 5);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38596, 'Intel', 0, 0, 2012, '2021-07-18 12:47:58', 'Mac OSX', 1, 16, 16, 16, 27, 16, 6);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24393, 'Intel', 111, 0, 2012, '2018-07-03 21:53:52', 'Windows', 1, 17, 17, 17, 37, 17, 7);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28482, '', 0, 326, 2012, '2018-03-29 12:46:24', 'Windows', 1, 18, 18, 18, 38, 18, 8);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31714, 'ARM', 176, 1863, 2015, '2017-02-28 21:56:44', 'Linux', 1, 19, 19, 19, 45, 19, 9);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18225, '', 184, 1329, 2013, '2019-07-20 04:21:20', 'Mac OSX', 1, 20, 20, 20, 47, 20, 10);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32536, '', 0, 1684, 2016, '2019-06-03 16:00:44', 'Mac OSX', 1, 21, 21, 21, 48, 21, 11);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36653, '', 0, 274, 2016, '2018-09-14 13:24:40', 'Linux', 1, 22, 22, 22, 52, 22, 12);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15234, '', 0, 0, 2015, '2020-02-11 03:49:54', 'Windows', 1, 23, 23, 23, 53, 23, 13);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30676, '', 0, 2039, 2014, '2016-12-15 11:14:38', 'Linux', 1, 24, 24, 24, 54, 24, 14);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10334, '', 0, 0, 2016, '2017-06-04 09:27:25', 'Mac OSX', 1, 25, 25, 25, 56, 25, 15);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32081, '', 4, 0, 2014, '2017-06-15 08:49:58', 'Mac OSX', 1, 26, 26, 26, 57, 26, 16);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23142, 'AMD', 0, 0, 2016, '2018-04-10 23:08:29', 'Linux', 1, 27, 27, 27, 58, 27, 17);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23139, 'Intel', 0, 1974, 2013, '2021-03-14 08:21:05', 'Linux', 1, 28, 28, 28, 60, 28, 18);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17460, '', 134, 0, 2014, '2019-12-26 12:04:40', 'Linux', 1, 29, 29, 29, 61, 29, 19);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35560, '', 0, 1880, 2015, '2018-01-14 09:50:23', 'Mac OSX', 1, 30, 30, 30, 66, 30, 20);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15828, 'Intel', 205, 0, 2015, '2019-05-14 07:39:42', 'Mac OSX', 1, 31, 31, 31, 67, 31, 21);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17262, 'Intel', 149, 1195, 2014, '2018-09-21 15:04:54', 'Mac OSX', 1, 32, 32, 32, 69, 32, 22);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22903, '', 162, 0, 2012, '2018-01-14 02:59:30', 'Windows', 1, 33, 33, 33, 1, 33, 23);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25197, 'Intel', 0, 0, 2012, '2020-08-29 03:05:55', 'Mac OSX', 1, 34, 34, 34, 3, 34, 24);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18299, '', 0, 0, 2012, '2019-08-01 20:11:58', 'Linux', 1, 35, 35, 35, 4, 35, 25);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13602, '', 92, 407, 2013, '2020-03-24 19:54:01', 'Mac OSX', 1, 36, 36, 36, 6, 36, 26);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30729, '', 0, 0, 2014, '2017-04-18 00:46:29', 'Windows', 1, 37, 37, 37, 8, 37, 27);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18042, '', 244, 676, 2013, '2021-05-10 15:57:50', 'Mac OSX', 1, 38, 38, 38, 9, 38, 28);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38621, 'AMD', 0, 340, 2013, '2020-12-03 23:27:35', 'Windows', 1, 39, 39, 39, 10, 39, 29);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32172, '', 139, 369, 2014, '2019-05-21 19:13:57', 'Windows', 1, 40, 40, 40, 11, 40, 30);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27201, 'AMD', 0, 1897, 2015, '2018-06-04 09:38:26', 'Windows', 1, 41, 41, 41, 12, 41, 31);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10483, '', 143, 0, 2012, '2018-08-26 00:02:52', 'Mac OSX', 1, 42, 42, 42, 13, 42, 32);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11132, 'AMD', 205, 1770, 2016, '2017-01-23 20:04:48', 'Windows', 1, 43, 43, 43, 15, 43, 33);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26741, '', 122, 770, 2016, '2019-11-09 20:29:55', 'Linux', 1, 44, 44, 44, 17, 44, 34);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32960, 'ARM', 112, 0, 2012, '2020-02-06 02:32:39', 'Linux', 1, 45, 45, 45, 18, 45, 35);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35866, '', 60, 0, 2013, '2019-02-09 11:38:37', 'Linux', 1, 46, 46, 46, 19, 46, 36);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35591, 'Intel', 0, 0, 2013, '2020-01-12 20:21:18', 'Linux', 1, 47, 47, 47, 20, 47, 37);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38767, '', 0, 0, 2014, '2019-07-13 10:42:22', 'Mac OSX', 1, 48, 48, 48, 22, 48, 38);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19113, 'Intel', 231, 0, 2015, '2017-02-24 04:02:17', 'Windows', 1, 49, 49, 49, 25, 49, 39);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33646, 'AMD', 0, 936, 2014, '2019-07-10 07:28:49', 'Mac OSX', 1, 50, 50, 50, 27, 50, 40);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11756, '', 0, 0, 2015, '2021-02-14 16:40:33', 'Mac OSX', 1, 51, 51, 51, 37, 51, 41);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20094, 'AMD', 0, 1662, 2012, '2017-06-17 03:28:45', 'Mac OSX', 1, 52, 52, 52, 38, 52, 42);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33906, '', 0, 0, 2014, '2019-09-24 03:55:07', 'Windows', 1, 53, 53, 53, 45, 53, 43);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27068, '', 0, 0, 2012, '2020-09-07 01:35:15', 'Windows', 1, 54, 54, 54, 47, 54, 44);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11633, '', 207, 584, 2016, '2020-11-19 12:49:48', 'Linux', 1, 55, 55, 55, 48, 55, 45);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28183, 'ARM', 0, 0, 2014, '2020-11-09 10:56:30', 'Windows', 1, 56, 56, 56, 52, 56, 46);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13888, 'Intel', 0, 0, 2016, '2019-07-16 18:50:14', 'Mac OSX', 1, 57, 57, 57, 53, 57, 47);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38860, 'ARM', 0, 1210, 2012, '2017-06-04 01:47:07', 'Windows', 1, 58, 58, 58, 54, 58, 48);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26980, 'ARM', 48, 0, 2016, '2018-08-19 11:49:17', 'Linux', 1, 59, 59, 59, 56, 59, 49);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22674, 'AMD', 0, 1683, 2015, '2019-08-29 19:31:44', 'Linux', 1, 60, 60, 60, 57, 60, 50);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29355, 'AMD', 39, 0, 2015, '2021-07-13 19:55:18', 'Windows', 1, 61, 61, 61, 58, 61, 51);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27162, '', 236, 0, 2016, '2019-09-21 05:57:34', 'Mac OSX', 1, 62, 62, 62, 60, 62, 52);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32653, 'AMD', 103, 0, 2014, '2017-09-25 21:11:29', 'Linux', 1, 63, 63, 63, 61, 63, 53);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35236, 'AMD', 148, 429, 2014, '2020-06-21 19:52:37', 'Linux', 1, 64, 64, 64, 66, 64, 54);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15625, '', 0, 0, 2012, '2021-04-27 00:57:12', 'Windows', 1, 65, 65, 65, 67, 65, 55);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34224, '', 0, 262, 2013, '2019-09-19 19:46:32', 'Mac OSX', 1, 66, 66, 66, 69, 66, 56);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31304, 'AMD', 117, 847, 2015, '2019-07-22 04:38:00', 'Mac OSX', 1, 67, 67, 67, 1, 67, 57);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29075, '', 229, 1082, 2015, '2018-06-21 08:01:41', 'Mac OSX', 1, 68, 68, 68, 3, 68, 58);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15327, '', 163, 2040, 2014, '2018-01-09 22:05:23', 'Windows', 1, 69, 69, 69, 4, 69, 59);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27755, '', 0, 0, 2014, '2020-12-09 01:22:00', 'Linux', 1, 70, 70, 70, 6, 70, 60);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26158, 'ARM', 0, 2026, 2013, '2020-11-16 16:31:12', 'Windows', 1, 1, 1, 1, 8, 1, 61);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23854, '', 0, 1745, 2014, '2017-08-25 14:15:25', 'Windows', 1, 2, 2, 2, 9, 2, 62);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15617, 'ARM', 105, 517, 2016, '2021-03-09 22:46:56', 'Linux', 1, 3, 3, 3, 10, 3, 63);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23608, '', 0, 0, 2013, '2020-12-01 22:35:26', 'Windows', 1, 4, 4, 4, 11, 4, 64);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12633, 'ARM', 0, 671, 2015, '2020-05-01 05:47:57', 'Linux', 1, 5, 5, 5, 12, 5, 65);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24253, '', 0, 0, 2012, '2019-01-29 00:58:58', 'Mac OSX', 1, 6, 6, 6, 13, 6, 66);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17678, '', 75, 0, 2014, '2018-11-03 20:33:56', 'Linux', 1, 7, 7, 7, 15, 7, 67);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32162, 'AMD', 158, 580, 2014, '2019-09-29 20:31:11', 'Mac OSX', 1, 8, 8, 8, 17, 8, 68);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36766, '', 0, 1299, 2012, '2017-10-01 06:17:05', 'Windows', 1, 9, 9, 9, 18, 9, 69);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21620, 'Intel', 0, 0, 2015, '2020-01-06 17:00:34', 'Linux', 1, 10, 10, 10, 19, 10, 70);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29936, '', 142, 0, 2016, '2020-08-08 23:01:09', 'Mac OSX', 1, 11, 11, 11, 20, 11, 71);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23819, '', 241, 1274, 2015, '2020-05-02 04:24:31', 'Linux', 1, 12, 12, 12, 22, 12, 72);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31041, '', 0, 1236, 2013, '2020-05-07 02:48:33', 'Mac OSX', 1, 13, 13, 13, 25, 13, 73);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10702, 'ARM', 140, 1005, 2014, '2020-12-23 14:35:12', 'Windows', 1, 14, 14, 14, 27, 14, 74);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24029, '', 0, 0, 2015, '2019-09-06 22:13:30', 'Mac OSX', 1, 15, 15, 15, 37, 15, 75);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30474, 'AMD', 0, 0, 2012, '2020-11-05 21:33:58', 'Mac OSX', 1, 16, 16, 16, 38, 16, 76);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10527, '', 37, 0, 2015, '2019-08-16 10:53:22', 'Linux', 1, 17, 17, 17, 45, 17, 77);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35433, 'ARM', 245, 0, 2015, '2017-03-14 03:39:52', 'Windows', 1, 18, 18, 18, 47, 18, 78);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38230, 'Intel', 7, 0, 2015, '2019-09-16 07:27:00', 'Linux', 1, 19, 19, 19, 48, 19, 79);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21125, '', 123, 0, 2014, '2016-12-22 07:34:21', 'Linux', 1, 20, 20, 20, 52, 20, 80);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37627, '', 195, 1457, 2012, '2021-05-20 04:14:01', 'Windows', 1, 21, 21, 21, 53, 21, 1);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18397, '', 243, 0, 2014, '2021-10-04 05:52:32', 'Linux', 1, 22, 22, 22, 54, 22, 2);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30429, '', 0, 1610, 2013, '2021-11-19 17:14:48', 'Linux', 1, 23, 23, 23, 56, 23, 3);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13293, '', 9, 1802, 2016, '2021-04-03 07:16:36', 'Windows', 1, 24, 24, 24, 57, 24, 4);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34918, 'Intel', 200, 0, 2013, '2018-02-20 22:30:25', 'Linux', 1, 25, 25, 25, 58, 25, 5);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33870, 'AMD', 0, 762, 2014, '2019-06-17 08:24:25', 'Linux', 1, 26, 26, 26, 60, 26, 6);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25561, '', 0, 0, 2014, '2021-02-21 13:52:37', 'Mac OSX', 1, 27, 27, 27, 61, 27, 7);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16831, '', 238, 0, 2012, '2017-01-22 04:39:14', 'Linux', 1, 28, 28, 28, 66, 28, 8);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21135, 'AMD', 0, 0, 2015, '2018-03-30 05:47:33', 'Linux', 1, 29, 29, 29, 67, 29, 9);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38009, '', 20, 1661, 2014, '2018-01-25 23:57:30', 'Mac OSX', 1, 30, 30, 30, 69, 30, 10);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16069, '', 24, 0, 2013, '2019-09-14 07:37:33', 'Linux', 1, 31, 31, 31, 1, 31, 11);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20320, '', 0, 0, 2012, '2017-01-27 10:28:54', 'Linux', 1, 32, 32, 32, 3, 32, 12);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24119, '', 21, 49, 2014, '2021-05-02 04:24:38', 'Windows', 1, 33, 33, 33, 4, 33, 13);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34343, '', 0, 0, 2014, '2020-10-17 16:06:49', 'Linux', 1, 34, 34, 34, 6, 34, 14);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37608, '', 32, 0, 2016, '2019-07-28 20:28:01', 'Linux', 1, 35, 35, 35, 8, 35, 15);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14748, 'ARM', 149, 0, 2012, '2017-02-24 06:58:53', 'Linux', 1, 36, 36, 36, 9, 36, 16);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11003, 'Intel', 0, 1352, 2012, '2017-09-19 04:53:18', 'Linux', 1, 37, 37, 37, 10, 37, 17);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23509, '', 0, 0, 2012, '2017-04-01 08:04:18', 'Windows', 1, 38, 38, 38, 11, 38, 18);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24327, 'ARM', 0, 1103, 2012, '2018-04-30 02:11:16', 'Mac OSX', 1, 39, 39, 39, 12, 39, 19);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11685, 'Intel', 97, 0, 2014, '2019-09-07 02:20:49', 'Linux', 1, 40, 40, 40, 13, 40, 20);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25009, '', 27, 278, 2013, '2019-05-04 18:27:45', 'Linux', 1, 41, 41, 41, 15, 41, 21);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31702, 'AMD', 7, 0, 2012, '2017-08-25 15:57:31', 'Windows', 1, 42, 42, 42, 17, 42, 22);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38306, 'ARM', 196, 2027, 2012, '2019-02-27 13:44:51', 'Windows', 1, 43, 43, 43, 18, 43, 23);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20641, '', 71, 0, 2014, '2019-06-04 04:05:18', 'Mac OSX', 1, 44, 44, 44, 19, 44, 24);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17820, '', 0, 0, 2015, '2018-07-08 06:52:09', 'Windows', 1, 45, 45, 45, 20, 45, 25);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12189, 'ARM', 0, 0, 2016, '2018-11-07 21:19:10', 'Windows', 1, 46, 46, 46, 22, 46, 26);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28407, 'AMD', 95, 767, 2015, '2017-11-18 04:56:20', 'Linux', 1, 47, 47, 47, 25, 47, 27);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26535, 'ARM', 253, 1049, 2015, '2020-10-13 23:04:39', 'Windows', 1, 48, 48, 48, 27, 48, 28);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23183, '', 114, 1977, 2015, '2020-06-09 15:33:40', 'Mac OSX', 1, 49, 49, 49, 37, 49, 29);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20535, '', 155, 761, 2013, '2019-02-07 00:03:45', 'Mac OSX', 1, 50, 50, 50, 38, 50, 30);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27635, '', 0, 1897, 2015, '2020-11-17 01:50:55', 'Mac OSX', 1, 51, 51, 51, 45, 51, 31);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10660, '', 0, 898, 2015, '2018-06-10 08:16:04', 'Windows', 1, 52, 52, 52, 47, 52, 32);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32274, 'Intel', 44, 0, 2013, '2018-05-24 11:50:57', 'Linux', 1, 53, 53, 53, 48, 53, 33);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19470, 'Intel', 0, 876, 2014, '2018-11-01 17:37:50', 'Linux', 1, 54, 54, 54, 52, 54, 34);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29654, '', 0, 0, 2012, '2021-05-19 09:07:01', 'Linux', 1, 55, 55, 55, 53, 55, 35);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24061, '', 46, 731, 2015, '2020-01-16 20:14:27', 'Windows', 1, 56, 56, 56, 54, 56, 36);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30700, 'AMD', 129, 1448, 2013, '2017-01-12 02:32:43', 'Linux', 1, 57, 57, 57, 56, 57, 37);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21632, '', 0, 0, 2016, '2017-01-28 03:05:12', 'Windows', 1, 58, 58, 58, 57, 58, 38);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34026, '', 0, 0, 2015, '2018-02-04 19:38:17', 'Mac OSX', 1, 59, 59, 59, 58, 59, 39);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11860, 'ARM', 160, 828, 2016, '2019-11-12 15:26:23', 'Mac OSX', 1, 60, 60, 60, 60, 60, 40);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12548, '', 133, 0, 2012, '2016-12-21 03:03:02', 'Windows', 1, 61, 61, 61, 61, 61, 41);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13846, 'AMD', 0, 995, 2015, '2017-07-14 17:44:34', 'Mac OSX', 1, 62, 62, 62, 66, 62, 42);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23478, '', 232, 0, 2012, '2021-08-12 09:49:19', 'Linux', 1, 63, 63, 63, 67, 63, 43);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26265, 'AMD', 0, 694, 2012, '2019-08-11 23:02:40', 'Windows', 1, 64, 64, 64, 69, 64, 44);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28113, '', 51, 1923, 2013, '2017-09-12 13:25:54', 'Linux', 1, 65, 65, 65, 1, 65, 45);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26522, 'AMD', 9, 0, 2013, '2017-06-23 07:34:28', 'Mac OSX', 1, 66, 66, 66, 3, 66, 46);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32681, '', 38, 1644, 2014, '2021-07-29 06:22:06', 'Mac OSX', 1, 67, 67, 67, 4, 67, 47);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12708, '', 59, 0, 2012, '2019-02-15 00:11:10', 'Windows', 1, 68, 68, 68, 6, 68, 48);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20060, '', 0, 245, 2012, '2019-05-09 10:30:57', 'Linux', 1, 69, 69, 69, 8, 69, 49);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28090, 'AMD', 0, 640, 2016, '2017-01-19 20:59:32', 'Windows', 1, 70, 70, 70, 9, 70, 50);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31584, '', 189, 1921, 2013, '2018-02-21 21:34:53', 'Windows', 1, 1, 1, 1, 10, 1, 51);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29704, '', 0, 705, 2015, '2020-12-31 18:18:39', 'Linux', 1, 2, 2, 2, 11, 2, 52);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17564, 'ARM', 0, 0, 2013, '2020-03-23 08:26:51', 'Windows', 1, 3, 3, 3, 12, 3, 53);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22792, 'ARM', 0, 0, 2014, '2017-10-07 04:50:38', 'Linux', 1, 4, 4, 4, 13, 4, 54);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20450, 'AMD', 50, 0, 2015, '2021-03-18 22:13:50', 'Mac OSX', 1, 5, 5, 5, 15, 5, 55);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32899, '', 122, 0, 2013, '2020-06-25 23:34:20', 'Windows', 1, 6, 6, 6, 17, 6, 56);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25740, 'AMD', 0, 0, 2012, '2020-01-08 16:04:28', 'Mac OSX', 1, 7, 7, 7, 18, 7, 57);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21322, '', 0, 1795, 2012, '2018-06-22 16:09:46', 'Mac OSX', 1, 8, 8, 8, 19, 8, 58);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11433, '', 0, 346, 2014, '2021-01-31 07:11:32', 'Windows', 1, 9, 9, 9, 20, 9, 59);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17283, '', 149, 0, 2014, '2020-05-30 03:56:59', 'Mac OSX', 1, 10, 10, 10, 22, 10, 60);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28952, '', 224, 0, 2016, '2019-12-02 21:02:15', 'Mac OSX', 1, 11, 11, 11, 25, 11, 61);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36607, 'AMD', 8, 19, 2016, '2021-11-29 17:48:50', 'Windows', 1, 12, 12, 12, 27, 12, 62);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37526, '', 0, 1666, 2014, '2020-06-10 03:35:35', 'Linux', 1, 13, 13, 13, 37, 13, 63);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31941, 'Intel', 126, 0, 2013, '2018-06-03 03:43:52', 'Windows', 1, 14, 14, 14, 38, 14, 64);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31391, 'AMD', 0, 769, 2012, '2019-10-24 00:04:32', 'Windows', 1, 15, 15, 15, 45, 15, 65);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16807, '', 175, 0, 2012, '2020-04-08 11:21:11', 'Mac OSX', 1, 16, 16, 16, 47, 16, 66);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28481, 'Intel', 0, 0, 2012, '2016-12-21 15:10:43', 'Windows', 1, 17, 17, 17, 48, 17, 67);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19681, 'ARM', 204, 0, 2012, '2020-01-27 20:24:51', 'Linux', 1, 18, 18, 18, 52, 18, 68);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10012, '', 0, 203, 2013, '2017-08-06 14:56:07', 'Windows', 1, 19, 19, 19, 53, 19, 69);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12718, '', 0, 745, 2015, '2019-12-06 09:20:38', 'Mac OSX', 1, 20, 20, 20, 54, 20, 70);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14399, '', 0, 549, 2013, '2017-03-25 21:06:49', 'Mac OSX', 1, 21, 21, 21, 56, 21, 71);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33204, '', 107, 0, 2012, '2020-06-20 01:26:31', 'Mac OSX', 1, 22, 22, 22, 57, 22, 72);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10281, 'Intel', 242, 0, 2014, '2018-08-08 07:46:31', 'Windows', 1, 23, 23, 23, 58, 23, 73);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27952, '', 0, 1123, 2013, '2019-10-04 13:05:56', 'Mac OSX', 1, 24, 24, 24, 60, 24, 74);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26299, 'Intel', 59, 1574, 2016, '2018-11-07 23:29:23', 'Windows', 1, 25, 25, 25, 61, 25, 75);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12749, 'ARM', 0, 0, 2014, '2019-05-13 20:01:13', 'Mac OSX', 1, 26, 26, 26, 66, 26, 76);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26344, 'AMD', 0, 1263, 2016, '2019-03-11 08:35:55', 'Linux', 1, 27, 27, 27, 67, 27, 77);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11581, '', 0, 0, 2016, '2017-03-17 05:38:38', 'Windows', 1, 28, 28, 28, 69, 28, 78);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38174, '', 62, 0, 2015, '2019-10-12 06:16:14', 'Windows', 1, 29, 29, 29, 1, 29, 79);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19269, 'Intel', 0, 2034, 2012, '2018-08-26 09:58:07', 'Linux', 1, 30, 30, 30, 3, 30, 80);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21500, '', 0, 506, 2016, '2018-09-18 03:12:07', 'Mac OSX', 1, 31, 31, 31, 4, 31, 1);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29848, '', 189, 0, 2012, '2019-02-08 21:25:49', 'Windows', 1, 32, 32, 32, 6, 32, 2);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21738, 'Intel', 0, 0, 2013, '2019-11-04 07:54:26', 'Linux', 1, 33, 33, 33, 8, 33, 3);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14761, 'Intel', 0, 0, 2016, '2017-07-07 09:02:26', 'Mac OSX', 1, 34, 34, 34, 9, 34, 4);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26529, 'Intel', 162, 1986, 2013, '2017-10-24 00:03:28', 'Mac OSX', 1, 35, 35, 35, 10, 35, 5);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27676, 'AMD', 0, 0, 2014, '2020-08-26 11:41:18', 'Mac OSX', 1, 36, 36, 36, 11, 36, 6);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24217, 'Intel', 238, 1653, 2016, '2018-11-13 07:01:34', 'Linux', 1, 37, 37, 37, 12, 37, 7);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32865, '', 0, 0, 2014, '2020-02-20 09:58:53', 'Linux', 1, 38, 38, 38, 13, 38, 8);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32012, '', 199, 993, 2016, '2019-07-09 09:22:27', 'Linux', 1, 39, 39, 39, 15, 39, 9);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32026, 'Intel', 16, 1306, 2014, '2018-05-12 07:07:21', 'Linux', 1, 40, 40, 40, 17, 40, 10);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37852, '', 19, 1885, 2014, '2020-05-29 06:58:11', 'Mac OSX', 1, 41, 41, 41, 18, 41, 11);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28322, '', 0, 1991, 2015, '2019-03-21 04:11:17', 'Linux', 1, 42, 42, 42, 19, 42, 12);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26272, 'AMD', 0, 566, 2013, '2020-12-17 04:56:20', 'Mac OSX', 1, 43, 43, 43, 20, 43, 13);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18082, 'AMD', 0, 0, 2015, '2019-12-05 11:21:34', 'Windows', 1, 44, 44, 44, 22, 44, 14);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32031, 'AMD', 49, 0, 2013, '2018-02-28 02:34:35', 'Windows', 1, 45, 45, 45, 25, 45, 15);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36490, '', 0, 1426, 2013, '2021-07-27 12:24:16', 'Linux', 1, 46, 46, 46, 27, 46, 16);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15492, 'AMD', 47, 239, 2013, '2020-05-22 00:26:34', 'Linux', 1, 47, 47, 47, 37, 47, 17);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30709, 'AMD', 111, 1356, 2012, '2021-02-02 22:44:18', 'Windows', 1, 48, 48, 48, 38, 48, 18);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34274, 'AMD', 0, 605, 2015, '2019-11-12 23:03:03', 'Linux', 1, 49, 49, 49, 45, 49, 19);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31923, 'AMD', 90, 0, 2014, '2017-09-27 14:21:50', 'Mac OSX', 1, 50, 50, 50, 47, 50, 20);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31148, 'ARM', 175, 0, 2014, '2019-06-08 23:26:10', 'Windows', 1, 51, 51, 51, 48, 51, 21);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13312, 'AMD', 0, 0, 2012, '2020-04-20 21:21:05', 'Mac OSX', 1, 52, 52, 52, 52, 52, 22);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24593, 'Intel', 204, 0, 2014, '2017-10-13 18:20:29', 'Mac OSX', 1, 53, 53, 53, 53, 53, 23);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29470, 'Intel', 7, 2043, 2016, '2021-11-28 00:11:41', 'Mac OSX', 1, 54, 54, 54, 54, 54, 24);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19137, '', 133, 0, 2015, '2021-06-29 17:36:11', 'Windows', 1, 55, 55, 55, 56, 55, 25);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25148, 'ARM', 119, 1167, 2012, '2020-04-23 00:08:43', 'Windows', 1, 56, 56, 56, 57, 56, 26);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35712, '', 0, 0, 2013, '2018-12-11 17:20:46', 'Windows', 1, 57, 57, 57, 58, 57, 27);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31870, 'AMD', 98, 0, 2014, '2020-06-17 05:08:51', 'Windows', 1, 58, 58, 58, 60, 58, 28);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36153, 'Intel', 67, 1626, 2015, '2017-06-10 23:57:12', 'Mac OSX', 1, 59, 59, 59, 61, 59, 29);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36330, '', 44, 0, 2012, '2019-08-14 14:55:03', 'Windows', 1, 60, 60, 60, 66, 60, 30);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16222, 'ARM', 0, 945, 2013, '2021-01-25 18:17:54', 'Linux', 1, 61, 61, 61, 67, 61, 31);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13478, 'Intel', 0, 0, 2014, '2019-06-06 17:52:10', 'Mac OSX', 1, 62, 62, 62, 69, 62, 32);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18049, '', 170, 0, 2015, '2019-07-08 01:19:38', 'Mac OSX', 1, 63, 63, 63, 1, 63, 33);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35389, 'Intel', 176, 0, 2014, '2017-11-30 23:41:26', 'Linux', 1, 64, 64, 64, 3, 64, 34);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22151, 'Intel', 125, 573, 2016, '2021-05-11 04:08:24', 'Windows', 1, 65, 65, 65, 4, 65, 35);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35201, 'ARM', 0, 0, 2013, '2017-11-10 01:04:30', 'Linux', 1, 66, 66, 66, 6, 66, 36);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13521, '', 222, 0, 2016, '2021-05-09 12:25:04', 'Mac OSX', 1, 67, 67, 67, 8, 67, 37);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26096, '', 129, 0, 2015, '2019-08-24 01:21:42', 'Linux', 1, 68, 68, 68, 9, 68, 38);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27764, 'Intel', 0, 0, 2012, '2018-11-25 10:56:18', 'Windows', 1, 69, 69, 69, 10, 69, 39);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14877, 'AMD', 184, 0, 2012, '2018-07-07 08:58:32', 'Mac OSX', 1, 70, 70, 70, 11, 70, 40);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28629, '', 0, 1439, 2013, '2020-03-12 10:58:27', 'Linux', 1, 1, 1, 1, 12, 1, 41);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23254, 'AMD', 57, 1793, 2016, '2017-02-16 12:04:31', 'Windows', 1, 2, 2, 2, 13, 2, 42);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31817, 'ARM', 0, 0, 2014, '2021-09-19 06:23:46', 'Windows', 1, 3, 3, 3, 15, 3, 43);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29241, 'ARM', 0, 1436, 2015, '2018-03-16 01:07:31', 'Mac OSX', 1, 4, 4, 4, 17, 4, 44);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16379, 'AMD', 0, 0, 2014, '2017-07-06 12:20:48', 'Mac OSX', 1, 5, 5, 5, 18, 5, 45);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15259, 'Intel', 180, 173, 2013, '2016-12-25 05:45:19', 'Windows', 1, 6, 6, 6, 19, 6, 46);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30609, 'ARM', 218, 0, 2014, '2019-01-04 03:58:03', 'Mac OSX', 1, 7, 7, 7, 20, 7, 47);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10080, 'ARM', 0, 0, 2016, '2019-02-12 02:52:46', 'Mac OSX', 1, 8, 8, 8, 22, 8, 48);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17835, 'ARM', 0, 1067, 2016, '2021-04-18 10:43:08', 'Linux', 1, 9, 9, 9, 25, 9, 49);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38429, '', 0, 0, 2013, '2019-03-24 05:07:32', 'Linux', 1, 10, 10, 10, 27, 10, 50);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19531, '', 0, 0, 2012, '2019-10-09 18:36:43', 'Windows', 1, 11, 11, 11, 37, 11, 51);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28528, 'ARM', 18, 893, 2016, '2018-12-29 07:37:10', 'Mac OSX', 1, 12, 12, 12, 38, 12, 52);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19032, 'Intel', 137, 2045, 2014, '2019-04-22 09:58:11', 'Linux', 1, 13, 13, 13, 45, 13, 53);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24801, '', 0, 0, 2013, '2019-12-07 20:59:31', 'Mac OSX', 1, 14, 14, 14, 47, 14, 54);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16988, '', 0, 1611, 2014, '2017-09-23 21:18:12', 'Linux', 1, 15, 15, 15, 48, 15, 55);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38068, '', 0, 0, 2015, '2020-12-15 23:54:15', 'Windows', 1, 16, 16, 16, 52, 16, 56);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14058, 'Intel', 0, 281, 2013, '2020-08-05 17:39:01', 'Linux', 1, 17, 17, 17, 53, 17, 57);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18617, '', 167, 103, 2012, '2021-02-20 05:10:10', 'Mac OSX', 1, 18, 18, 18, 54, 18, 58);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23114, 'ARM', 206, 0, 2014, '2021-01-19 08:13:23', 'Linux', 1, 19, 19, 19, 56, 19, 59);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31980, 'AMD', 187, 703, 2013, '2017-09-11 06:01:52', 'Linux', 1, 20, 20, 20, 57, 20, 60);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16869, '', 0, 1863, 2013, '2019-01-01 22:41:59', 'Linux', 1, 21, 21, 21, 58, 21, 61);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35170, '', 0, 44, 2013, '2017-07-03 09:43:05', 'Linux', 1, 22, 22, 22, 60, 22, 62);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20218, 'Intel', 8, 993, 2014, '2019-12-04 21:58:11', 'Windows', 1, 23, 23, 23, 61, 23, 63);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37096, '', 47, 0, 2013, '2018-04-16 09:49:34', 'Windows', 1, 24, 24, 24, 66, 24, 64);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13027, 'AMD', 0, 0, 2012, '2018-10-17 15:55:55', 'Linux', 1, 25, 25, 25, 67, 25, 65);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11264, '', 175, 0, 2015, '2018-12-12 03:50:54', 'Mac OSX', 1, 26, 26, 26, 69, 26, 66);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12114, 'AMD', 0, 0, 2015, '2019-11-24 04:08:04', 'Windows', 1, 27, 27, 27, 1, 27, 67);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13213, '', 185, 0, 2012, '2020-11-17 08:47:23', 'Windows', 1, 28, 28, 28, 3, 28, 68);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23585, '', 34, 1563, 2016, '2017-01-15 15:48:54', 'Mac OSX', 1, 29, 29, 29, 4, 29, 69);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16400, 'AMD', 0, 0, 2016, '2018-06-04 04:06:34', 'Windows', 1, 30, 30, 30, 6, 30, 70);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29025, 'AMD', 235, 0, 2014, '2021-08-09 01:52:48', 'Linux', 1, 31, 31, 31, 8, 31, 71);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33452, 'ARM', 0, 0, 2014, '2017-11-25 12:32:09', 'Mac OSX', 1, 32, 32, 32, 9, 32, 72);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32239, '', 0, 0, 2012, '2018-02-21 22:45:44', 'Mac OSX', 1, 33, 33, 33, 10, 33, 73);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16834, 'AMD', 0, 644, 2013, '2019-09-18 13:41:39', 'Windows', 1, 34, 34, 34, 11, 34, 74);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10120, '', 0, 0, 2014, '2017-01-18 10:56:22', 'Mac OSX', 1, 35, 35, 35, 12, 35, 75);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10570, 'Intel', 0, 0, 2014, '2021-02-15 08:31:08', 'Windows', 1, 36, 36, 36, 13, 36, 76);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19973, 'Intel', 0, 0, 2016, '2017-07-10 16:54:39', 'Mac OSX', 1, 37, 37, 37, 15, 37, 77);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25355, '', 0, 0, 2015, '2017-12-26 04:06:27', 'Linux', 1, 38, 38, 38, 17, 38, 78);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27783, '', 14, 0, 2016, '2021-12-09 13:56:34', 'Linux', 1, 39, 39, 39, 18, 39, 79);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15561, '', 0, 0, 2015, '2019-04-22 22:43:21', 'Linux', 1, 40, 40, 40, 19, 40, 80);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36366, '', 0, 388, 2016, '2017-04-29 02:43:52', 'Mac OSX', 1, 41, 41, 41, 20, 41, 1);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30924, 'Intel', 0, 2019, 2016, '2018-10-06 13:25:53', 'Mac OSX', 1, 42, 42, 42, 22, 42, 2);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32030, 'Intel', 0, 0, 2012, '2018-10-16 12:19:17', 'Linux', 1, 43, 43, 43, 25, 43, 3);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28520, 'Intel', 97, 1313, 2015, '2017-01-27 10:21:11', 'Linux', 1, 44, 44, 44, 27, 44, 4);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29626, '', 0, 2039, 2013, '2019-10-08 08:18:15', 'Windows', 1, 45, 45, 45, 37, 45, 5);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18459, '', 0, 978, 2016, '2021-05-23 13:18:35', 'Windows', 1, 46, 46, 46, 38, 46, 6);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17420, 'AMD', 0, 1605, 2012, '2018-07-24 04:22:05', 'Windows', 1, 47, 47, 47, 45, 47, 7);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32430, 'AMD', 0, 388, 2013, '2016-12-17 15:04:14', 'Linux', 1, 48, 48, 48, 47, 48, 8);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16595, '', 0, 1865, 2015, '2017-02-26 03:36:17', 'Linux', 1, 49, 49, 49, 48, 49, 9);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16768, 'Intel', 0, 0, 2016, '2019-12-28 10:03:49', 'Linux', 1, 50, 50, 50, 52, 50, 10);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25048, 'ARM', 80, 0, 2013, '2019-05-03 19:44:43', 'Mac OSX', 1, 51, 51, 51, 53, 51, 11);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24833, '', 146, 0, 2016, '2017-03-21 02:05:08', 'Linux', 1, 52, 52, 52, 54, 52, 12);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36460, 'Intel', 158, 0, 2016, '2020-07-15 17:19:37', 'Windows', 1, 53, 53, 53, 56, 53, 13);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32443, 'ARM', 0, 0, 2015, '2017-01-05 02:21:55', 'Linux', 1, 54, 54, 54, 57, 54, 14);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33634, '', 0, 80, 2015, '2018-07-29 06:43:21', 'Mac OSX', 1, 55, 55, 55, 58, 55, 15);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16935, '', 0, 1279, 2016, '2018-06-04 20:09:45', 'Mac OSX', 1, 56, 56, 56, 60, 56, 16);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15312, 'AMD', 75, 1565, 2012, '2018-11-26 08:37:22', 'Mac OSX', 1, 57, 57, 57, 61, 57, 17);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13343, '', 135, 1557, 2016, '2019-04-11 06:35:41', 'Mac OSX', 1, 58, 58, 58, 66, 58, 18);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14057, '', 0, 83, 2013, '2020-08-30 08:26:52', 'Windows', 1, 59, 59, 59, 67, 59, 19);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14611, '', 132, 0, 2015, '2018-09-06 04:57:43', 'Linux', 1, 60, 60, 60, 69, 60, 20);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13975, '', 0, 951, 2015, '2020-01-30 23:03:36', 'Mac OSX', 1, 61, 61, 61, 1, 61, 21);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35134, 'AMD', 0, 1180, 2013, '2021-02-15 06:01:37', 'Mac OSX', 1, 62, 62, 62, 3, 62, 22);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26346, 'ARM', 0, 0, 2013, '2018-05-11 20:13:15', 'Mac OSX', 1, 63, 63, 63, 4, 63, 23);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25016, 'AMD', 170, 0, 2013, '2017-12-20 04:47:23', 'Windows', 1, 64, 64, 64, 6, 64, 24);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35156, '', 126, 1773, 2013, '2017-10-28 12:15:21', 'Windows', 1, 65, 65, 65, 8, 65, 25);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30505, 'AMD', 0, 0, 2014, '2019-08-01 02:50:26', 'Mac OSX', 1, 66, 66, 66, 9, 66, 26);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22431, '', 25, 0, 2013, '2020-03-08 21:22:38', 'Mac OSX', 1, 67, 67, 67, 10, 67, 27);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25725, '', 134, 0, 2012, '2019-03-06 21:33:40', 'Mac OSX', 1, 68, 68, 68, 11, 68, 28);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30555, 'AMD', 0, 0, 2012, '2017-08-10 07:07:58', 'Linux', 1, 69, 69, 69, 12, 69, 29);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19437, 'Intel', 0, 0, 2016, '2021-02-21 22:59:33', 'Linux', 1, 70, 70, 70, 13, 70, 30);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29498, '', 79, 0, 2012, '2021-03-24 16:12:52', 'Windows', 1, 1, 1, 1, 15, 1, 31);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18900, '', 0, 0, 2012, '2019-05-24 07:30:52', 'Windows', 1, 2, 2, 2, 17, 2, 32);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29337, '', 0, 1187, 2014, '2018-03-11 00:58:03', 'Mac OSX', 1, 3, 3, 3, 18, 3, 33);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24775, '', 134, 0, 2012, '2021-10-30 07:57:08', 'Windows', 1, 4, 4, 4, 19, 4, 34);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34563, 'ARM', 81, 0, 2014, '2019-03-23 19:55:02', 'Mac OSX', 1, 5, 5, 5, 20, 5, 35);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33203, '', 5, 329, 2011, '2019-06-08 08:41:28', 'Mac OSX', 1, 6, 6, 6, 22, 6, 36);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37231, '', 194, 0, 2012, '2017-07-22 15:59:28', 'Mac OSX', 1, 7, 7, 7, 25, 7, 37);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22848, '', 184, 0, 2013, '2018-06-19 18:12:18', 'Linux', 1, 8, 8, 8, 27, 8, 38);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12529, '', 223, 966, 2014, '2021-06-06 05:15:44', 'Mac OSX', 1, 9, 9, 9, 37, 9, 39);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19629, '', 0, 74, 2014, '2020-09-18 18:57:04', 'Windows', 1, 10, 10, 10, 38, 10, 40);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33565, 'ARM', 87, 601, 2016, '2020-03-24 00:02:33', 'Mac OSX', 1, 11, 11, 11, 45, 11, 41);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22356, 'AMD', 0, 0, 2015, '2019-02-16 14:30:13', 'Mac OSX', 1, 12, 12, 12, 47, 12, 42);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23672, 'Intel', 0, 0, 2013, '2018-06-18 04:37:39', 'Mac OSX', 1, 13, 13, 13, 48, 13, 43);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15998, '', 0, 0, 2015, '2021-04-07 16:44:26', 'Linux', 1, 14, 14, 14, 52, 14, 44);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11645, '', 0, 1767, 2016, '2020-07-24 19:40:53', 'Windows', 1, 15, 15, 15, 53, 15, 45);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30230, 'ARM', 135, 0, 2013, '2017-01-15 21:55:30', 'Windows', 1, 16, 16, 16, 54, 16, 46);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20250, '', 0, 0, 2015, '2020-10-08 01:44:39', 'Linux', 1, 17, 17, 17, 56, 17, 47);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29576, 'AMD', 69, 0, 2013, '2017-02-05 01:52:15', 'Windows', 1, 18, 18, 18, 57, 18, 48);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26508, 'Intel', 0, 0, 2013, '2017-09-25 10:30:24', 'Linux', 1, 19, 19, 19, 58, 19, 49);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24732, '', 141, 1258, 2012, '2018-10-02 14:53:07', 'Mac OSX', 1, 20, 20, 20, 60, 20, 50);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34552, '', 86, 1188, 2013, '2016-12-29 05:59:02', 'Windows', 1, 21, 21, 21, 61, 21, 51);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34420, 'Intel', 0, 0, 2015, '2020-08-27 12:21:24', 'Mac OSX', 1, 22, 22, 22, 66, 22, 52);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34184, 'ARM', 0, 0, 2014, '2018-10-30 10:08:41', 'Linux', 1, 23, 23, 23, 67, 23, 53);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35858, 'AMD', 0, 1600, 2013, '2020-04-20 05:11:52', 'Mac OSX', 1, 24, 24, 24, 69, 24, 54);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31315, '', 0, 0, 2016, '2021-01-26 15:03:36', 'Linux', 1, 25, 25, 25, 1, 25, 55);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15760, 'ARM', 0, 1433, 2016, '2021-07-26 23:37:08', 'Linux', 1, 26, 26, 26, 3, 26, 56);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15864, 'Intel', 231, 0, 2016, '2020-11-09 01:36:34', 'Mac OSX', 1, 27, 27, 27, 4, 27, 57);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32442, '', 0, 1443, 2013, '2019-02-21 05:13:13', 'Linux', 1, 28, 28, 28, 6, 28, 58);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14417, 'ARM', 0, 0, 2013, '2017-03-21 02:48:17', 'Windows', 1, 29, 29, 29, 8, 29, 59);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19063, '', 128, 0, 2012, '2021-11-25 16:08:13', 'Mac OSX', 1, 30, 30, 30, 9, 30, 60);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23339, 'ARM', 238, 1241, 2013, '2020-02-14 09:23:37', 'Windows', 1, 31, 31, 31, 10, 31, 61);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22809, '', 81, 0, 2014, '2020-10-26 20:35:43', 'Windows', 1, 32, 32, 32, 11, 32, 62);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34885, 'AMD', 77, 0, 2015, '2017-07-04 08:09:25', 'Linux', 1, 33, 33, 33, 12, 33, 63);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37646, '', 0, 1978, 2015, '2018-12-15 18:24:34', 'Windows', 1, 34, 34, 34, 13, 34, 64);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35954, '', 0, 1861, 2012, '2017-09-16 00:44:35', 'Mac OSX', 1, 35, 35, 35, 15, 35, 65);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22936, '', 13, 0, 2015, '2021-03-16 05:18:05', 'Windows', 1, 36, 36, 36, 17, 36, 66);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25892, 'ARM', 0, 0, 2012, '2019-12-04 20:07:32', 'Mac OSX', 1, 37, 37, 37, 18, 37, 67);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34747, '', 0, 0, 2015, '2021-04-18 03:29:08', 'Mac OSX', 1, 38, 38, 38, 19, 38, 68);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35902, 'Intel', 252, 972, 2015, '2019-03-29 18:29:39', 'Windows', 1, 39, 39, 39, 20, 39, 69);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37097, '', 0, 1529, 2012, '2017-04-23 22:59:29', 'Mac OSX', 1, 40, 40, 40, 22, 40, 70);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32919, 'ARM', 12, 1890, 2014, '2017-08-06 16:34:03', 'Mac OSX', 1, 41, 41, 41, 25, 41, 71);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14004, '', 0, 0, 2015, '2019-10-07 11:20:33', 'Mac OSX', 1, 42, 42, 42, 27, 42, 72);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10161, '', 0, 827, 2013, '2017-08-21 15:59:25', 'Mac OSX', 1, 43, 43, 43, 37, 43, 73);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22203, 'Intel', 0, 183, 2016, '2021-03-24 10:29:35', 'Windows', 1, 44, 44, 44, 38, 44, 74);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20868, '', 91, 114, 2014, '2020-05-03 10:35:18', 'Mac OSX', 1, 45, 45, 45, 45, 45, 75);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16341, '', 0, 0, 2012, '2017-10-12 12:12:05', 'Linux', 1, 46, 46, 46, 47, 46, 76);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11887, '', 85, 90, 2015, '2021-04-30 10:52:34', 'Mac OSX', 1, 47, 47, 47, 48, 47, 77);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30645, '', 17, 0, 2015, '2021-02-10 22:05:56', 'Windows', 1, 48, 48, 48, 52, 48, 78);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33769, '', 97, 0, 2013, '2019-07-04 02:04:38', 'Linux', 1, 49, 49, 49, 53, 49, 79);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30355, '', 0, 1777, 2015, '2021-11-02 02:01:42', 'Linux', 1, 50, 50, 50, 54, 50, 80);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34062, '', 94, 227, 2012, '2020-06-04 16:35:40', 'Windows', 1, 51, 51, 51, 56, 51, 1);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16384, 'AMD', 113, 999, 2016, '2021-03-21 22:33:52', 'Linux', 1, 52, 52, 52, 57, 52, 2);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36612, '', 0, 1845, 2014, '2019-10-12 17:55:16', 'Linux', 1, 53, 53, 53, 58, 53, 3);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26505, '', 0, 298, 2016, '2018-09-08 19:39:40', 'Linux', 1, 54, 54, 54, 60, 54, 4);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25040, '', 0, 0, 2013, '2017-09-14 19:16:59', 'Linux', 1, 55, 55, 55, 61, 55, 5);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25323, '', 0, 815, 2016, '2017-12-16 17:12:56', 'Linux', 1, 56, 56, 56, 66, 56, 6);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34921, '', 0, 915, 2012, '2019-07-14 23:40:04', 'Linux', 1, 57, 57, 57, 67, 57, 7);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32597, 'ARM', 0, 1620, 2012, '2019-10-17 23:08:57', 'Windows', 1, 58, 58, 58, 69, 58, 8);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23234, '', 117, 404, 2012, '2018-06-21 05:04:10', 'Mac OSX', 1, 59, 59, 59, 1, 59, 9);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12023, '', 224, 0, 2016, '2017-07-01 21:07:03', 'Linux', 1, 60, 60, 60, 3, 60, 10);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31845, '', 198, 1574, 2015, '2019-12-13 19:23:21', 'Windows', 1, 61, 61, 61, 4, 61, 11);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25864, 'Intel', 120, 1309, 2014, '2018-12-13 20:59:01', 'Mac OSX', 1, 62, 62, 62, 6, 62, 12);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33014, 'ARM', 0, 0, 2014, '2017-12-17 22:57:49', 'Windows', 1, 63, 63, 63, 8, 63, 13);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11566, '', 0, 1839, 2013, '2020-05-07 21:09:02', 'Mac OSX', 1, 64, 64, 64, 9, 64, 14);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (16105, 'ARM', 166, 0, 2012, '2017-04-29 05:57:22', 'Linux', 1, 65, 65, 65, 10, 65, 15);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17132, 'Intel', 235, 0, 2013, '2017-11-21 21:48:54', 'Linux', 1, 66, 66, 66, 11, 66, 16);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21575, 'AMD', 0, 1134, 2015, '2021-08-10 11:02:10', 'Windows', 1, 67, 67, 67, 12, 67, 17);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21893, '', 0, 0, 2014, '2019-11-06 15:08:57', 'Mac OSX', 1, 68, 68, 68, 13, 68, 18);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12682, 'ARM', 0, 1841, 2016, '2017-08-05 01:03:14', 'Linux', 1, 69, 69, 69, 15, 69, 19);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18729, '', 0, 709, 2015, '2017-04-14 11:34:00', 'Linux', 1, 70, 70, 70, 17, 70, 20);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11538, 'Intel', 0, 165, 2012, '2020-10-08 21:41:55', 'Windows', 1, 1, 1, 1, 18, 1, 21);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36085, '', 38, 0, 2012, '2017-01-28 22:01:11', 'Linux', 1, 2, 2, 2, 19, 2, 22);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32336, '', 0, 0, 2013, '2021-12-05 03:29:55', 'Linux', 1, 3, 3, 3, 20, 3, 23);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19563, '', 16, 1454, 2014, '2019-07-20 01:41:56', 'Mac OSX', 1, 4, 4, 4, 22, 4, 24);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26122, 'AMD', 48, 0, 2012, '2020-02-17 11:30:20', 'Linux', 1, 5, 5, 5, 25, 5, 25);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23394, '', 251, 634, 2011, '2021-07-25 04:15:53', 'Linux', 1, 6, 6, 6, 27, 6, 26);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (23095, '', 0, 0, 2012, '2017-10-15 19:10:34', 'Mac OSX', 1, 7, 7, 7, 37, 7, 27);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24160, '', 0, 793, 2012, '2019-07-19 00:13:26', 'Windows', 1, 8, 8, 8, 38, 8, 28);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35158, 'AMD', 0, 1556, 2016, '2019-06-13 23:19:35', 'Linux', 1, 9, 9, 9, 45, 9, 29);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30015, '', 99, 0, 2016, '2018-09-28 15:09:31', 'Mac OSX', 1, 10, 10, 10, 47, 10, 30);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18330, '', 0, 0, 2013, '2017-05-20 19:23:28', 'Mac OSX', 1, 11, 11, 11, 48, 11, 31);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35993, 'AMD', 252, 1167, 2012, '2020-04-28 22:04:14', 'Linux', 1, 12, 12, 12, 52, 12, 32);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36050, '', 142, 1993, 2013, '2017-01-12 14:52:53', 'Mac OSX', 1, 13, 13, 13, 53, 13, 33);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20115, '', 0, 0, 2013, '2017-03-07 23:26:18', 'Windows', 1, 14, 14, 14, 54, 14, 34);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28634, '', 0, 1360, 2013, '2019-07-27 07:28:05', 'Linux', 1, 15, 15, 15, 56, 15, 35);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (17471, 'Intel', 0, 697, 2015, '2021-07-23 19:58:17', 'Linux', 1, 16, 16, 16, 57, 16, 36);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38651, '', 247, 0, 2015, '2017-09-19 05:51:30', 'Mac OSX', 1, 17, 17, 17, 58, 17, 37);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32446, 'AMD', 0, 0, 2014, '2018-08-08 03:18:53', 'Linux', 1, 18, 18, 18, 60, 18, 38);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35583, 'AMD', 185, 1382, 2013, '2019-01-02 04:42:46', 'Windows', 1, 19, 19, 19, 61, 19, 39);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13115, '', 0, 0, 2015, '2020-05-01 12:36:24', 'Windows', 1, 20, 20, 20, 66, 20, 40);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15873, '', 0, 502, 2012, '2018-02-03 04:42:47', 'Mac OSX', 1, 21, 21, 21, 67, 21, 41);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20368, 'AMD', 0, 1175, 2015, '2020-02-29 22:43:18', 'Mac OSX', 1, 22, 22, 22, 69, 22, 42);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25609, 'AMD', 118, 1713, 2015, '2018-04-10 10:02:54', 'Mac OSX', 1, 23, 23, 23, 1, 23, 43);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30884, '', 0, 0, 2014, '2019-11-02 05:25:03', 'Mac OSX', 1, 24, 24, 24, 3, 24, 44);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29162, '', 29, 0, 2013, '2019-02-10 14:51:32', 'Mac OSX', 1, 25, 25, 25, 4, 25, 45);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37559, '', 0, 0, 2013, '2019-06-29 18:35:44', 'Mac OSX', 1, 26, 26, 26, 6, 26, 46);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35974, 'Intel', 43, 0, 2015, '2019-01-30 01:30:30', 'Mac OSX', 1, 27, 27, 27, 8, 27, 47);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (11316, 'Intel', 0, 0, 2012, '2017-06-07 14:44:36', 'Windows', 1, 28, 28, 28, 9, 28, 48);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19364, 'Intel', 81, 1602, 2014, '2021-08-18 11:59:43', 'Linux', 1, 29, 29, 29, 10, 29, 49);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (28372, '', 0, 0, 2016, '2020-04-14 18:04:57', 'Linux', 1, 30, 30, 30, 11, 30, 50);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35274, 'ARM', 174, 0, 2015, '2020-07-28 01:45:48', 'Windows', 1, 31, 31, 31, 12, 31, 51);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33070, 'AMD', 30, 348, 2015, '2019-05-28 19:42:25', 'Linux', 1, 32, 32, 32, 13, 32, 52);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15012, '', 0, 0, 2012, '2021-09-16 05:47:48', 'Windows', 1, 33, 33, 33, 15, 33, 53);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25839, '', 0, 1371, 2015, '2019-05-01 13:15:59', 'Linux', 1, 34, 34, 34, 17, 34, 54);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12500, 'ARM', 0, 992, 2014, '2019-08-21 13:17:35', 'Windows', 1, 35, 35, 35, 18, 35, 55);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30608, 'ARM', 199, 0, 2013, '2020-01-15 04:31:13', 'Windows', 1, 36, 36, 36, 19, 36, 56);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13310, '', 0, 0, 2014, '2017-01-01 06:44:30', 'Windows', 1, 37, 37, 37, 20, 37, 57);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35226, '', 0, 0, 2013, '2018-11-17 13:31:59', 'Mac OSX', 1, 38, 38, 38, 22, 38, 58);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (33023, '', 0, 0, 2012, '2020-05-22 16:14:47', 'Windows', 1, 39, 39, 39, 25, 39, 59);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10001, 'Intel', 0, 635, 2015, '2019-06-18 06:37:04', 'Mac OSX', 1, 40, 40, 40, 27, 40, 60);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19369, 'ARM', 79, 911, 2013, '2017-09-05 22:23:36', 'Windows', 1, 41, 41, 41, 37, 41, 61);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (36997, '', 0, 0, 2015, '2020-03-18 02:40:02', 'Linux', 1, 42, 42, 42, 38, 42, 62);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34979, 'Intel', 165, 0, 2015, '2020-06-01 11:35:18', 'Windows', 1, 43, 43, 43, 45, 43, 63);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14642, '', 0, 0, 2014, '2017-04-10 18:31:27', 'Linux', 1, 44, 44, 44, 47, 44, 64);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (35932, 'ARM', 52, 144, 2016, '2018-08-17 03:25:10', 'Windows', 1, 45, 45, 45, 48, 45, 65);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (26629, '', 0, 407, 2015, '2020-02-08 17:19:01', 'Mac OSX', 1, 46, 46, 46, 52, 46, 66);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25175, 'Intel', 0, 1659, 2012, '2017-06-23 07:02:47', 'Mac OSX', 1, 47, 47, 47, 53, 47, 67);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25684, '', 0, 1085, 2014, '2018-10-03 21:46:10', 'Windows', 1, 48, 48, 48, 54, 48, 68);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (15300, '', 0, 1358, 2013, '2017-10-01 19:19:49', 'Windows', 1, 49, 49, 49, 56, 49, 69);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34960, '', 128, 0, 2015, '2019-05-28 14:56:15', 'Linux', 1, 50, 50, 50, 57, 50, 70);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (38794, 'Intel', 5, 1005, 2013, '2021-04-02 07:44:37', 'Linux', 1, 51, 51, 51, 58, 51, 71);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18991, '', 168, 0, 2015, '2021-05-05 06:35:25', 'Windows', 1, 52, 52, 52, 60, 52, 72);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24572, 'Intel', 232, 0, 2015, '2017-12-02 10:07:55', 'Linux', 1, 53, 53, 53, 61, 53, 73);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (37770, 'AMD', 0, 0, 2013, '2017-06-09 18:21:53', 'Windows', 1, 54, 54, 54, 66, 54, 74);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (19838, '', 0, 0, 2015, '2021-08-02 01:14:26', 'Windows', 1, 55, 55, 55, 67, 55, 75);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (30777, '', 0, 1321, 2016, '2020-12-01 08:36:04', 'Mac OSX', 1, 56, 56, 56, 69, 56, 76);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (13446, 'Intel', 129, 591, 2014, '2020-12-06 06:57:00', 'Linux', 1, 57, 57, 57, 1, 57, 77);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25984, '', 77, 0, 2016, '2021-02-21 13:57:16', 'Linux', 1, 58, 58, 58, 3, 58, 78);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (10414, 'AMD', 215, 0, 2014, '2018-06-27 09:27:11', 'Mac OSX', 1, 59, 59, 59, 4, 59, 79);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (12919, '', 213, 0, 2015, '2016-12-18 03:36:52', 'Linux', 1, 60, 60, 60, 6, 60, 80);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (32265, '', 0, 1132, 2011, '2017-02-25 04:49:08', 'Linux', 1, 61, 61, 61, 8, 61, 1);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (22135, 'Intel', 0, 0, 2013, '2019-06-16 19:38:11', 'Mac OSX', 1, 62, 62, 62, 9, 62, 2);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34732, '', 0, 159, 2014, '2016-12-28 10:14:06', 'Windows', 1, 63, 63, 63, 10, 63, 3);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (27572, '', 156, 1632, 2015, '2021-07-01 10:18:56', 'Linux', 1, 64, 64, 64, 11, 64, 4);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20029, '', 0, 1656, 2013, '2018-04-22 14:20:47', 'Linux', 1, 65, 65, 65, 12, 65, 5);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34179, '', 0, 1810, 2016, '2020-10-27 15:08:10', 'Windows', 1, 66, 66, 66, 13, 66, 6);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (14175, '', 154, 63, 2013, '2017-08-01 12:22:05', 'Mac OSX', 1, 67, 67, 67, 15, 67, 7);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34405, 'ARM', 25, 847, 2012, '2018-12-01 16:18:37', 'Linux', 1, 68, 68, 68, 17, 68, 8);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25623, '', 0, 0, 2013, '2020-04-03 10:09:32', 'Windows', 1, 69, 69, 69, 18, 69, 9);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (25440, '', 169, 0, 2013, '2021-01-30 05:28:00', 'Windows', 1, 70, 70, 70, 19, 70, 10);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24486, 'AMD', 109, 0, 2015, '2019-08-15 19:10:18', 'Mac OSX', 1, 1, 1, 1, 20, 1, 11);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (18495, '', 0, 0, 2013, '2017-10-14 14:00:37', 'Windows', 1, 2, 2, 2, 22, 2, 12);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (21309, 'ARM', 0, 0, 2015, '2019-05-23 01:14:04', 'Linux', 1, 3, 3, 3, 25, 3, 13);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (31515, '', 40, 0, 2013, '2018-07-12 09:46:58', 'Windows', 1, 4, 4, 4, 27, 4, 14);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24462, 'Intel', 0, 0, 2015, '2018-02-01 00:38:57', 'Windows', 1, 5, 5, 5, 37, 5, 15);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (29871, '', 0, 0, 2015, '2018-11-18 00:52:02', 'Mac OSX', 1, 6, 6, 6, 38, 6, 16);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (20202, '', 185, 0, 2016, '2020-03-16 16:10:10', 'Windows', 1, 7, 7, 7, 45, 7, 17);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (24560, 'AMD', 71, 0, 2015, '2020-08-31 13:17:28', 'Mac OSX', 1, 8, 8, 8, 47, 8, 18);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34311, '', 237, 0, 2016, '2020-03-23 09:56:52', 'Windows', 1, 9, 9, 9, 48, 9, 19);
INSERT INTO device (serial_number, custom_cpu, custom_ram, custom_hdsize, year_manufac, date_purchased, os, is_active, nwmodel_id, monitormodel_id, cameramodel_id, projectormodel_id, printermodel_id, computermodel_id) VALUES (34731, 'Intel', 0, 0, 2013, '2020-07-01 13:19:51', 'Windows', 1, 10, 10, 10, 52, 10, 20);

INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (1, 'Hahn-Mayert', 'http://mcdermott.biz/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (2, 'Marquardt and Sons', 'http://www.considine.org/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (3, 'Lind-Reilly', 'http://bogisich.biz/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (4, 'Fadel, Bogisich and Casper', 'http://swaniawskistanton.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (5, 'Sawayn-Fahey', 'http://www.renner.net/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (6, 'Witting-Treutel', 'http://www.bins.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (7, 'Muller, Barrows and Maggio', 'http://runolfsson.info/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (8, 'Lowe, Little and Lakin', 'http://langworthhudson.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (9, 'Hahn-Greenfelder', 'http://tremblaywalker.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (10, 'Runolfsson LLC', 'http://shanahanrussel.org/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (11, 'Bernhard Inc', 'http://www.lubowitz.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (12, 'Gutmann-Stark', 'http://www.watershodkiewicz.org/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (13, 'Schultz Group', 'http://zboncakcarter.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (14, 'Hudson, Skiles and Swift', 'http://mueller.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (15, 'Zboncak-DuBuque', 'http://bins.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (16, 'Greenfelder-Braun', 'http://www.littlecasper.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (17, 'Koss LLC', 'http://thielwaelchi.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (18, 'Mohr-Welch', 'http://www.watersdach.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (19, 'Johnson, Schinner and Jast', 'http://www.hilll.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (20, 'Mertz-Wisoky', 'http://stiedemann.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (21, 'Sawayn PLC', 'http://www.schowalter.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (22, 'Hickle-Leffler', 'http://spinka.info/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (23, 'Bogisich LLC', 'http://www.ryanankunding.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (24, 'Spencer-Gleason', 'http://www.leschleuschke.com/', 1);
INSERT INTO manufacturer (manuf_id, name, support_site, is_active) VALUES (25, 'Metz PLC', 'http://www.gusikowski.com/', 1);

INSERT INTO member_of (person_id, dept_id, is_active) VALUES (1, 1, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (2, 2, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (3, 3, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (4, 4, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (5, 5, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (6, 6, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (7, 7, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (8, 8, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (9, 9, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (10, 10, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (11, 11, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (12, 12, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (13, 13, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (14, 14, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (15, 15, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (16, 1, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (17, 2, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (18, 3, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (19, 4, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (20, 5, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (21, 6, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (22, 7, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (23, 8, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (24, 9, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (25, 10, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (26, 11, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (27, 12, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (28, 13, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (29, 14, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (30, 15, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (31, 1, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (32, 2, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (33, 3, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (34, 4, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (35, 5, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (36, 6, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (37, 7, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (38, 8, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (39, 9, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (40, 10, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (41, 11, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (42, 12, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (43, 13, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (44, 14, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (45, 15, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (46, 1, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (47, 2, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (48, 3, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (49, 4, 1);
INSERT INTO member_of (person_id, dept_id, is_active) VALUES (50, 5, 1);

INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (1, 3, 'TN', 2786, 19, 243, 1, 5);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (2, 0, 'IPS', 2679, 28, 204, 1, 25);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (3, 3, 'VA', 2353, 37, 309, 1, 14);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (4, 4, 'Retina', 1171, 36, 83, 1, 13);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (5, 3, 'VA', 2888, 39, 296, 1, 4);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (6, 0, 'TN', 1513, 26, 278, 1, 20);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (7, 1, 'Retina', 2577, 28, 330, 1, 22);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (8, 1, 'Retina', 2808, 36, 215, 1, 9);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (9, 2, 'VA', 4012, 41, 188, 1, 23);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (10, 3, 'VA', 3732, 20, 109, 1, 7);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (11, 3, 'Retina', 3085, 32, 343, 1, 11);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (12, 2, 'Retina', 1373, 36, 304, 1, 8);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (13, 3, 'VA', 3885, 36, 201, 1, 9);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (14, 4, 'IPS', 1952, 32, 356, 1, 19);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (15, 4, 'IPS', 2557, 36, 99, 1, 16);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (16, 3, 'IPS', 2328, 26, 101, 1, 25);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (17, 2, 'IPS', 3630, 23, 171, 1, 17);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (18, 4, 'VA', 3454, 17, 102, 1, 3);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (19, 2, 'IPS', 4193, 26, 328, 1, 11);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (20, 0, 'IPS', 3539, 22, 171, 1, 17);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (21, 1, 'IPS', 2302, 34, 285, 1, 6);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (22, 4, 'Retina', 2813, 21, 64, 1, 18);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (23, 4, 'VA', 2923, 23, 162, 1, 2);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (24, 2, 'Retina', 2924, 35, 146, 1, 18);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (25, 2, 'TN', 1897, 43, 113, 1, 1);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (26, 1, 'TN', 1649, 23, 191, 1, 14);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (27, 0, 'IPS', 2439, 36, 168, 1, 8);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (28, 2, 'VA', 1442, 29, 243, 1, 10);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (29, 1, 'TN', 1416, 27, 249, 1, 13);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (30, 0, 'IPS', 2876, 41, 72, 1, 20);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (31, 2, 'Retina', 3741, 21, 93, 1, 5);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (32, 1, 'IPS', 1412, 39, 72, 1, 5);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (33, 1, 'IPS', 4276, 37, 167, 1, 19);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (34, 4, 'Retina', 2909, 32, 148, 1, 16);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (35, 2, 'TN', 2576, 21, 348, 1, 10);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (36, 4, 'Retina', 2531, 41, 298, 1, 15);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (37, 2, 'VA', 2796, 17, 255, 1, 2);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (38, 0, 'IPS', 1396, 35, 263, 1, 20);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (39, 3, 'VA', 2042, 39, 352, 1, 13);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (40, 4, 'Retina', 2657, 18, 125, 1, 1);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (41, 3, 'TN', 1194, 39, 119, 1, 3);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (42, 2, 'VA', 2721, 31, 154, 1, 7);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (43, 0, 'VA', 1388, 36, 186, 1, 21);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (44, 1, 'TN', 2589, 19, 315, 1, 10);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (45, 1, 'Retina', 2977, 19, 225, 1, 9);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (46, 2, 'IPS', 1856, 36, 129, 1, 8);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (47, 4, 'IPS', 2884, 32, 201, 1, 15);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (48, 1, 'IPS', 2455, 37, 286, 1, 15);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (49, 0, 'VA', 2017, 32, 159, 1, 11);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (50, 2, 'VA', 4182, 33, 108, 1, 24);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (51, 2, 'Retina', 2229, 20, 129, 1, 3);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (52, 3, 'IPS', 1905, 27, 305, 1, 18);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (53, 3, 'Retina', 2137, 23, 132, 1, 12);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (54, 0, 'IPS', 2795, 19, 204, 1, 24);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (55, 3, 'TN', 3355, 18, 177, 1, 6);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (56, 2, 'VA', 4101, 31, 189, 1, 1);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (57, 2, 'IPS', 2627, 41, 153, 1, 2);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (58, 1, 'Retina', 4092, 41, 218, 1, 12);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (59, 1, 'Retina', 2874, 24, 146, 1, 4);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (60, 1, 'Retina', 1322, 41, 153, 1, 21);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (61, 4, 'Retina', 2688, 31, 144, 1, 23);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (62, 0, 'TN', 3478, 29, 241, 1, 17);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (63, 1, 'VA', 3834, 31, 287, 1, 4);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (64, 0, 'Retina', 4285, 41, 127, 1, 14);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (65, 0, 'TN', 1540, 19, 226, 1, 12);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (66, 3, 'VA', 1839, 43, 220, 1, 7);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (67, 2, 'TN', 1133, 35, 126, 1, 19);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (68, 4, 'VA', 2291, 17, 169, 1, 16);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (69, 0, 'TN', 2261, 21, 86, 1, 22);
INSERT INTO monitor_model (model_id, hdmi, panel_type, resolution, size, refresh_rate, is_active, manuf_id) VALUES (70, 3, 'TN', 3702, 33, 305, 1, 6);

INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (1, '62i74zq67et9', 'RIP', 'Gateway', 153, 2, 128, 408, 1, 14);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (2, '30q05lp76tt0', 'RIP', 'Gateway', 893, 66, 87, 515, 1, 13);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (3, '78f47iq15ax9', 'IEEE 802', 'Modem', 1481, 100, 106, 249, 1, 18);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (4, '87x37wi16ir7', 'LWAPP', 'Router', 4592, 6, 180, 425, 1, 13);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (5, '42b29ri98vj4', 'RGIP', 'Router', 1302, 68, 134, 353, 1, 23);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (6, '80p65zf00gm9', 'RIP', 'Modem', 8704, 67, 95, 422, 1, 13);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (7, '27n52rp22ca5', 'IEEE 802', 'Modem', 7900, 28, 21, 731, 1, 14);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (8, '74s39dz16ys1', 'LWAPP', 'Switch', 8659, 3, 77, 288, 1, 3);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (9, '93k43qy56jn4', 'RIP', 'Switch', 8402, 18, 118, 1009, 1, 3);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (10, '56x25lw66cy1', 'IEEE 802', 'Gateway', 3791, 87, 151, 292, 1, 12);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (11, '41f82zt24wn5', 'RIP', 'Switch', 3525, 92, 49, 640, 1, 10);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (12, '58t66gq98xk9', 'IEEE 802', 'Switch', 273, 57, 26, 176, 1, 7);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (13, '50d47jr75li8', 'IEEE 802', 'Router', 5722, 78, 27, 1068, 1, 15);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (14, '18j81kh05cu4', 'IEEE 802', 'Switch', 166, 75, 165, 399, 1, 8);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (15, '15r92kv17je4', 'LWAPP', 'Router', 9351, 14, 158, 662, 1, 11);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (16, '54t82xe97nh4', 'LWAPP', 'Switch', 6160, 65, 198, 382, 1, 22);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (17, '36x79ve29at7', 'LWAPP', 'Router', 7945, 66, 60, 733, 1, 5);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (18, '10g04sa18fl3', 'IEEE 802', 'Switch', 1309, 60, 32, 977, 1, 23);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (19, '89s58gm39pk4', 'IEEE 802', 'Router', 9215, 20, 49, 963, 1, 9);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (20, '49p03rz35eu3', 'RGIP', 'Switch', 4391, 87, 194, 241, 1, 22);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (21, '29r42sm72hd7', 'RGIP', 'Gateway', 9895, 30, 149, 815, 1, 19);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (22, '14c73mi97kk9', 'LWAPP', 'Switch', 9734, 38, 22, 140, 1, 20);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (23, '75z89re13ff1', 'LWAPP', 'Switch', 9606, 65, 49, 57, 1, 16);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (24, '65l08cx54xv8', 'RGIP', 'Switch', 4384, 73, 96, 89, 1, 2);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (25, '07g58ax69eq3', 'LWAPP', 'Router', 1375, 76, 61, 389, 1, 9);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (26, '23t65hg14lq4', 'RIP', 'Modem', 6198, 56, 106, 786, 1, 18);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (27, '54f59gb84bs0', 'RGIP', 'Gateway', 1330, 53, 163, 181, 1, 1);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (28, '41x83ye87hy2', 'RGIP', 'Router', 14, 41, 161, 64, 1, 8);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (29, '31s80ed61fk6', 'LWAPP', 'Modem', 6625, 97, 92, 733, 1, 10);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (30, '93i27gu97we7', 'RIP', 'Switch', 6520, 82, 169, 1107, 1, 5);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (31, '10g38gs94pq2', 'RIP', 'Switch', 2520, 72, 60, 1010, 1, 25);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (32, '97t09sb22yx1', 'RIP', 'Gateway', 8756, 87, 87, 182, 1, 3);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (33, '83s56xs36rf1', 'RIP', 'Modem', 454, 7, 108, 308, 1, 7);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (34, '01x00uq22xn8', 'IEEE 802', 'Modem', 7475, 98, 54, 89, 1, 1);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (35, '98s94hp46kc3', 'IEEE 802', 'Gateway', 5349, 89, 133, 1179, 1, 6);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (36, '76r20lo29cr6', 'LWAPP', 'Gateway', 6989, 96, 65, 1194, 1, 16);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (37, '40l97oc29pa2', 'RGIP', 'Router', 8749, 56, 88, 715, 1, 25);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (38, '29m59nt10gy4', 'RIP', 'Gateway', 6998, 85, 147, 268, 1, 5);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (39, '46b53rn46ix8', 'RIP', 'Router', 5987, 60, 133, 452, 1, 20);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (40, '13m87as32tk3', 'RGIP', 'Router', 2645, 10, 165, 742, 1, 15);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (41, '66j24rb03qc3', 'RGIP', 'Router', 4602, 53, 36, 989, 1, 6);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (42, '85p90gb99lz1', 'RGIP', 'Switch', 2755, 65, 154, 259, 1, 8);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (43, '16b93wr80dh4', 'RIP', 'Gateway', 8435, 32, 95, 939, 1, 2);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (44, '71t57kn30au8', 'LWAPP', 'Modem', 290, 83, 63, 299, 1, 2);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (45, '31l64zy95wv0', 'LWAPP', 'Router', 1279, 55, 78, 23, 1, 4);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (46, '73r53up24ac8', 'IEEE 802', 'Switch', 8534, 36, 88, 285, 1, 16);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (47, '37h93mb93iw3', 'IEEE 802', 'Modem', 5118, 57, 131, 988, 1, 17);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (48, '75s00lf31qx8', 'RIP', 'Router', 4563, 70, 26, 75, 1, 21);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (49, '29d72al21mv4', 'RGIP', 'Gateway', 2074, 99, 172, 723, 1, 12);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (50, '29r64sw62lv2', 'RGIP', 'Router', 640, 31, 187, 821, 1, 9);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (51, '07i19no71ng4', 'RIP', 'Modem', 1332, 97, 29, 1120, 1, 11);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (52, '22x70uw12yn5', 'RGIP', 'Router', 6254, 4, 158, 298, 1, 4);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (53, '30i27me02yv5', 'RIP', 'Switch', 1469, 78, 144, 537, 1, 20);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (54, '12l53ex23ar4', 'LWAPP', 'Modem', 357, 60, 98, 624, 1, 19);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (55, '10z85as69wv9', 'RIP', 'Switch', 9791, 44, 31, 481, 1, 17);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (56, '37i08cr23fc9', 'RGIP', 'Gateway', 8080, 8, 144, 535, 1, 14);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (57, '45k91do87pj9', 'LWAPP', 'Gateway', 1510, 67, 25, 906, 1, 24);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (58, '39q03vy11lz4', 'RGIP', 'Modem', 4797, 6, 66, 573, 1, 24);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (59, '51j61mh27gv6', 'RGIP', 'Switch', 5129, 100, 65, 27, 1, 11);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (60, '29u40om96iv2', 'IEEE 802', 'Gateway', 5277, 92, 45, 505, 1, 10);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (61, '65k58bd29ko2', 'RIP', 'Gateway', 6345, 77, 28, 118, 1, 17);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (62, '59i73nw94hm2', 'LWAPP', 'Modem', 9462, 11, 181, 463, 1, 12);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (63, '66p68tc44rq8', 'RGIP', 'Switch', 4151, 69, 39, 698, 1, 4);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (64, '48c07wi53vi2', 'RGIP', 'Router', 2349, 92, 195, 714, 1, 18);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (65, '88q29pz94uh1', 'RGIP', 'Modem', 394, 68, 148, 822, 1, 7);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (66, '93g54ch03bx1', 'RIP', 'Router', 5981, 1, 97, 395, 1, 19);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (67, '65a45lu20tx6', 'RGIP', 'Switch', 5520, 29, 76, 948, 1, 1);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (68, '06y91rq65zt9', 'RGIP', 'Switch', 1941, 27, 105, 788, 1, 21);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (69, '99y49ue98fa6', 'RIP', 'Gateway', 4008, 34, 36, 304, 1, 6);
INSERT INTO nwdevice_models (model_id, mac_address, protocol, type, speed, number_ports, coverage, number_clients, is_active, manuf_id) VALUES (70, '77o40qx39gl7', 'RIP', 'Gateway', 1181, 24, 37, 1175, 1, 15);

INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10001, 1, 1, 1, 1, 1);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10012, 1, 2, 2, 2, 2);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10034, 1, 3, 3, 3, 3);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10057, 1, 4, 4, 4, 4);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10080, 1, 5, 5, 5, 5);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10120, 1, 6, 6, 6, 6);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10161, 1, 7, 7, 7, 7);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10281, 1, 8, 8, 8, 8);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10334, 1, 9, 9, 9, 9);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10414, 1, 10, 10, 10, 10);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10483, 1, 11, 11, 11, 11);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10527, 1, 12, 12, 12, 12);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10570, 1, 13, 13, 13, 13);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10660, 1, 14, 14, 14, 14);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (10702, 1, 15, 15, 15, 15);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11003, 1, 16, 1, 16, 16);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11044, 1, 17, 2, 17, 17);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11132, 1, 18, 3, 18, 18);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11264, 1, 19, 4, 19, 19);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11299, 1, 20, 5, 20, 20);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11316, 1, 21, 6, 21, 21);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11389, 1, 22, 7, 22, 22);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11433, 1, 23, 8, 23, 23);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11538, 1, 24, 9, 24, 24);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11566, 1, 25, 10, 25, 25);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11581, 1, 26, 11, 26, 26);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11608, 1, 27, 12, 27, 27);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11633, 1, 28, 13, 28, 28);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11645, 1, 29, 14, 29, 29);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11685, 1, 30, 15, 30, 30);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11756, 1, 31, 1, 31, 31);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11850, 1, 32, 2, 32, 32);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11860, 1, 33, 3, 33, 33);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (11887, 1, 34, 4, 34, 34);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12023, 1, 35, 5, 35, 35);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12055, 1, 36, 6, 1, 1);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12114, 1, 37, 7, 2, 2);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12189, 1, 38, 8, 3, 3);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12500, 1, 39, 9, 4, 4);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12529, 1, 40, 10, 5, 5);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12548, 1, 41, 11, 6, 6);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12633, 1, 42, 12, 7, 7);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12682, 1, 43, 13, 8, 8);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12708, 1, 44, 14, 9, 9);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12718, 1, 45, 15, 10, 10);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12749, 1, 46, 1, 11, 11);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (12919, 1, 47, 2, 12, 12);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (13027, 1, 48, 3, 13, 13);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (13115, 1, 49, 4, 14, 14);
INSERT INTO owner (serial_number, is_active, person_id, dept_id, room_id, rack_id) VALUES (13213, 1, 50, 5, 15, 15);

INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (1, 'Nia', 'Keebler', '0', 'charles.reynolds@example.com', 'TA', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (2, 'Destiny', 'Kub', '941', 'bernardo.kunde@example.net', 'student', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (3, 'Bernadette', 'Considine', '198', 'ccarter@example.org', 'TA', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (4, 'Pamela', 'Schuster', '1', 'ilene.hudson@example.net', 'student', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (5, 'Chanel', 'Champlin', '1', 'kbailey@example.com', 'TA', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (6, 'Angelita', 'Rosenbaum', '1', 'schultz.cicero@example.com', 'student', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (7, 'Gertrude', 'Conroy', '1', 'glennie.jerde@example.org', 'student', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (8, 'Mitchel', 'Kovacek', '0', 'swest@example.com', 'admin', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (9, 'Constance', 'Little', '0', 'skylar.kuvalis@example.net', 'admin', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (10, 'Gavin', 'Hansen', '73', 'kris.carmela@example.com', 'admin', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (11, 'Shany', 'Heller', '0', 'agleichner@example.org', 'TA', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (12, 'Watson', 'Shanahan', '1', 'grimes.esperanza@example.net', 'professor', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (13, 'Hildegard', 'Hilpert', '239605', 'waters.denis@example.net', 'admin', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (14, 'Daisha', 'Marquardt', '249', 'ylegros@example.org', 'professor', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (15, 'Alexane', 'Streich', '616817', 'dare.royce@example.net', 'professor', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (16, 'Eddie', 'Kutch', '1', 'edmund07@example.com', 'professor', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (17, 'Marguerite', 'Ebert', '0', 'ewatsica@example.com', 'professor', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (18, 'Zoila', 'Herman', '614', 'ilehner@example.com', 'student', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (19, 'Stephania', 'Bradtke', '0', 'edd.okeefe@example.com', 'admin', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (20, 'Alexandria', 'Bogan', '1', 'moises64@example.net', 'admin', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (21, 'Hobart', 'Wintheiser', '1676200132', 'jordyn09@example.org', 'professor', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (22, 'Pierre', 'Larson', '665405', 'igislason@example.org', 'student', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (23, 'Lavon', 'Quigley', '893033', 'gbuckridge@example.net', 'student', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (24, 'Orlando', 'Schultz', '1', 'bturcotte@example.net', 'student', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (25, 'Anahi', 'Cormier', '501101', 'schuster.florine@example.org', 'professor', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (26, 'Raleigh', 'Mayer', '1', 'porter79@example.org', 'student', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (27, 'Jennings', 'Dickinson', '128109', 'kara99@example.com', 'professor', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (28, 'Augustus', 'Boehm', '155501', 'kharvey@example.com', 'professor', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (29, 'Kailey', 'Emmerich', '353899', 'jacobson.katelynn@example.org', 'admin', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (30, 'Ignacio', 'Waelchi', '0', 'melvin.marquardt@example.net', 'TA', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (31, 'Rhianna', 'Collins', '50', 'ora.parisian@example.com', 'admin', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (32, 'Janessa', 'Hackett', '1', 'wilson.lind@example.com', 'admin', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (33, 'Sean', 'Kertzmann', '5908230180', 'johathan.walter@example.com', 'TA', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (34, 'Sonya', 'Homenick', '0', 'barbara75@example.net', 'student', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (35, 'Vena', 'Russel', '684', 'tremayne26@example.com', 'admin', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (36, 'Ezekiel', 'Schmeler', '642374', 'donavon84@example.net', 'student', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (37, 'Andreanne', 'Russel', '524', 'hschoen@example.net', 'TA', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (38, 'Krystina', 'Steuber', '1', 'darrion48@example.net', 'TA', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (39, 'Rafael', 'Jaskolski', '1', 'yswaniawski@example.net', 'TA', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (40, 'Deanna', 'Marvin', '243793', 'jacobs.samantha@example.net', 'student', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (41, 'Rickie', 'Casper', '512', 'deckow.ezra@example.net', 'admin', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (42, 'Camren', 'Nolan', '451', 'koepp.noel@example.com', 'TA', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (43, 'Vivienne', 'Russel', '0', 'nikita66@example.org', 'TA', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (44, 'Cristopher', 'Trantow', '603', 'adrienne.wisoky@example.com', 'TA', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (45, 'Kacie', 'OKeefe', '1', 'stoltenberg.kaden@example.net', 'admin', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (46, 'Magnolia', 'Cormier', '0', 'kemmerich@example.org', 'admin', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (47, 'Meaghan', 'King', '672', 'marcelo.jacobson@example.net', 'TA', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (48, 'Kaya', 'Kiehn', '307342', 'ward.lauretta@example.org', 'professor', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (49, 'Thea', 'Borer', '0', 'alyson34@example.net', 'professor', 1);
INSERT INTO person (person_id, fname, lname, phone, email, role, is_active) VALUES (50, 'Willie', 'Bernier', '1', 'chadd.kuhn@example.com', 'TA', 1);

INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (1, 1, 'A6', 6, 24, 1, '"bw"', 1, 21);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (2, 0, 'A6', 3, 23, 1, '"bw"', 1, 12);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (3, 1, 'A3', 5, 8, 1, '"color"', 1, 8);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (4, 0, 'A4', 2, 13, 0, '"color"', 1, 16);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (5, 1, 'A3', 4, 6, 1, '"bw"', 1, 9);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (6, 1, 'A6', 7, 20, 0, '"color"', 1, 8);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (7, 0, 'A1', 4, 4, 1, '"color"', 1, 6);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (8, 1, 'A6', 3, 14, 1, '"color"', 1, 17);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (9, 1, 'A3', 6, 5, 0, '"bw"', 1, 18);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (10, 1, 'A6', 8, 22, 0, '"color"', 1, 1);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (11, 1, 'A6', 3, 21, 0, '"color"', 1, 19);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (12, 1, 'A3', 5, 24, 1, '"color"', 1, 11);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (13, 1, 'A3', 3, 23, 0, '"bw"', 1, 6);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (14, 0, 'A4', 6, 4, 0, '"bw"', 1, 14);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (15, 1, 'A1', 7, 20, 1, '"color"', 1, 23);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (16, 1, 'A4', 6, 22, 1, '"bw"', 1, 17);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (17, 1, 'A2', 3, 24, 1, '"bw"', 1, 5);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (18, 0, 'A2', 7, 23, 1, '"bw"', 1, 2);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (19, 1, 'A1', 2, 16, 1, '"bw"', 1, 8);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (20, 1, 'A6', 4, 22, 1, '"color"', 1, 12);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (21, 1, 'A2', 7, 9, 1, '"color"', 1, 15);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (22, 1, 'A5', 3, 22, 1, '"color"', 1, 18);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (23, 0, 'A4', 8, 25, 0, '"bw"', 1, 2);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (24, 1, 'A5', 3, 18, 1, '"color"', 1, 5);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (25, 1, 'A5', 6, 6, 0, '"color"', 1, 20);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (26, 1, 'A4', 4, 9, 1, '"color"', 1, 13);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (27, 1, 'A2', 3, 18, 1, '"color"', 1, 24);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (28, 0, 'A3', 4, 15, 0, '"color"', 1, 5);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (29, 1, 'A6', 6, 15, 1, '"color"', 1, 2);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (30, 1, 'A2', 8, 16, 0, '"bw"', 1, 9);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (31, 1, 'A6', 8, 6, 0, '"bw"', 1, 19);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (32, 0, 'A3', 8, 20, 0, '"bw"', 1, 16);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (33, 1, 'A3', 7, 24, 1, '"color"', 1, 12);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (34, 1, 'A5', 3, 13, 0, '"color"', 1, 13);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (35, 1, 'A6', 8, 4, 0, '"color"', 1, 11);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (36, 1, 'A5', 2, 17, 0, '"color"', 1, 10);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (37, 1, 'A5', 7, 18, 1, '"bw"', 1, 20);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (38, 1, 'A5', 5, 3, 1, '"bw"', 1, 18);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (39, 1, 'A2', 7, 9, 0, '"bw"', 1, 4);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (40, 1, 'A4', 8, 10, 1, '"bw"', 1, 7);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (41, 0, 'A2', 4, 11, 1, '"color"', 1, 14);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (42, 1, 'A3', 2, 5, 1, '"bw"', 1, 23);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (43, 0, 'A2', 6, 22, 1, '"bw"', 1, 3);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (44, 1, 'A2', 4, 3, 0, '"color"', 1, 7);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (45, 1, 'A2', 4, 22, 0, '"color"', 1, 6);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (46, 1, 'A5', 6, 14, 0, '"color"', 1, 14);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (47, 1, 'A4', 2, 8, 1, '"color"', 1, 17);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (48, 1, 'A5', 2, 19, 1, '"color"', 1, 24);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (49, 0, 'A1', 8, 16, 1, '"bw"', 1, 22);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (50, 0, 'A4', 8, 5, 0, '"color"', 1, 22);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (51, 1, 'A2', 3, 15, 0, '"color"', 1, 15);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (52, 1, 'A3', 5, 4, 1, '"color"', 1, 4);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (53, 0, 'A3', 6, 21, 1, '"color"', 1, 9);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (54, 0, 'A4', 7, 13, 1, '"color"', 1, 1);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (55, 1, 'A1', 2, 18, 1, '"bw"', 1, 1);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (56, 1, 'A3', 7, 15, 1, '"color"', 1, 3);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (57, 1, 'A2', 8, 20, 1, '"color"', 1, 3);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (58, 1, 'A1', 4, 14, 1, '"color"', 1, 15);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (59, 0, 'A4', 7, 13, 0, '"color"', 1, 25);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (60, 0, 'A6', 3, 4, 1, '"color"', 1, 25);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (61, 0, 'A4', 4, 7, 1, '"bw"', 1, 21);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (62, 1, 'A4', 8, 7, 1, '"bw"', 1, 10);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (63, 1, 'A6', 5, 8, 1, '"color"', 1, 11);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (64, 0, 'A6', 8, 21, 0, '"color"', 1, 10);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (65, 0, 'A6', 4, 7, 1, '"color"', 1, 19);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (66, 0, 'A2', 4, 21, 0, '"bw"', 1, 4);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (67, 1, 'A2', 4, 16, 1, '"bw"', 1, 16);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (68, 0, 'A2', 6, 18, 0, '"color"', 1, 7);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (69, 0, 'A6', 4, 17, 1, '"color"', 1, 20);
INSERT INTO printer_model (model_id, double_sided, paper_size, memory, speed, laser, bw_color, is_active, manuf_id) VALUES (70, 1, 'A4', 2, 22, 0, '"color"', 1, 13);

INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (1, 2, 0, 1551, 1, 1621, 12, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (3, 2, 1, 2448, 0, 2028, 7, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (4, 1, 1, 1774, 0, 1671, 13, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (6, 1, 1, 3093, 0, 1762, 24, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (8, 1, 0, 3076, 0, 1414, 10, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (9, 1, 0, 1546, 0, 2065, 15, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (10, 1, 0, 3517, 0, 1140, 13, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (11, 1, 0, 1563, 1, 1655, 3, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (12, 4, 0, 3288, 1, 1690, 2, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (13, 4, 0, 2224, 0, 1412, 14, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (15, 1, 0, 3192, 0, 1627, 19, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (17, 2, 1, 2309, 1, 1609, 12, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (18, 1, 1, 2854, 1, 1669, 18, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (19, 1, 1, 1603, 0, 1470, 19, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (20, 1, 1, 1748, 0, 1130, 11, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (22, 4, 1, 1856, 0, 1359, 5, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (25, 4, 0, 3477, 0, 1322, 25, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (27, 4, 1, 2778, 1, 1329, 11, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (37, 2, 1, 3244, 0, 1623, 16, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (38, 3, 0, 1776, 1, 1820, 18, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (45, 2, 1, 1722, 0, 1527, 11, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (47, 2, 1, 1735, 1, 1530, 8, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (48, 1, 0, 3203, 0, 1885, 10, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (52, 3, 0, 2824, 1, 2028, 20, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (53, 3, 1, 1830, 0, 1342, 25, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (54, 3, 1, 3388, 1, 2049, 10, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (56, 3, 0, 3409, 1, 1298, 20, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (57, 2, 0, 3309, 0, 1724, 4, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (58, 1, 1, 3577, 0, 1487, 16, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (60, 3, 1, 1734, 1, 1975, 7, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (61, 1, 0, 2797, 1, 1741, 12, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (66, 4, 1, 2379, 0, 1242, 21, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (67, 3, 0, 1918, 1, 1241, 16, 1);
INSERT INTO projector_model (model_id, hdmi, wifi, lumens, speakers, resolution, manuf_id, is_active) VALUES (69, 3, 0, 3458, 0, 1127, 9, 1);

INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (4, 1, 1, 32);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (5, 2, 1, 35);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (0, 3, 1, 26);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (18, 4, 1, 30);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (7, 5, 1, 29);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (18, 6, 1, 22);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (17, 7, 1, 27);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (3, 8, 1, 7);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (9, 9, 1, 17);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (11, 10, 1, 28);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (11, 11, 1, 24);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (4, 12, 1, 12);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (10, 13, 1, 25);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (11, 14, 1, 18);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (17, 15, 1, 21);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (16, 16, 1, 16);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (18, 17, 1, 34);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (18, 18, 1, 31);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (5, 19, 1, 15);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (4, 20, 1, 5);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (2, 21, 1, 13);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (11, 22, 1, 4);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (14, 23, 1, 11);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (10, 24, 1, 9);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (3, 25, 1, 8);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (0, 26, 1, 14);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (3, 27, 1, 33);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (3, 28, 1, 6);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (13, 29, 1, 10);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (13, 30, 1, 20);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (14, 31, 1, 1);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (19, 32, 1, 3);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (13, 33, 1, 2);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (20, 34, 1, 19);
INSERT INTO rack (slot, rack_id, is_active, room_id) VALUES (2, 35, 1, 23);

INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (1, 'Saepe officiis cumque ex debitis excepturi.', 1, 4);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (2, 'Est et quidem delectus vero doloremque dolorum.', 1, 3);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (3, 'Distinctio iure ut doloremque voluptate est.', 1, 10);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (4, 'Corrupti ratione totam molestiae expedita.', 1, 12);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (5, 'Debitis expedita neque quod voluptatem earum omnis', 1, 9);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (6, 'Consequuntur vero ratione occaecati ab et.', 1, 8);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (7, 'Non dolorem aliquam sed omnis dolor quidem.', 1, 1);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (8, 'Et magnam ducimus maiores.', 1, 15);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (9, 'Quaerat cumque tenetur dicta aperiam.', 1, 5);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (10, 'Quia dolores nostrum ea veniam.', 1, 25);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (11, 'Molestias voluptas similique quidem blanditiis dol', 1, 19);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (12, 'Ratione consequatur inventore pariatur placeat.', 1, 13);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (13, 'Sint quia tempore quis maxime explicabo.', 1, 18);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (14, 'Quasi enim consequatur laudantium vitae iste ratio', 1, 23);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (15, 'Eligendi vel modi nulla ex eos incidunt earum.', 1, 6);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (16, 'Dignissimos neque occaecati vero et cumque non.', 1, 20);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (17, 'Suscipit voluptas commodi facere blanditiis culpa ', 1, 5);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (18, 'Minus eum corporis deserunt nihil.', 1, 8);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (19, 'Voluptas dolorem est numquam at adipisci rerum.', 1, 22);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (20, 'Maiores numquam optio sit aut soluta beatae aut.', 1, 7);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (21, 'Repellendus placeat aut ab sint.', 1, 16);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (22, 'Molestias laudantium ut nisi.', 1, 2);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (23, 'Maiores velit a veritatis eum.', 1, 4);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (24, 'Laboriosam dolore amet saepe nihil omnis atque.', 1, 11);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (25, 'Numquam autem veritatis enim id hic.', 1, 14);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (26, 'Quia iste consequatur error unde eum et fuga.', 1, 6);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (27, 'Voluptatem quibusdam facere omnis inventore ullam.', 1, 3);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (28, 'Eum non atque dolorem velit quam.', 1, 21);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (29, 'Ut eos libero autem laboriosam dolore numquam cupi', 1, 2);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (30, 'Voluptatem iusto cumque totam temporibus aut et ad', 1, 10);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (31, 'Eligendi libero hic nesciunt eum.', 1, 9);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (32, 'Recusandae qui quo explicabo non non.', 1, 17);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (33, 'Quam et omnis voluptatem molestiae dolor.', 1, 24);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (34, 'Unde quo cum deleniti magni.', 1, 1);
INSERT INTO room (room_id, description, is_active, bldg_id) VALUES (35, 'Molestias maiores nostrum facilis porro iure.', 1, 7);

INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (1, 181, 'Windows', 1985, 1, 16, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (2, 116, 'Linux', 465, 0, 25, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (3, 221, 'Linux', 1481, 1, 9, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (4, 103, 'Linux', 1825, 0, 6, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (5, 33, 'Mac', 313, 1, 9, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (6, 175, 'Windows', 704, 1, 3, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (7, 30, 'Windows', 1946, 0, 8, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (8, 177, 'Linux', 439, 0, 5, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (9, 42, 'Mac', 703, 1, 20, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (10, 107, 'Linux', 917, 1, 10, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (11, 41, 'Windows', 1115, 1, 14, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (12, 94, 'Linux', 2045, 0, 14, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (13, 183, 'Mac', 1807, 1, 19, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (14, 133, 'Mac', 1718, 1, 4, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (15, 48, 'Linux', 252, 0, 21, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (16, 222, 'Linux', 520, 1, 12, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (17, 167, 'Linux', 1008, 0, 10, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (18, 151, 'Windows', 1867, 0, 2, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (19, 183, 'Mac', 1836, 1, 24, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (20, 190, 'Linux', 610, 0, 11, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (21, 217, 'Windows', 444, 0, 23, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (22, 252, 'Mac', 157, 0, 24, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (23, 8, 'Linux', 985, 0, 20, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (24, 152, 'Mac', 633, 1, 10, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (25, 126, 'Windows', 1513, 1, 3, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (26, 66, 'Mac', 275, 1, 16, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (27, 93, 'Windows', 883, 1, 11, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (28, 219, 'Linux', 1706, 0, 7, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (29, 251, 'Linux', 749, 0, 13, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (30, 37, 'Windows', 1749, 1, 17, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (31, 228, 'Linux', 1374, 0, 13, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (32, 220, 'Linux', 1910, 1, 18, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (33, 244, 'Linux', 735, 0, 13, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (34, 49, 'Linux', 1083, 0, 15, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (35, 207, 'Mac', 376, 0, 5, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (36, 87, 'Linux', 1834, 1, 4, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (37, 182, 'Linux', 1116, 1, 2, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (38, 227, 'Linux', 370, 0, 14, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (39, 25, 'Windows', 1431, 1, 15, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (40, 246, 'Mac', 1090, 1, 1, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (41, 14, 'Windows', 1161, 0, 1, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (42, 193, 'Windows', 446, 1, 22, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (43, 83, 'Linux', 373, 0, 18, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (44, 130, 'Windows', 1586, 1, 12, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (45, 141, 'Windows', 204, 1, 3, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (46, 222, 'Linux', 1385, 1, 19, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (47, 94, 'Mac', 1351, 0, 7, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (48, 161, 'Linux', 1257, 1, 19, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (49, 67, 'Linux', 854, 1, 20, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (50, 172, 'Windows', 1531, 1, 5, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (51, 133, 'Mac', 482, 1, 6, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (52, 85, 'Mac', 1756, 1, 8, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (53, 112, 'Linux', 1056, 1, 16, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (54, 30, 'Windows', 1947, 1, 1, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (55, 78, 'Windows', 1171, 0, 7, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (56, 26, 'Linux', 472, 1, 25, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (57, 87, 'Mac', 1610, 0, 4, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (58, 66, 'Windows', 1422, 0, 8, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (59, 44, 'Linux', 1812, 0, 11, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (60, 144, 'Linux', 468, 1, 2, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (61, 192, 'Windows', 1212, 1, 6, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (62, 212, 'Mac', 1338, 1, 23, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (63, 182, 'Linux', 279, 1, 18, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (64, 194, 'Mac', 1406, 0, 15, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (65, 199, 'Mac', 1104, 1, 17, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (66, 64, 'Linux', 269, 1, 17, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (67, 154, 'Windows', 1377, 1, 12, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (68, 75, 'Linux', 1307, 1, 21, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (69, 202, 'Mac', 252, 1, 22, 1);
INSERT INTO server_model (model_id, ram, os, hd_size, virtual, manuf_id, is_active) VALUES (70, 165, 'Linux', 1066, 1, 9, 1);

INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (1, 1, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (2, 2, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (3, 3, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (4, 4, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (5, 5, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (6, 6, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (7, 7, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (8, 8, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (9, 9, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (10, 10, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (11, 11, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (12, 12, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (13, 13, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (14, 14, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (15, 15, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (16, 16, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (17, 17, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (18, 18, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (19, 19, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (20, 20, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (21, 21, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (22, 22, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (23, 23, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (24, 24, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (25, 25, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (26, 1, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (27, 2, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (28, 3, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (29, 4, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (30, 5, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (31, 6, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (32, 7, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (33, 8, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (34, 9, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (35, 10, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (36, 11, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (37, 12, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (38, 13, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (39, 14, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (40, 15, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (41, 16, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (42, 17, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (43, 18, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (44, 19, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (45, 20, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (46, 21, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (47, 22, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (48, 23, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (49, 24, 1);
INSERT INTO works_in (person_id, bldg_id, is_active) VALUES (50, 25, 1);